$(document).ready(function(){
	var base_url = $('#base_url').val();
	$(document).on('click','.JSGrupaAdd',function(){
		var block_content = $(this).closest('tr').html();
		var str = block_content.replace('<span class="JSGrupaRemove"></span>', '<span class="JSGrupaRemove">'+trans('Obriši')+'</span>');
		$(this).closest('tr').after('<tr>'+str+'</tr>');
		$(this).closest('tr').next().find('.JSCenaKonf').text('0.00');

	});

	$('#JSKonfAdd').click(function(){
		$(this).attr('disabled',true);
		var not_available = false;
		$('.JSArtikal').each(function(i, obj){
			var roba_id = $(obj).val();
			var kolicina = $(obj).closest('tr').find('.JSKolicina').val();

			if(roba_id != '' && roba_id != -1){			
				$.ajax({
					type: "POST",
					url: base_url + 'konfigurator-cart-add',
					async: false,
					data: { roba_id: roba_id, kolicina: kolicina },
					success: function(msg) {
						if(msg=='not-available'){
							not_available = true;
						}
					}
				});	    	
			}
	    });
		if(not_available){
			 bootboxDialog({ message: "<p>" + trans('Neki od artikala nemaju traženu količinu na stanju') + ".</p><p>" + trans('Dodata je maksimalna količina') + ".</p>", closeButton: true }, 6000); 
		}
		location.href = base_url+'korpa';
	});

	$(document).on('click','.JSGrupaRemove',function(){
		$(this).closest('tr').remove();
		var ukupna = ukupna_cena();
		$('#JSUkupnaCenaKonf').text(ukupna);
		$('#JSUkupno').text(ukupna_sa_korpom(ukupna));
	});

	$(document).on('change','.JSArtikal',function(){
		var data = $(this).find('option:selected').data('cena');
		var cena = parseInt(data);
		var slika = $(this).find('option:selected').data('slika');
		var original_img = $(this).closest('tr').find('.JSSlika');
 
		var object = $(this).closest('tr');
		var kolicina = object.find('.JSKolicina').val();
		var ukupno = kolicina * cena;
		if(data == null){
			ukupno = 0;
		}
		object.find('.JSCenaKonf').text(ukupno+'.00');

		var ukupna = ukupna_cena();
		$('#JSUkupnaCenaKonf').text(ukupna);
		original_img.attr('src',slika);
		$('#JSUkupno').text(ukupna_sa_korpom(ukupna));
	});

	$(document).on('keyup','.JSKolicina',function(){
		var kolicina = parseInt($(this).val());
		if(kolicina>0 && kolicina<100){			
			var data = $(this).closest('tr').find('option:selected').data('cena');
			if(data != null){
				var cena = parseInt(data);
				$(this).closest('tr').find('.JSCenaKonf').text((kolicina*cena)+'.00');

				var ukupna = ukupna_cena();
				$('#JSUkupnaCenaKonf').text(ukupna);
				$('#JSUkupno').text(ukupna_sa_korpom(ukupna));
			}
		}
	});

	$(document).on('click','.JSArtDetails',function(){
		var roba_id = $(this).closest('td').find('.JSArtikal').val();
		if(roba_id != ''){		
			$.post(base_url+'link-artikla',{roba_id: roba_id},function(response){
				window.open(base_url+response, '_blank');
			});
		}
	});
});

function ukupna_cena(){
	var ukupna_cena = 0;
	$('.JSArtikal').each(function(i, obj){
			var cena_str = $(obj).closest('tr').find('.JSCenaKonf').text();
			var cena = parseInt(cena_str.replace('.00',''));
			ukupna_cena += cena;
	    });
	return ukupna_cena+'.00';
}

function ukupna_sa_korpom(ukupna_cena){
	var ukupna_cena = parseInt(ukupna_cena.replace('.00',''));
	var cena_korpa = $('#JSKorpaUkupnaCena').text();
	cena_korpa = parseInt(cena_korpa.replace('.00',''));
	return (cena_korpa+ukupna_cena)+'.00';
}

function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
} 