$(document).ready(function(){
	//login
	$("input[name='email_fg']").val($("input[name='email_login']").val());
	$("input[name='email_login']").keyup(function(){
		$("input[name='email_fg']").val($(this).val());
	});
	$("input[name='email_login']").change(function(){
		$("input[name='email_fg']").val($(this).val());
	});

	//registration
	if($("input[name='flag_vrsta_kupca']:checked").val() == 0){
		$("input[name='naziv']").closest('div').hide();
		$("input[name='pib']").closest('div').hide();
	}
	else if($("input[name='flag_vrsta_kupca']:checked").val() == 1){
		$("input[name='ime']").closest('div').hide();
		$("input[name='prezime']").closest('div').hide();
	}
	
	$("input[name='flag_vrsta_kupca']").click(function(){
		var vrsta = $(this).val();
		if(vrsta == 0){
			$("input[name='naziv']").closest('div').hide();
			$("input[name='pib']").closest('div').hide();
			$("input[name='ime']").closest('div').show();
			$("input[name='prezime']").closest('div').show();
		}else if(vrsta == 1){
			$("input[name='ime']").closest('div').hide();
			$("input[name='prezime']").closest('div').hide();			
			$("input[name='naziv']").closest('div').show();
			$("input[name='pib']").closest('div').show();
		}
	});

	//registration
	if($('.active-user').is('.private-user')){
		$("input[name='naziv']").closest('div').hide();
		$("input[name='pib']").closest('div').hide();
		$("input[name='maticni_br']").closest('div').hide();
	}
	else if($('.active-user').is('.company-user')){
		$("input[name='ime']").closest('div').hide();
		$("input[name='prezime']").closest('div').hide();
	}
	
	$('.login-title span').click(function(event){
		$('.active-user').removeClass('active-user');
		$(this).addClass('active-user');

		if($(event.target).is('.private-user')){

			$("input[name='flag_vrsta_kupca']").val('0');
			$("input[name='naziv']").closest('div').hide();
			$("input[name='pib']").closest('div').hide();
			$("input[name='maticni_br']").closest('div').hide();

			$("input[name='ime']").closest('div').show();
			$("input[name='prezime']").closest('div').show();
			
		}else if($(event.target).is('.company-user')){
			
			$("input[name='flag_vrsta_kupca']").val('1');
			$("input[name='ime']").closest('div').hide();
			$("input[name='prezime']").closest('div').hide();	

			$("input[name='maticni_br']").closest('div').show();		
			$("input[name='naziv']").closest('div').show();
			$("input[name='pib']").closest('div').show();
		}
	});

});