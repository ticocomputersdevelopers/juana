$(document).ready(function () {

	$(".order-status__select").change(function() {

		$selected = $(this).children(":selected");

		var status_target = $selected.data('status-target');
		var web_b2b_narudzbina_id = $selected.data('narudzbina-id');

		$.ajax({
				type: "POST",
				url: base_url+'admin/b2b/ajax/b2b_promena_statusa_narudzbine',
				data: { status_target: status_target, web_b2b_narudzbina_id: web_b2b_narudzbina_id },
				success: function(msg) {
						// alert(msg);
						location.reload();
				}
		}); 

	});
	
	//filtriranje po dodatnom statusu
	$("#narudzbina_status_id").change(function(){
	  		var narudzbina_status_id = $(this).val();
	  		var search = $('#search-btn').val();
	  		var status = $(this).data('status');
			var datum_od = $('#datepicker-from').val();
			var datum_do = $('#datepicker-to').val();
				if(!datum_od){
				datum_od = '0';
				}
				if(!datum_do){
				datum_do = '0';
		 		}
		 		if(!narudzbina_status_id){
				narudzbina_status_id = '0';
		 		}
		 		if(!search){
					search = '0';
		 		}
				var status_str= location.pathname.split('/')[4];
				var statuses = status_str.split('-');
				var index = statuses.indexOf(status);
				if(index !== -1 ){
					statuses.splice(index,1);
				}else{
					statuses.push(status);			
				}

				if(statuses.length == 0)
				{
					statuses.push('sve');
				}
			window.location.href = base_url + 'admin/b2b/narudzbine/' + status_str +'/'+ narudzbina_status_id + '/' + datum_od + '/' + datum_do + '/' + search;
	  	});

		//filtriranje po statusu
		$('.JSStatusNarudzbine').click(function() {
			var search = $('#search-btn').val();
			var status = $(this).data('status');
			var narudzbina_status_id = $('#narudzbina_status_id').val();
			var datum_od = $('#datepicker-from').val();
			var datum_do = $('#datepicker-to').val();
				if(!datum_od){
				datum_od = '0';
				}
				if(!datum_do){
				datum_do = '0';
		 		}
		 		if(!narudzbina_status_id){
				narudzbina_status_id = '0';
		 		}
		 		if(!search){
				search = '0';
		 		}

				var status_str= location.pathname.split('/')[4];
				var statuses = status_str.split('-');
				var index = statuses.indexOf(status);
				if(index !== -1 ){
					statuses.splice(index,1);
				}else{
					statuses.push(status);			
				}

				if(statuses.length == 0)
				{
					statuses.push('sve');
				}
			window.location.href = base_url + 'admin/b2b/narudzbine/' + statuses.join('-') + '/' + narudzbina_status_id + '/' + datum_od + '/' + datum_do + '/' + search;
		});
		
		$('#JSFilterClear').click(function(){

			window.location.href = base_url+'admin/b2b/narudzbine/sve/0/0/0/0';

		});

		$('#search-btn').click(function(){

		var search = $('#search').val();
		var status = $(this).data('status');
		var narudzbina_status_id = $('#narudzbina_status_id').val();
		var datum_od = $('#datepicker-from').val();
		var datum_do = $('#datepicker-to').val();
		var status_str= location.pathname.split('/')[4];
			if(!datum_od){
			datum_od = '0';
			}
			if(!datum_do){
			datum_do = '0';
		 	}
		 	if(!narudzbina_status_id){
			narudzbina_status_id = '0';
		 	}
		 	if(!search){
			search = '0';
		 	}
				

		window.location.href = base_url + 'admin/b2b/narudzbine/' + status_str +'/'+ narudzbina_status_id + '/' + datum_od + '/' + datum_do + '/' + search;
		
		});

		$('#search').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#search').val() != ''){
        	$("#search-btn").trigger("click");
    	}
    	});

		//kalendar za filtriranje po datumu
		$('#datepicker-from').change(function() {
		var search = $('#search-btn').val();	
		var status_str= location.pathname.split('/')[3];
		var narudzbina_status_id = $('#narudzbina_status_id').val();
		var datum_od = $(this).val();
		var datum_do = $(this).data('datumdo');

		if(!datum_od){
				datum_od = '0';
		}
		if(!narudzbina_status_id){
			narudzbina_status_id = '0';
		}
		if(!search){
			search = '0';
		}

		window.location.href = base_url + 'admin/b2b/narudzbine/' + status_str + '/' + narudzbina_status_id + '/' + datum_od + '/' + datum_do + '/' + search;
		});

		$('#datepicker-to').change(function() {
		 var search = $('#search-btn').val();
		 var datum_do = $(this).val();
		 var status_str= location.pathname.split('/')[3];
		 var narudzbina_status_id = $('#narudzbina_status_id').val();
		 var datum_od = $(this).data('datumod');
		 if(!datum_do){
				datum_do = '0';
		 }
		 if(!narudzbina_status_id){
			narudzbina_status_id = '0';
		 }
		 if(!search){
			search = '0';
		 }	

		 window.location.href = base_url + 'admin/b2b/narudzbine/' + status_str + '/' + narudzbina_status_id + '/' + datum_od + '/' + datum_do + '/' + search;
		});


 	$('.JS-prihvati').click(function() {
		var narudzbina_id = $('#narudzbina_id').val();
		$.post(base_url + 'admin/b2b/ajax/prihvati', {narudzbina_id:narudzbina_id}, function (response){
			location.reload();
		});

	});

 	$('.JS-realizuj').click(function(){
		var narudzbina_id = $('#narudzbina_id').val();
		$.post(base_url + 'admin/b2b/ajax/realizuj', {narudzbina_id:narudzbina_id}, function (response){
			location.reload();
		});

	});


	$('.JS-storniraj').click(function() {
		var narudzbina_id = $('#narudzbina_id').val();
		$.post(base_url + 'admin/b2b/ajax/storniraj', {narudzbina_id:narudzbina_id}, function (response){
			location.reload();
		});
	});
	$('.JS-nestorniraj').click(function() {
		var narudzbina_id = $('#narudzbina_id').val();
		$.post(base_url + 'admin/b2b/ajax/nestorniraj', {narudzbina_id:narudzbina_id}, function (response){
			location.reload();
		});
	});
	$('.JS-delete').click(function() {
		var narudzbina_id = $('#narudzbina_id').val();
		$.post(base_url + 'admin/b2b/ajax/obrisi', {narudzbina_id:narudzbina_id}, function (response){	
			alertify.alert(translate('Narudžbina je obrisana!'));
			window.location.href = base_url + 'admin/porudzbine/sve/0/0/0/0';
		});
	});

 	$('.JS-nazad').click(function() {	
		window.location.href = base_url + 'admin/b2b/narudzbine/sve/0/0/0/0';
	});
	
	
	// edit cene u b2b-u
	$('.JSCenaInput').hide();

		$('.JSEditCenaBtn').on('click', function(){
		$(this).parent().children('.JSCenaInput').show();
		$(this).parent().children('.JSCenaInput').focus();
		});

		$('.JSCenaInput').focusout(function(){
			$(this).css('display', 'none');
		});

		$('.JSCenaInput').on("keyup", function(event) {
		var val = $(this).val();
		var stavka_id = $(this).data('stavka-id');
		var narudzbina_id = $('#narudzbina_id').val();
		

		if(event.keyCode == 13){
			$(this).closest('.cena').children('.JSCenaVrednost').html(val);
	    	$(this).focusout();

	    	if(!isNaN(val)){
		    	$.post(base_url+'admin/b2b/ajax/editCena', {stavka_id:stavka_id, val: val, narudzbina_id: narudzbina_id}, function (response){
		    		window.location.reload();
		   //  		var data = JSON.parse(response);
					// $('#cena-artikla').text(data.cena_artikla);
					// $('#troskovi-isporuke').text(data.troskovi_isporuke);
					// $('#ukupna-cena').text(data.ukupna_cena);

		   //  		alertify.success('Uspešno ste promenili Cenu !')

		    	});
		    }else{
		    	window.location.reload();
		    }
  		}
		});

		$('.JSDeleteStavka').click(function() {

		var stavka_id = $(this).data('stavka-id');
		var narudzbina_id = $('#narudzbina_id').val();

		$.post(base_url + 'admin/b2b/ajax/delete_stavka', {stavka_id:stavka_id}, function (response){
			window.location.href = base_url + 'admin/b2b/narudzbina/' + narudzbina_id;
		});

	});
	var articlesTime = 0;
	$('#JSsearch_porudzbine').on("keyup", function() {
				clearTimeout(articlesTime);
				articlesTime = setTimeout(function(){
					$('.articles_list').remove();
					var articles = $('#JSsearch_porudzbine').val();
					var narudzbina_id = $('#narudzbina_id').val();
					if (articles != '' && articles.length > 2) {
						$.post(base_url + 'admin/b2b/ajax/narudzbine_search', {articles:articles,narudzbina_id:narudzbina_id}, function (response){
							$('#search_content').html(response);
						});
					}
				}, 500);
	});
	
	$('.JSMoreWiew').click(function(e) {
		var web_b2b_narudzbina_id = $(this).data('id-narudzbina');
		if (e.target.tagName.toLowerCase() != 'select') {
			window.location.href = base_url + 'admin/b2b/narudzbina/' + web_b2b_narudzbina_id;
		}  
	});

	$('.kolicina').keydown(false);
	$('.kolicina').change(function() {
		var stavka_id = $(this).data('stavka-id');
		var kolicina = $(this).val();  
		var narudzbina_id = $('#narudzbina_id').val();
		var lager = $(this).closest('tr').find('.JSLager');

		if(isNaN(kolicina) || kolicina == 0) {
			$(this).css('border-color', 'red');
		} else {
			$.post(base_url + 'admin/b2b/ajax/update_kolicina', {stavka_id:stavka_id, kolicina:kolicina, narudzbina_id:narudzbina_id}, function (response){
				window.location.reload();
				// var data = JSON.parse(response);
				// $('#cena-artikla').text(data.cena_artikla);
				// $('#troskovi-isporuke').text(data.troskovi_isporuke);
				// $('#ukupna-cena').text(data.ukupna_cena);
				// lager.text(data.lager);
			 });
		}
	});	

	$('#JSAvans').click(function() {
		var narudzbina_id = $('#narudzbina_id').val();
		var avans = 0;
		if($(this).is(':checked')){
			avans = 1;
		}
		$.post(base_url + 'admin/b2b/ajax/edit-avans', {avans:avans, narudzbina_id:narudzbina_id}, function (response){
			window.location.reload();
		 });		
	});

		

});