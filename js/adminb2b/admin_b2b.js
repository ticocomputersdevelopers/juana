$(document).ready(function () {

	$('.JScollapse_btn').on('click', function(){
		$(this).parent().siblings('.JS_collapse_content').toggleClass('hide'); 
	});

	$( ".JSeditSupport" ).change(function(){
		var url = $(this).val();
		if(url != ''){
			location.href = url;
		}
	});



	var $body,
		$headerWidthToggle,
		headerCookie;
 
	$(document).ready(function() {
	var partner_id = $('#JSCheckPartnerConfirm').data("id");
	if(partner_id!=0 && partner_id!=null){
		var check = confirm("Partner je vezan za narudžbine, import ili artikle! Da li želite obrisati sve sto je vezano za ovog partnera?");
		if(check){
			location.href = base_url+'admin/b2b/kupci_partneri/partneri/'+partner_id+'/1/delete';
		}
	}
	});

   	$('#login-pass2').hide();
		$("#show-pass").click(function(){
			$('#login-pass2').val($('#login-pass1').val());

			$('#login-pass2').toggle();
			$('#login-pass1').toggle();
		});
    
    $('.page-edit-save').click(function() {
         var naslov = $('#page_name').val();
         if (naslov == '' || naslov == ' ') {
             $('#page_name').css("border", "1px solid red").focus().fadeIn(200);
         } else {

             form_pages.submit();
         }
    });


	$(function() {
		$('.JSPagesSortable').sortable({
			update: function(event, ui) {

				var moved = ui.item[0].id;
				var parent_id = $(event.target).attr('parent');
				var order = [];
				$('.JSPagesSortable li').each( function(e) {
					order.push( $(this).attr('id') );
				});

				if(moved != 0){	 
					$.ajax({
						type: "POST",
						url: base_url+'admin/b2b/position',
						data: {parent_id:parent_id, order:order, moved:moved},
						success: function(msg) {
						  // console.log('test');
						}
					});
				}	
			}
		});					   
		$( ".JSPagesSortable").disableSelection();		
	});

    $(function() {
	$('.JSListaSliderB2B').sortable({
		update: function(event, ui) {

			var moved = ui.item[0].id;
			var order = []; 
			$('.JSListaSliderB2B li').each( function(e) {

			order.push( $(this).attr('id') );				 
			});
			if (moved !=0){
			$.ajax({
					type: "POST",
					url: base_url+'admin/b2b/position-baner',
					data: {order:order, moved:moved},
					success: function(msg) {
					  // console.log('test');
					}
				});
			}
		}
	});					   
	$( ".JSListaSliderB2B").disableSelection();
						
	});	

  // SORTABLE SLIDERS
  
	$(function() {
	 
	$('.JSListaBanerB2B').sortable({
		update: function(event, ui) {

			var moved = ui.item[0].id;
			var order = []; 
			$('.JSListaBanerB2B li').each( function(e) {
			order.push( $(this).attr('id') );				 
			});

			if(moved !=0){
				$.ajax({
					type: "POST",
					url: base_url+'admin/b2b/position-baner',
					data: {order:order, moved:moved},
					success: function(msg) {
					  // console.log('test');
					}
				});
			}
		}
	});					   
	$( ".JSListaBanerB2B").disableSelection();
						
	});  
	

    $('.images-delete').click(function() {
         var url = $(this).data('url');

         //alert("Link je"+url);

         $.ajax({
             type: "POST",
             url: base_url + 'admin/b2b/delete_image',
             data: { url: url },
             success: function(msg) {
                 //alert(msg);
                 location.reload();
             }
         });
     });
    
	$(function() {
    	$( "#datum_od_b2b, #datum_do_b2b" ).datepicker();
        });
        $('#datum_od_b2b').keydown(false);
        $('#datum_do_b2b').keydown(false);
        $('#datum_od_delete').click(function(){
            $('#datum_od_b2b').val('');
        });
        $('#datum_do_delete').click(function(){
            $('#datum_do_b2b').val('');
	});

// $('.rabat_novi').keydown(function(e){
// 	if(e.keyCode != 8 && e.keyCode != 46 && e.keyCode != 190 && e.keyCode != 37 && e.keyCode != 39){
// 		e.preventDefault();
// 	}	
// });

 $('#JSDeletePartnerKategorije').click(function(){
		var link = $(this).data('link');		
  		alertify.confirm("Da li ste sigurni da želite da izvršite akciju brisanja?",
            function(check){
                if(check){
             		location.href = link;
                }
		});
	});


	// function collapseAdminHeader() {
	//   $body.removeClass('small-width');
	//   $.post(
	// 	  base_url+'admin/ajax', {
	// 		  action: 'collapseAdminHeaderWidth'
	// 	  }, function (response){}
	//   );
	// }

	// function expendAdminHeader() {
	//   $body.addClass('small-width');
	//   $.post(
	// 	  base_url+'admin/ajax', {
	// 		  action: 'expendAdminHeaderWidth'
	// 	  }, function (response){}
	//   );
	// }
		function onHeaderWidthToggle() {
		if ($window.width() < 1014) {
		  $body.toggleClass('small-width');
		  $.post(
			  base_url+'admin/ajax', {
				  action: 'adminHeaderWidth'
			  }, function (response){}
		  );
		} else if ($window.width() < 767 ) {
		  $body.toggleClass('extra-small-width');
		} else {
		  $body.toggleClass('small-width');
		  $.post(
			  base_url+'admin/ajax', {
				  action: 'adminHeaderWidth'
			  }, function (response){}
		  );
		}
	}

	$(document).ready(function() {
	var partner_id = $('#JSCheckPartnerConfirm').data("id");
	if(partner_id!=0 && partner_id!=null){
		var check = confirm("Partner je vezan za narudžbine, import ili artikle! Da li želite obrisati sve sto je vezano za ovog partnera?");
		if(check){
			location.href = base_url+'admin/b2b/kupci_partneri/partneri/'+partner_id+'/1/delete';
		}
	}
	});
    
    $('.page-edit-save').click(function() {
         var naslov = $('#page_name').val();
         if (naslov == '' || naslov == ' ') {
             $('#page_name').css("border", "1px solid red").focus().fadeIn(200);
         } else {

             form_pages.submit();
         }
    });

    $('.images-delete').click(function() {
         var url = $(this).data('url');

         //alert("Link je"+url);

         $.ajax({
             type: "POST",
             url: base_url + 'admin/b2b/delete_image',
             data: { url: url },
             success: function(msg) {
                 //alert(msg);
                 location.reload();
             }
         });
     });


	// function collapseAdminHeader() {
	//   $body.removeClass('small-width');
	//   $.post(
	// 	  base_url+'admin/ajax', {
	// 		  action: 'collapseAdminHeaderWidth'
	// 	  }, function (response){}
	//   );
	// }

	// function expendAdminHeader() {
	//   $body.addClass('small-width');
	//   $.post(
	// 	  base_url+'admin/ajax', {
	// 		  action: 'expendAdminHeaderWidth'
	// 	  }, function (response){}
	//   );
	// }
 

	// function init() {
	//   if($window.width() < 1024) {
	// 	expendAdminHeader();
	//   }
	// }

	function binding () {
		$headerWidthToggle.on('click', onHeaderWidthToggle);
	}

	$(document).ready(function() {

		// declere variables
		$body = $('body');
		$window = $(window);
		$adminHeader = $body.find('#admin-header');
		$headerWidthToggle = $body.find('.header-width-toggle');

		var headerCookie = document.cookie = "headerSmall=true";

		binding();
		// init();
		
		// $window.resize(function(){
		//   init();
		// });
		



	// $(".select2").select2({
	// 	placeholder: 'Izaberite grupu'
	// });


	});

   // DATEPICKER 
	 
   $(function() {
		$( "#datepicker-from, #datepicker-to, #order-date" ).datepicker();
   }); 

	function check_fileds(polje) {
     polje2 = document.getElementById(polje).value;
     if (polje == 'email') {
         if (isValidEmailAddress(polje2) == false) {

             $('#email').css("border", "1px solid red").focus().fadeIn(200);
         } else {
             $('#email').css("border", "1px solid #ccc").fadeIn(200);
         }

     } else if (polje == 'without-reg-email') {
         if (isValidEmailAddress(polje2) == false) {

             $('#without-reg-email').css("border", "1px solid red").focus().fadeIn(200);
         } else {
             $('#without-reg-email').css("border", "1px solid #ccc").fadeIn(200);
         }

     } else {
         if (polje2 == '' || polje2 == ' ') {
             $('#' + polje).css("border", "1px solid red").focus().fadeIn(200);
         } else {
             $('#' + polje).css("border", "1px solid #ccc").fadeIn(200);
         }

     }

 }	



	$(function() {
	$( "#datum_od, #datum_do, #datum_do_din, #datum_od_din" ).datepicker();
	});

	$('#datum_do').keydown(false);
	$('#datum_od').keydown(false);
	$('#datum_do_din').keydown(false);
	$('#datum_od_din').keydown(false);

	$('#datum_od').change(function() {
	 var datum_od = $(this).val();
	 var datum_do = $('#datum_do').val();

	if(datum_do != '' && datum_od != ''){
		window.location.href = base_url + 'admin/b2b/analitika/' + datum_od + '/' + datum_do;
	}

	});

	$('.JSAnalitikaGrupa').click(function() {
		var id = $(this).data('id');
		if($(this).data('isopen') == 'closed'){
			$(this).data('isopen','opened');
			$(".JSAnalitikaArtikliGrupe[data-id='"+id+"']").removeAttr('hidden');
		}else{
			$(this).data('isopen','closed');
			$(".JSAnalitikaArtikliGrupe[data-id='"+id+"']").attr('hidden','hidden');
		}
	});


	$('#datum_do').change(function() {
	 var datum_do = $(this).val();
	 var datum_od = $('#datum_od').val();

	if(datum_do != '' && datum_od != ''){
		window.location.href = base_url + 'admin/b2b/analitika/' + datum_od + '/' + datum_do;
	}
	});
	
	$('.JSbtn-delete').on('click', function(e){
	e.preventDefault();
	var link = $(this).data('link');
  	alertify.confirm("Da li ste sigurni da želite da izvršite akciju brisanja?",
                function(e){
                if(e){
                    location.href = link;
                }
          });
	});

	$('#pretraga_prihoda').on('click', function(){
		var dt_od = $('#datum_od_din').val();
		var dt_do = $('#datum_do_din').val();
		console.log('clicked');
		if(dt_od != '' && dt_do != ''){
			window.location.href = base_url + 'admin/b2b/analitika/prihod/' + dt_od + '/' + dt_do;
		}
	});
	$('.datum-val').keydown(false);

	$("#JSWebKupac").change(function() {
		console.log(4);
		$.post(base_url + 'admin/b2b/ajax/narudzbina-kupac', {partner_id:$(this).val()}, function (response){
			$('#JSKupacAjaxContent').html(response);
		});
	});

});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}


 $(document).ready(function(){
 	if ($(window).width() > 1024 ) {    
		$('.tooltipz').on('mouseenter', function(e){    
			var title = $(this).attr('aria-label'),
				pos_left = e.pageX,
				pos_top = $(this).offset().top;
 
			$('body').append('<p id="JScustomTooltip">' + title + '</p>');  
			$('#JScustomTooltip').css({
				top: pos_top - $('#JScustomTooltip').outerHeight(true),
				left: pos_left - $('#JScustomTooltip').width() / 2 
			}).show()  
		}).on('mouseleave', function(){
			 $('#JScustomTooltip').remove();
		}); 
 	} 
 });
