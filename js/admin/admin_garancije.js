$(document).ready(function () {

	$("input[name='datum_izdavanja']").datepicker();
	
	$("#proizvodjac_id").select2({
		placeholder: translate('Izaberite proizvođaca'),
		language: {
            noResults: function () {
            return translate('Nema rezultata');
            }
        }
	});
	$("#roba_id").select2({
		placeholder: translate('Izaberite artikal'),
		language: {
            noResults: function () {
            return translate('Nema rezultata');
            }
        }
	});

	$("#proizvodjac_id").change(function(){
		$.post(base_url + 'proizvodjac-articles', {proizvodjac_id:$(this).val()}, function (response){
			$('#roba_id').html(response);
			$("#roba_id").select2("val", "0");
		});
	});

	// var articlesTime;
	// $('#articles').on("keyup", function() {
	// 	clearTimeout(articlesTime);
	// 	articlesTime = setTimeout(function(){
	// 		$('.articles_list').remove();
	// 		var articles = $('#articles').val();
	// 		if (articles != '' && articles.length > 2) {
	// 			$.post(base_url + 'produzena_garancija_articles_search', {articles:articles}, function (response){
	// 				$('#search_content').html(response);
	// 			});
	// 		} 				
	// 	}, 500);
	// });

	// $(document).on("click", ".articles_list__item", function() {
	// 	$('input[name="roba_id"]').val($(this).data('roba_id'));
	// 	$('input[name="model"]').val($(this).find('.articles_list__item__link__text').text());
	// 	$('.articles_list').remove();
	// });

	// $('html :not(.articles_list)').on("click", function() {
	// 	$('.articles_list').remove();
	// });

	$('input[name="datum_od"]').datepicker({
		format: 'Y-m-d'
	});

	$('input[name="datum_do"]').datepicker({
		format: 'Y-m-d'
	});

	$('.JSSort').click(function(){
		var sort_column = $(this).data('sort_column');
		var sort_direction = $(this).data('sort_direction');
		var current_url = window.location.pathname;
		var current_url_address = current_url.split('/');
		if(current_url_address[3] == undefined){
			current_url_address[3] = 'null';
		}
		current_url_address[6] = sort_column;
		current_url_address[7] = sort_direction;
		location.href = current_url_address.join("/");
	});

	$('#JSSearchBtn').click(function(){
		var search = $('#JSSearch').val();
		search = search.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
		if(search == ''){
			search = 'null';
		}
		var current_url = window.location.pathname;
		var current_url_address = current_url.split('/');
		current_url_address[3] = search;
		location.href = current_url_address.join("/");
	});

	$('input[name="datum_od"]').change(function(){
		var datum_od = $(this).val();
		if(datum_od == ''){
			datum_od = 'null';
		}
		var current_url = window.location.pathname;
		var current_url_address = current_url.split('/');
		current_url_address[4] = datum_od;
		location.href = current_url_address.join("/");
	});	

	$('input[name="datum_do"]').change(function(){
		var datum_do = $(this).val();
		if(datum_do == ''){
			datum_do = 'null';
		}
		var current_url = window.location.pathname;
		var current_url_address = current_url.split('/');
		current_url_address[5] = datum_do;
		location.href = current_url_address.join("/");
	});

	$('#JSGarancijeIzvestaj').click(function(){
		var current_url = window.location.pathname;
		location.href = current_url+'?izvestaj=1';
	});

});