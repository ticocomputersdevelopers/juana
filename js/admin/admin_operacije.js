$(document).ready(function() {

  	$('.JSOperacijaGrupa').keydown(function(event) {
    	if (event.keyCode == 13) {
			$.post(base_url + 'admin/ajax/operacija-grupa-save', { operacija_grupa_id: $(this).data('id'), naziv: $(this).val() }, function (response){
				location.reload(true);
			});	    		
    	}
  	});

  	$('.JSOperacija,.JSOperacijaCena').keydown(function(event) {
  		var naziv = $(this).closest('li').find('.JSOperacija').val();
  		var cena = $(this).closest('li').find('.JSOperacijaCena').val();

  		var data = { 
  			operacija_id: $(this).data('id'),
  			operacija_grupa_id: $(this).closest('li').data('id'),
  			naziv: naziv,
  			cena: cena
  		};

    	if (event.keyCode == 13 && naziv.length > 0 && !isNaN(cena)) {
			$.post(base_url + 'admin/ajax/operacija-save', data, function (response){
				location.reload(true);
			});	    		
    	}else if (event.keyCode == 13){
    		if(naziv.length == 0){
    			var error_message = translate('Polje naziva je prazno!');
    		}else if(isNaN(cena)){
    			var error_message = translate('Polje za cenu nema vrednost broja!');
    		}
			alertify.error(error_message);
    	}
  	});

  	$('.JSOperacijaServisa').keydown(function(event) {
    	if (event.keyCode == 13) {
    		var operacija_servis_id = $(this).data('operacija_servis_id');
    		var operacija_grupa_id = $(this).data('operacija_grupa_id');
    		var operacija_id = $(this).data('operacija_id');
    		if(operacija_servis_id == 0){
	    		var partner_id = $('#JSServisId').val();
	    	}else{
	    		var partner_id = $(this).data('partner_id');
	    	}
	    	
			$.post(base_url + 'admin/ajax/operacija-servis-save', { operacija_servis_id: operacija_servis_id, partner_id: partner_id, operacija_id: operacija_id, cena: $(this).val() }, function (response){
				if(response.success){
					alertify.success(response.message);
					setTimeout(function(){
						location.href = base_url + 'admin/operacije/' + operacija_grupa_id +'/'+ operacija_id;
					},1000);
				}else{
					alertify.error(response.message);
				}
			}); 	
    	}
  	});

});