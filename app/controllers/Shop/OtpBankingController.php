 <?php

class OtpBankingController extends BaseController{

    public function otp(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $web_b2c_narudzbina_id = Request::segment(2+$offset);

        $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();
        if(is_null($web_b2c_narudzbina)){
            return Redirect::to(Options::base_url());
        }
        $web_kupac = DB::table('web_kupac')->where('web_kupac_id',$web_b2c_narudzbina->web_kupac_id)->first();
        if(is_null($web_kupac)){
            return Redirect::to(Options::base_url());
        }
        $preduzece = DB::table('preduzece')->where('preduzece_id',1)->first();
        if(is_null($preduzece)){
            return Redirect::to(Options::base_url());
        }
        Session::put('valuta',1);

        $orgClientId  =  Options::str_options(3023,'str_data');
        $orgOid = $web_b2c_narudzbina_id;

        $orgAmount = DB::select("SELECT round(SUM(jm_cena*kolicina), 2) AS suma from web_b2c_narudzbina_stavka where web_b2c_narudzbina_id=".$orgOid)[0]->suma;
        $orgAmount += Cart::troskovi($web_b2c_narudzbina_id);
        $orgAmount = number_format($orgAmount,2,",","");

        $orgOkUrl =  Options::base_url()."otp-response";
        $orgFailUrl = Options::base_url()."otp-response";
        $orgCancelUrl = Options::base_url()."korpa";
        $orgTransactionType = "PreAuth";
        $orgRnd =  microtime();
        $orgCurrency = "941";

        $clientId = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgClientId));
        $oid = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgOid)); 
        $amount = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgAmount)); 
        $okUrl = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgOkUrl)); 
        $failUrl = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgFailUrl)); 
        $cancelUrl = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgCancelUrl));
        $transactionType = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgTransactionType)); 
        // $installment = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgInstallment)); 
        $rnd = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgRnd ));
        $currency = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgCurrency));
        $storeKey = str_replace("|", "\\|", str_replace("\\", "\\\\", Options::str_options(3024,'str_data')));
        // $plainText = $clientId .  $storeKey .  $oid .  $storeKey .  str_replace(",", "", $amount) .  $storeKey ;
        $plainText = "URLMS=".$cancelUrl."&URLDONE=".$okUrl."&ORDERID=".Order::broj_dokumenta($orgOid)."&SHOPID=".$clientId."&AMOUNT=".round($amount*100)."&CURRENCY=941&ACCOUNTINGMODE=D&AUTHORMODE=I&".$storeKey."";

        // var_dump($plainText2); die;
        //var_dump(str_replace(",", "", $amount));die;
        // $hashValue = hash('sha512', $plainText);
 
        $hash= hash('sha1',$plainText);
       
        
        // var_dump($hash2);die;/
        //$hash = base64_encode (pack('H*',$hashValue));
        // var_dump($cancelUrl); die;
        $data = array(
            'ShopID' => $clientId,
            'ShoppingCartID' => Order::broj_dokumenta($orgOid),
            'Version' => 2.0,
            'TotalAmount' => round($amount*100),
            'ReturnURL' => $okUrl,
            'ReturnErrorURL' => $failUrl,
            'CancelURL' => $cancelUrl,
            'web_kupac' => $web_kupac,
            'preduzece' => $preduzece,
	    	'title' => 'otp',
	    	'description' => 'otp',
	    	'keywords' => 'otp',
	    	'strana' => 'otp',
            "org_strana"=>'otp',
            "mac"=> $hash
            );

    	return View::make('shop/themes/'.Support::theme_path().'pages/otp',$data);
    }

    public function otp_response(){
        $request = Request::all();
        if(!isset($request) || is_null($request) || empty($request)) {
            return Redirect::to(Options::base_url().Url_mod::slug_trans('korpa'))->with('failure-message',Language::trans('Došlo je do greške. Račun platne kartice nije zadužen').'.');
        }

        if($request['RESULT'] != 00){

            $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('broj_dokumenta',$request['ORDERID'])->first();
            $web_b2c_narudzbina_id = $web_b2c_narudzbina->web_b2c_narudzbina_id;

            if(is_null($web_b2c_narudzbina)){
                return Redirect::to(Options::base_url().Url_mod::slug_trans('korpa'))->with('failure-message',Language::trans('Došlo je do greške. Račun platne kartice nije zadužen').'.');
            }

            $bank_response = json_encode(array(
                'realized' => $request['RESULT'] == 00 ? 1 : 0,
                'payment_id' => isset($request['TRANSACTIONID']) ? $request['TRANSACTIONID'] : '',
                'oid' => isset($request['ORDERID']) ? $request['ORDERID'] : '',
                'trans_id' => isset($request['TRANSACTIONID']) ? $request['TRANSACTIONID'] : '',                
                'track_id' => '',
                'post_date' => date("d.m.Y."),
                'result_code' => isset($request['RESULT']) ? $request['RESULT'] : '', 
                'auth_code' => isset($request['AUTHNUMBER']) ? $request['AUTHNUMBER'] : '',
                'response' => 'NEUSPEŠNO',
                'md_status' => '',
                'bank_response' => $request
                ));

            $data = array('result_code'=> $bank_response);

            DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->update($data);
        }else{
            $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('broj_dokumenta',$request['ORDERID'])->first();
            $web_b2c_narudzbina_id = $web_b2c_narudzbina->web_b2c_narudzbina_id;
            if(is_null($web_b2c_narudzbina)){
                return Redirect::to(Options::base_url().Url_mod::slug_trans('korpa'))->with('failure-message',Language::trans('Došlo je do greške. Račun platne kartice nije zadužen').'.');
            }           
            $bank_response = json_encode(array(
                'realized' => $request['RESULT'] == 00 ? 1 : 0,
                'payment_id' => isset($request['TRANSACTIONID']) ? $request['TRANSACTIONID'] : '',
                'oid' => isset($request['ORDERID']) ? $request['ORDERID'] : '',
                'trans_id' => isset($request['TRANSACTIONID']) ? $request['TRANSACTIONID'] : '',
                'track_id' => '',
                'post_date' => date("d.m.Y."),
                'result_code' => isset($request['RESULT']) ? $request['RESULT'] : '', 
                'auth_code' => isset($request['AUTHNUMBER']) ? $request['AUTHNUMBER'] : '',
                'response' => 'USPEŠNO',
                'md_status' => '',
                'bank_response' => $request
                ));
            
                $data = array('result_code'=> $bank_response);

            if($request['RESULT'] == 00){
                $data['stornirano'] = 0;
                $data['poslednja_izmena']=date('Y-m-d H:i:s');

                DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->update($data);
                return Redirect::to(Options::base_url().Url_mod::slug_trans('narudzbina').'/'.$web_b2c_narudzbina_id);
            }
            DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->update(array('result_code'=> $bank_response));
        }
        return Redirect::to(Options::base_url().Url_mod::slug_trans('otp-odgovor').'/'.$web_b2c_narudzbina_id);
    }

    public function otp_failure(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $web_b2c_narudzbina_id = Request::segment(2+$offset);

        $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();
        if(is_null($web_b2c_narudzbina) && $web_b2c_narudzbina->web_nacin_placanja_id!=3){
            return Redirect::to(Options::base_url());
        }
        $kupac = DB::table('web_kupac')->where('web_kupac_id',$web_b2c_narudzbina->web_kupac_id)->first();
        if(is_null($kupac)){
            return Redirect::to(Options::base_url());
        }
        if(!Session::has('b2c_korpa')){
            return Redirect::to(Options::base_url());
        }
        Session::forget('b2c_korpa');


        $subject=Language::trans('otp')." ".Order::broj_dokumenta($web_b2c_narudzbina_id);
        
        if($kupac->flag_vrsta_kupca == 0){
            $name = $kupac->ime.' '.$kupac->prezime;
        }else{
            $name = $kupac->naziv.' '.$kupac->pib;
        }
        $data=array(
            "strana"=>'order',
            "org_strana"=>'order',
            "title"=> $subject,
            "description"=>"",
            "keywords"=>"",
            "web_b2c_narudzbina_id"=>$web_b2c_narudzbina_id,
            "bank_result"=> json_decode($web_b2c_narudzbina->result_code),
            "kupac"=>$kupac
            );
        $body = View::make('shop/themes/'.Support::theme_path().'partials/order_details',$data)->render();
        WebKupac::send_email_to_client($body,$kupac->email,$subject);

        return View::make('shop/themes/'.Support::theme_path().'pages/order',$data);  
    }

}