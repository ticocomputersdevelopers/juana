<?php

class AdminDBUpdateVersion extends Controller {
	function __construct(){
		DB::statement("create or replace function addcol(schemaname varchar, tablename varchar, colname varchar, coltype varchar)
returns varchar 
language 'plpgsql'
as 
$$
declare 
    col_name varchar ;
begin 
      execute 'select column_name from information_schema.columns  where  table_schema = ' ||
      quote_literal(schemaname)||' and table_name='|| quote_literal(tablename) || '   and    column_name= '|| quote_literal(colname)    
      into   col_name ;   

      raise info  ' the val : % ', col_name;
      if(col_name is null ) then 
          col_name := colname;
          execute 'alter table ' ||schemaname|| '.'|| tablename || ' add column '|| colname || '  ' || coltype; 
      else
           col_name := colname ||' Already exist';
      end if;
return col_name;
end;
$$");
		DB::statement("create or replace function dropcol(schemaname varchar, tablename varchar, colname varchar)
returns varchar 
language 'plpgsql'
as 
$$
declare 
    col_name varchar ;
begin 
      execute 'select column_name from information_schema.columns  where  table_schema = ' ||
      quote_literal(schemaname)||' and table_name='|| quote_literal(tablename) || '   and    column_name= '|| quote_literal(colname)    
      into   col_name ;   

      raise info  ' the val : % ', col_name;
      if(col_name is null ) then 
           col_name := colname ||' Not exist';
      else
          col_name := colname;
          execute 'alter table ' ||schemaname|| '.'|| tablename || ' drop column '|| colname; 
      end if;
return col_name;
end;
$$");
		DB::statement("create or replace function addtable(schemaname varchar, tablename varchar, body varchar)
returns varchar 
language 'plpgsql'
as 
$$
declare 
    tab_name varchar;
begin 
      execute 'select table_name from information_schema.tables  where  table_schema = ' ||
      quote_literal(schemaname)||' and table_name='|| quote_literal(tablename)    
      into   tab_name;   

      raise info  ' the val : % ', tab_name;
      if(tab_name is null ) then 
          tab_name := tablename;
          execute 'create table ' ||schemaname|| '.'|| tablename || ' ('|| body ||') with (OIDS=TRUE)';
      else
           tab_name := tablename ||' Already exist';
      end if;
return tab_name;
end;
$$");
	}

	function update($old=false){
		if($old!=false){
		// update proizvodjac
	    DB::statement("ALTER TABLE proizvodjac ALTER COLUMN sifra TYPE character varying(100)");
	    DB::statement("SELECT addcol('public','proizvodjac','slika','character varying(300)')");
	    DB::statement("SELECT addcol('public','proizvodjac','opis','text')");
	    DB::statement("SELECT addcol('public','proizvodjac','keywords','character varying(300)')");
	    DB::statement("SELECT addcol('public','proizvodjac','brend_prikazi','smallint')");
	    DB::statement("SELECT addcol('public','proizvodjac','rbr','integer')");
	    $max = DB::select("SELECT MAX(proizvodjac_id) AS max FROM proizvodjac")[0]->max + 1;
	    DB::statement("INSERT INTO proizvodjac (proizvodjac_id,naziv,sifra,sifra_connect,slika,opis) SELECT ".$max.",naziv,sifra,sifra_connect,slika,opis FROM proizvodjac WHERE proizvodjac_id = 0");
	    DB::statement("UPDATE roba SET proizvodjac_id = ".$max." WHERE proizvodjac_id = 0");
	    DB::statement("DELETE FROM proizvodjac WHERE proizvodjac_id = 0");
	    // DB::statement("INSERT INTO proizvodjac (proizvodjac_id,naziv,sifra_connect) SELECT * FROM (VALUES (-1,'Svi proizvodjaci','0')) redovi(proizvodjac_id,naziv,sifra_connect) WHERE proizvodjac_id NOT IN (SELECT proizvodjac_id FROM proizvodjac)");

	    // tarifna grupa
	    DB::statement("SELECT addcol('public','tarifna_grupa','active','smallint')");
	    DB::statement("SELECT addcol('public','tarifna_grupa','default_tarifna_grupa','smallint')");

	    //rabat
	    DB::statement("SELECT addcol('public','grupa_pr','rabat','integer')");
	    DB::statement("SELECT addcol('public','proizvodjac','rabat','integer')");
	    
	    // preracunavanje cena kod auto short importa
	     DB::statement("SELECT addcol('public','partner','preracunavanje_cena','smallint DEFAULT 1')");

	    // update tip
	    DB::statement("SELECT addcol('public','tip_artikla','active','smallint')");
	    DB::statement("SELECT addcol('public','tip_artikla','rbr','integer')");

	    // update dobavljac_karakteristike
	    DB::statement("SELECT addcol('public','dobavljac_cenovnik_karakteristike','karakteristika_grupa','character varying(255)')");
	    DB::statement("SELECT addcol('public','dobavljac_cenovnik_karakteristike','redni_br_grupe','integer')");

	    // update grupa_pr, proizvodjac
	    DB::statement("SELECT addcol('public','grupa_pr','keywords','character varying(300)')");
	    DB::statement("INSERT INTO grupa_pr (grupa_pr_id,grupa,sifra) SELECT * FROM (VALUES 
	    	(-1,'Nedefinisana','-1')
	    	) redovi(grupa_pr_id,grupa,sifra) 
	        WHERE grupa_pr_id NOT IN (SELECT grupa_pr_id FROM grupa_pr)");
	    DB::statement("INSERT INTO proizvodjac (proizvodjac_id,naziv) SELECT * FROM (VALUES 
	    	(-1,'Nedefinisan'),(0,'Nedefinisan')
	    	) redovi(proizvodjac_id,naziv) 
	        WHERE proizvodjac_id NOT IN (SELECT proizvodjac_id FROM proizvodjac)");

	    // update roba
	    DB::statement("SELECT addcol('public','roba','pregledan_puta_b2b','bigint')");
	    DB::statement("SELECT addcol('public','roba','sku','character varying(10)')");
	    DB::statement("SELECT addcol('public','roba','datum_akcije_od','date')");
	    DB::statement("SELECT addcol('public','roba','datum_akcije_do','date')");
		DB::statement("SELECT addcol('public','roba','osobine','smallint')");
		DB::statement("SELECT addcol('public','roba','seo','text')");
		DB::statement("SELECT addcol('public','roba','sifra_is','character varying(255)')");
		DB::statement("SELECT dropcol('public','roba','sifra')");
		DB::statement("SELECT addcol('public','roba','id_is','character varying(255)')");

	    // update lager
	    DB::statement("SELECT addcol('public','lager','sifra_is','character varying(50)')");

	    // update dobavljac_cenovnik
	    DB::statement("SELECT addcol('public','dobavljac_cenovnik_temp','sku','character varying(10)')");
	    DB::statement("SELECT addcol('public','dobavljac_cenovnik','sku','character varying(10)')");

	    //update imenik
	    DB::statement("ALTER TABLE imenik ALTER COLUMN login TYPE character varying(255)");
	    DB::statement("ALTER TABLE imenik ALTER COLUMN password TYPE character varying(255)");
	    DB::statement("SELECT addcol('public','imenik','aop_user_id','integer DEFAULT (NULL)::integer')");
	    DB::statement("SELECT addcol('public','imenik','datum_otvaranja','timestamp without time zone')");
	    DB::statement("SELECT addcol('public','imenik','trial','smallint DEFAULT 0')");

	    //update web_files
	    DB::statement("SELECT addcol('public','web_files','vrsta_fajla_id','integer')");
	    //update web_vesti
	    DB::statement("SELECT addcol('public','web_vest_b2c','slika','character varying(300)')");
	    DB::statement("SELECT addcol('public','web_vest_b2c','rbr','integer')");
	    DB::statement("SELECT addcol('public','web_vest_b2c','b2b_aktuelno','smallint')");
	    DB::statement("SELECT dropcol('public','web_vest_b2c','tekst')");


	    //update dobavljac_kolone
	    DB::statement("SELECT addcol('public','dobavljac_cenovnik_kolone','import_naziv_web','character varying(50)')");
	    DB::statement("SELECT addcol('public','dobavljac_cenovnik_kolone','service_username','character varying(50)')");
	    DB::statement("SELECT addcol('public','dobavljac_cenovnik_kolone','service_password','character varying(255)')");
	    DB::statement("SELECT addcol('public','dobavljac_cenovnik_kolone','auto_link','character varying(500)')");
	    DB::statement("SELECT addcol('public','dobavljac_cenovnik_kolone','auto_import','smallint')");
	    DB::statement("SELECT addcol('public','dobavljac_cenovnik_kolone','auto_import_short','smallint')");

	    //update vrsta_cena
	    DB::statement("DELETE FROM vrsta_cena");
	    DB::statement("INSERT INTO vrsta_cena (vrsta_cena_id,naziv,valuta_id,selected) SELECT * FROM (VALUES (1,'NC',1,1),(2,'WEB',1,1),(3,'MP',1,1)) redovi(vrsta_cena_id,naziv,valuta_id,selected) 
	    	WHERE naziv NOT IN (SELECT naziv FROM vrsta_cena)");
	    //valuta
	    DB::statement("INSERT INTO valuta (valuta_id,valuta_sl,valuta_br,valuta_naziv,valuta_zemlja,valuta_paritet,ukljuceno) SELECT * FROM (VALUES (4,'GBP',826,'Britanska Funta','GB',1,1),(5,'CHF',756,'Svajcarski Franak','SUI',1,1)) redovi(valuta_id,valuta_sl,valuta_br,valuta_naziv,valuta_zemlja,valuta_paritet,ukljuceno) 
	    	WHERE valuta_sl NOT IN (SELECT valuta_sl FROM valuta)");

	    //update options
	    DB::statement("ALTER TABLE options ALTER COLUMN naziv TYPE character varying(100)");
	    DB::statement("INSERT INTO options (options_id,preduzece_id,naziv,str_data,int_data) SELECT * FROM (VALUES (3002,1,'Hash-ovana zastita za import i export','49d48ad56b07f614a601f3dcde319d17',0),
	        (3003,1,'SWIFT MAILER',null,0),(3004,1,'B2C CONNECT-poslovnagodina',null,0),(3005,1,'B2C CONNECT-prikaz artikala u vise grupa',null,1),(3006,1,'WEB ADMIN - dodatne opcije kod povezanih artikala',null,0),(3007,1,'Automatika - formiranje web cene od pmp cene',null,0),(3008,1,'Automatika - primarni dobavljac_id kod povezanih (0 - nema primarnog dobavljaca)',null,0),(3009,1,'Automatika - najniza cena kod povezanih (0 - ne prati najnizu cenu)',null,1),(3010,1,'Automatika - kolicina veca od 0 kod povezanih (0 - ne prati kolicinu)',null,1),(3011,1,'Automatika - Menjanje flaga na web-u u zavisnosti od kolicine',null,0),(3012,1,'Automatika - Cene kod primarnog dobavljaca se preracunavaju',null,1),(3013,1,'AOP - da li je AOP shop',null,0),(3018,1,'B2B korišćenje Kombinacije rabata',null,0),(3019,1,'Google analitika',null,0),(3020,1,'Zendesk Chat',null,0),(3021,1,'Custom boje',null,0),(3022,1,'Da li je AOP shop aktivan',null,1)) redovi(options_id,preduzece_id,naziv,str_data,int_data) 
	        WHERE options_id NOT IN (SELECT options_id FROM options)");

	    //update web_options
	    DB::statement("INSERT INTO web_options (web_options_id,sekcija_id,naziv,int_data,aplikacija_id) SELECT * FROM (VALUES (102,1,'Meni slike',1,100),(103,1,'Praćenje lagera',1,100),(104,1,'Newsletter',1,100),(105,1,'Mala korpa',1,100),(106,1,'Prikaz grid ili list view',1,100),(107,1,'Broj proizvoda po strani',1,100),(108,1,'Sortiranje proizvoda',1,100),(109,1,'Promena valute',1,100),(110,1,'Dozvoljeno filtriranje',1,100),(111,1,'Karakteristike poništavajuće',1,100),(112,1,'Nacin listanja kategorija',1,100),(114,1,'Fiksiran header',1,100),(115,1,'Prikaz kategorija',1,100),(116,1,'Sve kategorije',0,100),(117,1,'Uporedjenje artikala',0,100),(118,1,'Prikaz vezanih artikala',0,100),(119,1,'Flag za cenu kod vezanih artikala',0,100),
	        (120,1,'Prikaz dodatnih fajlova',0,100),(121,1,'Konfigurator',0,100),(130,1,'B2B portal',1,100),(131,1,'Rezervacija',0,100),(132,1,'Cena u prodavnici (1-web_cena, 0-mpcena)',1,100),(133,1,'Prikaz težine artikla',0,100),(134,1,'Prikaz tagova',0,100),(135,1,'Dodatni statusi',0,100),(136,1,'Prikaz svih artikala na pocetnoj',0,100)) redovi(web_options_id,sekcija_id,naziv,int_data,aplikacija_id) 
	        WHERE web_options_id NOT IN (SELECT web_options_id FROM web_options)");

	    // update ac_module
	    DB::statement("SELECT addcol('public','ac_module','aktivan','smallint DEFAULT 0')");
	    DB::statement("INSERT INTO ac_module (ac_module_id,aplikacija_id,modul,sifra,opis,tip,rbr,parrent_ac_module_id,aktivan) SELECT * FROM 
	        (VALUES (453,100,'frmImport','Prepisiwebcenu1','Preuzmidodatniopis1',1,53,400,0),(254,100,'frmListaRobe','SkiniSpecijalnuponudu1','Skidanje vise artikala odjednom sa akcije',1,48,200,0),
	            (255,100,'frmListaRobe','Postavikarakteristike1','postavka karakteristika na vise artikala odjednom',1,49,200,1),(256,100,'frmListaRobe','HTML1','postavka karakteristika na HTML za vise artikala odjednom',1,50,200,1),
	            (257,100,'frmListaRobe','Generisane1','postavka karakteristika na generisane za vise artikala odjednom',1,51,200,1),(258,100,'frmListaRobe','Oddobavljaca1','postavka karakteristika na generisane za vise artikala odjednom',1,52,200,1),
	            (5219,100,'frmVezani','frmVezani','Roba edit - vezani artikli',1,19,5200,1),(7000,100,'frmStranice','frmStranice','STRANICE',0,0,7000,1),
	            (7100,100,'frmBaneri','frmBaneri','BANERI',0,0,7100,1),(8000,100,'frmPodesavanja','frmPodesavanja','PODESAVANJA',0,0,8000,1),
	            (9000,100,'frmAnalitika','frmAnalitika','ANALITIKA',0,0,9000,1),(9500,100,'frmKonfigurator','frmKonfigurator','KONFIGURATOR',0,0,9500,1),(10000,100,'frmB2b','frmB2b','B2B',0,0,10000,1)
	        ) redovi(ac_module_id,aplikacija_id,modul,sifra,opis,tip,rbr,parrent_ac_module_id,aktivan) 
	        WHERE ac_module_id NOT IN (SELECT ac_module_id FROM ac_module)");
	    DB::statement("UPDATE ac_module SET aktivan = 1 WHERE ac_module_id IN (600,800,7000,1000,7100,8000,200,400,233,234,235,239,244,245,246,248,220,255,256,5200,5219,257,258,3200,3400,2400,9000,600,800,6600,1600,1800,2000,5600,5800,6000)");
	    DB::statement("INSERT INTO ac_group_module SELECT 0,ac_module_id,1,1 FROM ac_module WHERE aktivan = 1 AND ac_module_id NOT IN (SELECT ac_module_id FROM ac_group_module WHERE ac_group_id = 0)");

	    //update web_b2c_seo
	    DB::statement("SELECT addcol('public','web_b2c_seo','flag_page','smallint')");
	    DB::statement("SELECT addcol('public','web_b2c_seo','flag_b2b_show','smallint')");
	    DB::statement("SELECT addcol('public','web_b2c_seo','rb_strane','integer')");
	    DB::statement("SELECT addcol('public','web_b2c_seo','disable','integer DEFAULT 0')");
	    DB::statement("SELECT addcol('public','web_b2c_seo','tip_artikla_id','integer DEFAULT -1')");
	    DB::statement("SELECT addcol('public','web_b2c_seo','menu_top','integer DEFAULT 1')");
	    DB::statement("SELECT addcol('public','web_b2c_seo','header_menu','integer DEFAULT 1')");
	    DB::statement("SELECT addcol('public','web_b2c_seo','footer','integer DEFAULT 1')");
	    DB::statement("SELECT addcol('public','web_b2c_seo','b2b_header','integer DEFAULT 1')");
	    DB::statement("SELECT addcol('public','web_b2c_seo','b2b_footer','integer DEFAULT 1')");

	    DB::statement("SELECT setval('web_b2c_seo_web_b2c_seo_id_seq', COALESCE((SELECT MAX(web_b2c_seo_id)+1 FROM web_b2c_seo), 1), false)");

	    DB::statement("UPDATE web_b2c_seo SET disable = 0");
	    DB::statement("UPDATE web_b2c_seo SET disable = 1 WHERE naziv_stranice IN ('pocetna','kontakt','korpa','vesti','brendovi','konfigurator','registracija','pravno_lice','fizicko_lice','sve-kategorije','akcija','prijava','katalozi')");
	    DB::statement("INSERT INTO web_b2c_seo (naziv_stranice,title,menu_top,header_menu,footer,b2b_header,b2b_footer,disable,tip_artikla_id) SELECT * FROM (VALUES 
	    	('pocetna','Početna',0,0,0,0,0,1,-1),
	    	('kontakt','Kontakt',0,0,0,0,0,1,-1),
	    	('korpa','Korpa',0,0,0,0,0,1,-1),
	    	('blog','Blog',0,0,0,0,0,1,-1),
	    	('brendovi','Brendovi',0,0,0,0,0,1,-1),
	    	('konfigurator','Konfigurator',0,0,0,0,0,1,-1),
	    	('registracija','Registracija',0,0,0,0,0,1,-1),
	    	('pravno_lice','Pravno lice',0,0,0,0,0,1,-1),
	    	('fizicko_lice','Fizičko lice',0,0,0,0,0,1,-1),
	    	('sve-kategorije','Sve kategorije',0,0,0,0,0,1,-1),
	    	('akcija','Akcija',0,0,0,0,0,1,0),
	    	('prijava','Prijava',0,0,0,0,0,1,-1),
	    	('katalozi','Katalozi',0,0,0,0,0,1,-1)
	    	) redovi(naziv_stranice,title,menu_top,header_menu,footer,b2b_header,b2b_footer,disable,tip_artikla_id) 
	        WHERE naziv_stranice NOT IN (SELECT naziv_stranice FROM web_b2c_seo)");

	    //update preduzece
	    DB::statement("SELECT addcol('public','preduzece','logo','character varying(150)')");
	    DB::statement("SELECT addcol('public','preduzece','mapa','character varying(150)')");
	    DB::statement("SELECT addcol('public','preduzece','facebook','character varying(255)')");
	    DB::statement("SELECT addcol('public','preduzece','twitter','character varying(255)')");
	    DB::statement("SELECT addcol('public','preduzece','google_p','character varying(255)')");
	    DB::statement("SELECT addcol('public','preduzece','skype','character varying(255)')");
	    DB::statement("SELECT dropcol('public','preduzece','mesto_id')");
	    DB::statement("SELECT addcol('public','preduzece','mesto','character varying(255)')");
	    DB::statement("ALTER TABLE preduzece ALTER COLUMN facebook TYPE character varying(255)");
	    DB::statement("ALTER TABLE preduzece ALTER COLUMN twitter TYPE character varying(255)");
	    DB::statement("ALTER TABLE preduzece ALTER COLUMN google_p TYPE character varying(255)");
	    DB::statement("ALTER TABLE preduzece ALTER COLUMN skype TYPE character varying(255)");

	    // add table baneri
	    DB::statement("SELECT addtable('public','baneri','baneri_id bigserial NOT NULL, img character varying(150), link character varying(150), redni_broj integer, naziv character(255), tip_prikaza smallint, web_b2c_seo_id integer')");

	    // add table podrzan import
	    DB::statement("SELECT addtable('public','podrzan_import','podrzan_import_id smallint NOT NULL,naziv character varying(100),vrednost_kolone character varying(100),dozvoljen smallint DEFAULT 0')");
	    DB::statement("SELECT addcol('public','podrzan_import','file_upload','smallint DEFAULT 0')");
	    DB::statement("SELECT addcol('public','podrzan_import','file_type','character varying(10)')");

	    DB::statement("TRUNCATE podrzan_import");
	    DB::statement("INSERT INTO podrzan_import (podrzan_import_id,naziv,vrednost_kolone,file_upload,file_type,dozvoljen) SELECT * FROM (VALUES 
	    	(1,'Ewe','import_ewe',1,'xml',1),
	    	(2,'KimTec','import_kimtec',0,NULL,1),
	    	(3,'CT','import_ct',0,NULL,1),
	    	(4,'Computerland','import_computerland',1,'xml',1),
	    	(5,'PC Centar','import_pccentar',1,'xml',0),
	    	(6,'Tnt','import_tnt',1,'xml',0),
	    	(7,'Asbis','import_asbis',0,NULL,0),
	    	(8,'Cobra Toys','import_cobratoys',1,'xlsx',0),
	    	(9,'Westaco','import_westaco',1,'xlsx',0),
	    	(10,'Yuglob','import_yuglob',1,'xlsx',0),
	    	(11,'Kosa','import_kosa',1,'xlsx',0),
	    	(12,'Laluco','import_laluco',1,'xlsx',0),
	    	(13,'Happy dog','import_happydog',1,'xlsx',0),
	    	(14,'Xplorer','import_xplorer',1,'xml',0),
	    	(15,'Audio Planeta','import_audioplaneta',1,'xml',0),
	    	(16,'Agromarket','import_agromarket',1,'xlsx',0),
	    	(17,'Zorbal','import_zorbal',1,'xlsx',0),
	    	(18,'Atom Partner','import_atompartner',1,'xml',0),
	    	(19,'Bosh (excel)','import_bosh',1,'xlsx',0),
	    	(20,'Capriolo','import_capriolo',1,'xlsx',0),
	    	(21,'CT (excel)','import_ctexcel',1,'xlsx',0),
	    	(22,'CTC Unit','import_ctcunit',1,'xlsx',0),
	    	(23,'Digimax','import_digimax',1,'xlsx',0),
	    	(24,'DS','import_ds',1,'xlsx',0),
	    	(25,'Eurom Denis','import_euromdenis',1,'xml',0),
	    	(26,'Forma VS','import_formavs',1,'xlsx',0),
	    	(27,'Gume','import_gume',1,'xlsx',0),
	    	(28,'Keno','import_keno',1,'xlsx',0),
	    	(29,'Keprom','import_keprom',1,'xlsx',0),
	    	(30,'Kimtec (excel)','import_kimtecexcel',1,'xlsx',0),
	    	(31,'Mcg','import_mcg',1,'xlsx',0),
	    	(32,'Milenijum','import_milenijum',1,'xlsx',0),
	    	(33,'Mison','import_mison',1,'xlsx',0),
	    	(34,'Oxygen','import_oxygen',1,'xlsx',0),
	    	(35,'PC Centar (exel)','import_pccentarexcel',1,'xlsx',0),
	    	(36,'Pet Protector','import_petprotector',1,'xlsx',0),
	    	(37,'Pin Computers','import_pincomputers',1,'xml',1),
	    	(38,'Roaming','import_roaming',1,'xlsx',0),
	    	(39,'Sbalsan','import_sbalsan',1,'xlsx',0),
	    	(40,'Svetlost (MKA)','import_svetlostmka',1,'xlsx',0),
	    	(41,'Telit','import_telit',1,'xml',0),
	    	(42,'Ts Mod','import_tsmod',1,'xlsx',0),
	    	(43,'Vox','import_vox',1,'xlsx',0),
	    	(44,'Whirlpool','import_whirlpool',1,'xlsx',0),
	    	(45,'Woobyhouse','import_woobyhouse',1,'xlsx',0),
	    	(46,'Zoom Impex','import_zoomimpex',1,'xlsx',0),
	    	(47,'NT Company','import_ntcompany',1,'xml',0),
	    	(48,'Refot','import_refot',1,'xlsx',0),
	    	(49,'Virtikom','import_virtikom',1,'xlsx',0),
	    	(50,'Asbis (stari)','import_asbisold',1,'xml',0),
	    	(51,'CT (xml)','import_ctxml',1,'xml',0),
	    	(52,'Svetlost (VKA)','import_svetlostvka',1,'xlsx',0),
	    	(53,'Gorenje (MKA)','import_gorenjemka',1,'xlsx',0),
	    	(54,'Gorenje (VKA)','import_gorenjevka',1,'xlsx',0),
	    	(55,'IBG','import_ibg',1,'xml',0),
	    	(56,'Gembird','import_gembird',1,'xml',0),
	    	(57,'Josipovic','import_josipovic',1,'json',0),
	    	(58,'OFY','import_ofy',1,'xml',0),
	    	(59,'Venco','import_venco',1,'xlsx',0),
	    	(60,'HomeJungle','import_homejungle',1,'xlsx',0),
	    	(61,'Keller','import_keller',1,'xlsx',0),
	    	(62,'Gama','import_gama',1,'xml',0)
	    	) redovi(podrzan_import_id,naziv,vrednost_kolone,file_upload,file_type,dozvoljen) 
	        WHERE podrzan_import_id NOT IN (SELECT podrzan_import_id FROM podrzan_import)");

	    // add table vezani artikli
	    DB::statement("SELECT addtable('public','vezani_artikli','roba_id bigint NOT NULL,vezani_roba_id bigint NOT NULL,flag_cena smallint,CONSTRAINT vezani_artikli_fk1 FOREIGN KEY (roba_id) REFERENCES roba (roba_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,CONSTRAINT vezani_artikli_fk2 FOREIGN KEY (vezani_roba_id) REFERENCES roba (roba_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION')");

	    // add table vrsta fajla
	    DB::statement("SELECT addtable('public','vrsta_fajla','vrsta_fajla_id integer NOT NULL,ekstenzija character varying(25)')");
	    DB::statement("INSERT INTO vrsta_fajla (vrsta_fajla_id,ekstenzija) SELECT * FROM (VALUES (1,'PDF'),(2,'WORD'),(3,'EXCEL'),
	        (4,'VIDEO'),(5,'YOUTUBE'),(6,'SLIKA')) redovi(vrsta_fajla_id,ekstenzija) 
	        WHERE vrsta_fajla_id NOT IN (SELECT vrsta_fajla_id FROM vrsta_fajla)");

	    // add table konfigurator
	    DB::statement("SELECT addtable('public','konfigurator','konfigurator_id integer NOT NULL, naziv character varying(255),dozvoljen smallint DEFAULT 0')");
	    DB::statement("SELECT addtable('public','konfigurator_grupe','konfigurator_id integer NOT NULL, grupa_pr_id integer NOT NULL,rbr smallint DEFAULT 0')");

	    //add table lista_zelja
	    DB::statement("SELECT addtable('public','lista_zelja','web_kupac_id integer NOT NULL, roba_id integer')");
	    // add table export
	    DB::statement("SELECT addtable('public','export','export_id smallint NOT NULL,naziv character varying(255),vrednost_kolone character varying(100),dozvoljen smallint DEFAULT 0')");
	    DB::statement("SELECT addtable('public','roba_export','roba_id bigint NOT NULL,export_id smallint NOT NULL,aktivan smallint')");
	    DB::statement("SELECT dropcol('public','roba_export','aktivan')");
	    
	    //update export
	    DB::statement("INSERT INTO export (export_id,naziv,vrednost_kolone,dozvoljen) SELECT * FROM (VALUES (1,'IT Svet','it-svet',0),(2,'Pametno.rs','pametno-rs',0),(3,'Shopmania','shopmania',0),(4,'Magento','magento',0),(5,'Woocommerce','woocommerce',0),(6,'Shopify','shopify',0),(7,'Eponuda','eponuda',0),(8,'Kupindo','kupindo',0)) redovi(export_id,naziv,vrednost_kolone,dozvoljen) 
	        WHERE export_id NOT IN (SELECT export_id FROM export)");


	    // teme prodavnice
	    DB::statement("SELECT addtable('public','prodavnica_tema','prodavnica_tema_id integer NOT NULL,naziv character varying(25),putanja character varying(50),aktivna smallint')");
	    DB::statement("INSERT INTO prodavnica_tema (prodavnica_tema_id,naziv,putanja,aktivna) SELECT * FROM (VALUES 
	    	(1,'Simple shop','simple/',1), (2,'Modern shop','modern/',1), (3,'Cool','cool/',1), (4,'Awesome','awesome/',1), (5,'BS modern','bsmodern/',1)) 
	    	redovi(prodavnica_tema_id,naziv,putanja,aktivna) 
	        WHERE prodavnica_tema_id NOT IN (SELECT prodavnica_tema_id FROM prodavnica_tema)");

	    // stil prodavnice
	    DB::statement("SELECT addtable('public','prodavnica_stil','prodavnica_stil_id integer NOT NULL,prodavnica_tema_id integer NOT NULL,naziv character varying(25),putanja character varying(50),aktivna smallint,izabrana smallint,zakljucana smallint')");
	    DB::statement("INSERT INTO prodavnica_stil (prodavnica_stil_id,prodavnica_tema_id,naziv,putanja,aktivna,izabrana,zakljucana) SELECT * FROM (VALUES 
	    	(1,1,'Light Blue','light_blue/',1,1,0), (2,1,'Light Green','light_green/',1,0,0), 
	    	(3,1,'Light Orange','light_orange/',1,0,0), (4,1,'Dark Blue','dark_blue/',1,0,0), 
	    	(5,1,'Dark Green','dark_green/',1,0,0), (6,1,'Dark Orange','dark_orange/',1,0,0),
	    	(7,2,'Light','light/',1,0,0),(8,3,'Light','light/',1,0,0),(9,4,'Light','light/',1,0,0),
	    	(10,5,'Light','light/',1,0,0),(11,5,'Light Blue','light-blue/',1,0,0),(12,5,'Light Red','light-red/',1,0,0))
	    	redovi(prodavnica_stil_id,prodavnica_tema_id,naziv,putanja,aktivna,izabrana,zakljucana) 
	        WHERE prodavnica_stil_id NOT IN (SELECT prodavnica_stil_id FROM prodavnica_stil)");


	    //nacin placanja
	    DB::statement("DELETE FROM web_nacin_placanja WHERE naziv = 'PLATIMO servisom'");

	    //magacini
	    DB::statement("UPDATE orgj SET b2c = 0");
	    DB::statement("UPDATE orgj SET b2c = 1 WHERE orgj_id = 1");
	    DB::statement("UPDATE imenik_magacin SET orgj_id = 1 WHERE imenik_magacin_id = 10");
	    DB::statement("INSERT INTO imenik_magacin (imenik_magacin_id,imenik_id,aplikacija_id,orgj_id,izabrani) SELECT * FROM (VALUES (20,0,100,4,0)) redovi(imenik_magacin_id,imenik_id,aplikacija_id,orgj_id,izabrani) WHERE imenik_magacin_id NOT IN (SELECT imenik_magacin_id FROM imenik_magacin)");

	    DB::statement("DELETE FROM posta_slanje WHERE posta_slanje_id <> -1");
	    DB::statement("INSERT INTO posta_slanje (posta_slanje_id,naziv,aktivna) SELECT * FROM (VALUES (1,'Post Express',1), (2,'City Express',1), (3,'D Express',1), (4,'DHL Express',1), (5,'BEX - BALKAN EXPRESS',1), (6,'AKS EXPRESS',1), (7,'In Time Express',1), (8,'Team Express',1)) redovi(posta_slanje_id,naziv,aktivna) WHERE posta_slanje_id NOT IN (SELECT posta_slanje_id FROM posta_slanje)");

	    //brisanje tabele operacija
	    DB::statement("DROP TABLE IF EXISTS operacija_norma");
	    DB::statement("DROP TABLE IF EXISTS r_nalog_operacije_uradio");
	    DB::statement("DROP TABLE IF EXISTS r_nalog_operacije");

		//web_b2c_komentari
		DB::statement("SELECT addcol('public','web_b2c_komentari','ocena','smallint')");

		//osobine
	    DB::statement("SELECT addtable('public','osobina_naziv','osobina_naziv_id bigserial NOT NULL, naziv character varying(255) NOT NULL,aktivna smallint,color_picker smallint,rbr integer,CONSTRAINT osobina_naziv_pkey PRIMARY KEY (osobina_naziv_id)')");
	    DB::statement("SELECT addcol('public','osobina_naziv','prikazi_vrednost','smallint DEFAULT 1')");

	    DB::statement("SELECT addtable('public','osobina_vrednost','osobina_vrednost_id bigserial NOT NULL,osobina_naziv_id bigint NOT NULL, vrednost character varying(255) NOT NULL,aktivna smallint,boja_css character varying(20),rbr integer,CONSTRAINT osobina_vrednost_fk1 FOREIGN KEY (osobina_naziv_id) REFERENCES osobina_naziv (osobina_naziv_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION')");

		DB::statement("SELECT addtable('public','osobina_roba','roba_id bigint NOT NULL,osobina_naziv_id bigint NOT NULL,osobina_vrednost_id bigint NOT NULL,aktivna smallint,rbr integer,CONSTRAINT osobina_roba_fk1 FOREIGN KEY (roba_id) REFERENCES roba (roba_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,CONSTRAINT osobina_roba_fk2 FOREIGN KEY (osobina_naziv_id) REFERENCES osobina_naziv (osobina_naziv_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION')");

		//korpa osobine
		DB::statement("SELECT addcol('public','web_b2c_korpa','naruceno','smallint DEFAULT 0')");
		DB::statement("SELECT addcol('public','web_b2c_korpa_stavka','osobina_vrednost_ids','character varying(255)')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','osobina_vrednost_ids','character varying(255)')");

		//verzije prodavnice i admina
	    DB::statement("SELECT addtable('public','verzija_prodavnice','verzija_id bigint NOT NULL, prodavnica character varying(25), web_admin character varying(25), b2b_prodavnica character varying(25), b2b_admin character varying(25)')");
	    DB::statement("TRUNCATE verzija_prodavnice");
	    DB::statement("INSERT INTO verzija_prodavnice (verzija_id,prodavnica,web_admin,b2b_prodavnica,b2b_admin) SELECT * FROM (VALUES (1,'2.0.0','2.0.0','2.0.0','2.0.0')) redovi(verzija_id,prodavnica,web_admin,b2b_prodavnica,b2b_admin) WHERE verzija_id NOT IN (SELECT verzija_id FROM verzija_prodavnice)");

		//shopmania grupe
		// DB::statement("DROP TABLE shopmania_grupe");
	    DB::statement("SELECT addtable('public','shopmania_grupe','shopmania_grupa_id bigserial NOT NULL, naziv character varying(255), parent_id bigint NOT NULL, grupa_pr_ids character varying(255)')");
	    DB::statement("INSERT INTO shopmania_grupe (shopmania_grupa_id,naziv,parent_id,grupa_pr_ids) SELECT * FROM (VALUES (0,'Shopmania kategorije',-1,NULL)) redovi(shopmania_grupa_id,naziv,parent_id,grupa_pr_ids) WHERE shopmania_grupa_id NOT IN (SELECT shopmania_grupa_id FROM shopmania_grupe)");

		DB::statement("SELECT addtable('public','web_troskovi_isporuke','web_troskovi_isporuke_id bigint NOT NULL, tezina_gr numeric(20,2) DEFAULT 0.00, cena numeric(10,2) DEFAULT 0.00')");
	   
		// partner_rabat_grupa
		// DB::statement("DROP TABLE partner_rabat_grupa");
		// DB::statement("SELECT addtable('public', 'partner_rabat_grupa', 'partner_id integer NOT NULL, grupa_pr_id integer NOT NULL, rabat numeric(20,2), sifra_kupca_logic integer, sifra_kategorije_logic integer, proizvodjac_id integer, valuta integer, id bigserial NOT NULL, CONSTRAINT partner_rabat_grupa_pkey PRIMARY KEY (id)')");
		
		//update web_kupac//
		DB::statement("SELECT addcol('public','web_kupac','mesto','character varying(255)')");
		DB::statement("SELECT dropcol('public','web_kupac','mesto_id')");

		//web_b2c_narudzbina//
		DB::statement("SELECT addcol('public','web_b2c_narudzbina','narudzbina_status_id','integer')");
		DB::statement("SELECT addcol('public','web_b2b_narudzbina','narudzbina_status_id','integer')");
		//narudzbina_status 
		DB::statement("SELECT addtable('public','narudzbina_status','narudzbina_status_id integer NOT NULL, naziv character varying(150), selected integer DEFAULT 1')");

		//partneri
		DB::statement("SELECT addcol('public','partner','mesto','character varying(255)')");
		DB::statement("SELECT dropcol('public','partner','mesto_id')");
		DB::statement("SELECT addcol('public','partner','id_is','character varying(255) DEFAULT NULL')");

		//web_b2b_narudzbina
		DB::statement("SELECT addcol('public','web_b2b_narudzbina','sifra_is','character varying(255) DEFAULT NULL')");

		//web_b2b_kartica
		DB::statement("SELECT addcol('public','web_b2b_kartica','id_is','character varying(255) DEFAULT NULL')");

		//vrsta dokumenta
		DB::statement("INSERT INTO vrsta_dokumenta (vrsta_dokumenta_id,sifra_vd,naziv_vd,knjizi,parent_vrsta_dokumenta_id,naziv,vrsta_naloga_id,smer,pregled_dokumenata,markiranje,tabela,smer_markiranje) SELECT * FROM (VALUES (500,'WNB','Web nalog B2B',0,500,'Web nalog B2B',-1,0,1,1,'narudzbina',0),(501,'VNC','Web nalog B2C',0,501,'Web nalog B2C',-1,0,1,1,'narudzbina',0)) redovi(vrsta_dokumenta_id,sifra_vd,naziv_vd,knjizi,parent_vrsta_dokumenta_id,naziv,vrsta_naloga_id,smer,pregled_dokumenata,markiranje,tabela,smer_markiranje) WHERE vrsta_dokumenta_id NOT IN (SELECT vrsta_dokumenta_id FROM vrsta_dokumenta)");

		//prevodilac
	    DB::statement("SELECT addtable('public','jezik','jezik_id bigserial PRIMARY KEY, naziv character varying(20),kod character varying(20), aktivan smallint, izabrani smallint')");
	    DB::statement("INSERT INTO jezik (jezik_id,naziv,kod,aktivan,izabrani) SELECT * FROM (VALUES (1,'Srpski','sr',1,1)) redovi(jezik_id,naziv,kod,aktivan,izabrani) WHERE jezik_id NOT IN (SELECT jezik_id FROM jezik)");

	    DB::statement("SELECT addtable('public','prevodilac','prevodilac_id bigserial NOT NULL, izabrani_jezik_id integer, izabrani_jezik_reci character varying(1000), jezik_id integer, reci character varying(1000)')");

	    // add table web b2c seo jezik
	    DB::statement("SELECT addtable('public','web_b2c_seo_jezik','web_b2c_seo_id integer NOT NULL,jezik_id integer NOT NULL,sadrzaj text,title character varying(100),description character varying(400),keywords character varying(255), CONSTRAINT web_b2c_seo_jezik_pkey PRIMARY KEY (web_b2c_seo_id, jezik_id), CONSTRAINT web_b2c_seo_id_fkey FOREIGN KEY (web_b2c_seo_id) REFERENCES public.web_b2c_seo (web_b2c_seo_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT jezik_id_fkey FOREIGN KEY (jezik_id) REFERENCES public.jezik (jezik_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

	    // add table grupa jezik
	    DB::statement("SELECT addtable('public','grupa_pr_jezik','grupa_pr_id integer NOT NULL,jezik_id integer NOT NULL,sablon_opis text,title character varying(100),description character varying(400),keywords character varying(255), CONSTRAINT grupa_pr_jezik_pkey PRIMARY KEY (grupa_pr_id, jezik_id), CONSTRAINT grupa_pr_id_fkey FOREIGN KEY (grupa_pr_id) REFERENCES public.grupa_pr (grupa_pr_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT jezik_id_fkey FOREIGN KEY (jezik_id) REFERENCES public.jezik (jezik_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

	    // add table roba jezik
	    DB::statement("SELECT addtable('public','roba_jezik','roba_id integer NOT NULL,jezik_id integer NOT NULL,title character varying(100),description character varying(400),keywords character varying(255), CONSTRAINT roba_jezik_pkey PRIMARY KEY (roba_id, jezik_id), CONSTRAINT roba_id_fkey FOREIGN KEY (roba_id) REFERENCES public.roba (roba_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT jezik_id_fkey FOREIGN KEY (jezik_id) REFERENCES public.jezik (jezik_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

	    DB::statement("SELECT addtable('public','proizvodjac_jezik','proizvodjac_id integer NOT NULL,jezik_id integer NOT NULL,title character varying(100),description character varying(400),keywords character varying(255), CONSTRAINT proizvodjac_jezik_pkey PRIMARY KEY (proizvodjac_id, jezik_id), CONSTRAINT proizvodjac_id_fkey FOREIGN KEY (proizvodjac_id) REFERENCES public.proizvodjac (proizvodjac_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT jezik_id_fkey FOREIGN KEY (jezik_id) REFERENCES public.jezik (jezik_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

	    DB::statement("SELECT addtable('public','web_vest_b2c_jezik','web_vest_b2c_id integer NOT NULL,jezik_id integer NOT NULL,naslov character varying(100),sadrzaj text,title character varying(100),description character varying(400),keywords character varying(255), CONSTRAINT web_vest_b2c_jezik_pkey PRIMARY KEY (web_vest_b2c_id, jezik_id), CONSTRAINT web_vest_b2c_id_fkey FOREIGN KEY (web_vest_b2c_id) REFERENCES public.web_vest_b2c (web_vest_b2c_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT jezik_id_fkey FOREIGN KEY (jezik_id) REFERENCES public.jezik (jezik_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

	    // add validator
	    DB::statement("SELECT addtable('public','validator','validator_id bigserial PRIMARY KEY, kod character varying(30), aktivan smallint DEFAULT 1')");

	    DB::statement("SELECT addtable('public','validator_jezik','validator_id integer NOT NULL,jezik_id integer NOT NULL,poruka character varying(255), CONSTRAINT validator_jezik_pkey PRIMARY KEY (validator_id, jezik_id), CONSTRAINT validator_id_fkey FOREIGN KEY (validator_id) REFERENCES public.validator (validator_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT jezik_id_fkey FOREIGN KEY (jezik_id) REFERENCES public.jezik (jezik_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

	    DB::statement("INSERT INTO validator (validator_id,kod,aktivan) SELECT * FROM (VALUES 
	    	(1,'required',1),(2,'regex',1),(3,'between',1),(4,'email',1),(5,'exists',1),(6,'unique',1),(7,'alpha_num',1) ) redovi(validator_id,kod,aktivan) WHERE validator_id NOT IN (SELECT validator_id FROM validator)");
	    DB::statement("INSERT INTO validator_jezik (validator_id,jezik_id,poruka) SELECT * FROM (VALUES 
	    	(1,1,'Niste popunili polje!'),(2,1,'Uneli ste ne dozvoljene karaktere!'),(3,1,'Vaš sadzaj polja je prekratak ili predugačak!'),(4,1,'Uneli ste neispravnu e-mail adresu!'),(5,1,'Uneli ste pogrešnu e-mail adresu ili lozinku!'),(6,1,'E-mail adresa već postoji u bazi!'),(7,1,'Polje ne sme sadržati specijalne karaktere!')) redovi(validator_id,jezik_id,poruka) WHERE poruka NOT IN (SELECT poruka FROM validator_jezik)");

	    	    // add validator
	    DB::statement("SELECT addtable('public','prodavnica_boje','prodavnica_boje_id bigserial PRIMARY KEY, title character varying(50), naziv character varying(50), kod character varying(50), default_kod character varying(50), aktivan smallint DEFAULT 1')");

	    DB::statement("INSERT INTO prodavnica_boje (prodavnica_boje_id,title,naziv,kod,default_kod) SELECT * FROM (VALUES 
			(1,'Header','header_bg',null, '#ffffff'),
			(2,'Pre Header','top_menu_bg',null, '#000000'),
			(3,'Text Pre Header','top_menu_color',null, '#ffffff'),
			(4,'Menu','menu_bg',null, '#30323A'),
			(5,'Footer','footer_bg',null, '#30323A'),
			(6,'Tekst','body_color',null, '#222222'),
			(7,'Tekst paragrafa','paragraph_color',null, '#ffffff'),
			(8,'Pozadina','body_bg',null, '#ffffff'),
			(9,'Veliki naslovi','h2_color',null, '#30323A'),
			(10,'Mali naslovi','h5_color',null, '#ffffff'),
			(11,'Dugmeta','btn_bg',null, '#F8694A'),
			(12,'Hover dugmeta','btn_hover_bg',null, '#f74922'),
			(13,'Tekst dugmeta','btn_color',null, '#ffffff'),
			(14,'Linkovi','a_href_color',null, '#000000'),
			(15,'Kategorije 1','level_1_color',null, '#222222'),
			(16,'Kategorije 2','level_2_color',null, '#222222'),
			(17,'Pozadina kategorija','categories_title_bg',null, '#F8694A'),
			(18,'Pozadina kategorija 1','categories_level_1_bg',null, '#ffffff'),
			(19,'Pozadina kategorija 2','categories_level_2_bg',null, '#ffffff'),
			(20,'Artikal','product_bg',null, '#ffffff'),
			(21,'Artikal footer','product_bottom_bg',null, '#ffffff'),
			(22,'Artikal zvezdice','review_star_color',null, '#D13E3B'),
			(23,'Naziv artikla','product_title_color',null, '#30323A'),
			(24,'Cena artikla','article_product_price_color',null, '#D13E3B'),
			(25,'Akcijska cena','sale_action_price_bg',null, '#F8694A'),
			(26,'Tekst akcijske cene','sale_action_bg',null, '#30323A'),
			(27,'Tekst stare cene','product_old_price_color',null, '#F8694A'),
			(28,'Cena liste artikla','product_price_color',null, '#222222'),
			(29,'Login dugme','login_btn_bg',null, 'transparent'),
			(30,'Text login dugme','login_btn_color',null, '#000000'),
			(31,'Broj artikala u korpi','cart_number_bg',null, '#F8694A'),
			(32,'Pozadina liste artikala','inner_body_bg',null, '#ffffff'),
			(33,'Header liste artikala','product_list_bg',null, '#f5f5f5'),
			(34,'Tekst polja','input_bg_color',null, '#ffffff'),
			(35,'Breadcrumps','breadcrumb_bg',null, '#f5f5f5'),
			(36,'Valuta','currency_bg',null, '#ffffff'),
			(37,'Tekst valute','currency_color',null, '#000000'),
			(38,'Filteri','filters_bg',null, '#ffffff'),
			(39,'Paginacija','pagination_bg',null, '#ffffff'),
			(40,'Aktivna strana paginacije','pagination_bg_active',null, '#ffffff'),
			(41,'Tekst aktivne strane paginacije','pagination_color_active',null, '#337ab7'),
			(42,'Popust','discount_price_color',null, '#D13E3B'),
			(43,'Dodaj u korpu dugme','add_to_cart_btn_bg',null, '#F8694A'),
			(44,'Tekst dodaj u korpu dugmeta','add_to_cart_btn_color',null, '#ffffff'),
			(45,'Hover dodaj u korpu dugmeta','add_to_cart_btn_bg_hover',null, '#40323A'),
			(46,'Dugme nedostupno','add_to_cart_btn_bg_not_available',null, '#f79888'),
			(47,'Labele','label_color',null, '#222222'),
			(48,'Skrol dugme','scroll_top_bg',null, '#ffffff')

	    	) redovi(prodavnica_boje_id,title,naziv,kod,default_kod) WHERE prodavnica_boje_id NOT IN (SELECT prodavnica_boje_id FROM prodavnica_boje)");
		}
		Artisan::call('update:db');
	    return Redirect::to(AdminOptions::base_url().'admin');
	}

	function update_old(){
		//update_roba
		// DB::statement("ALTER TABLE roba ALTER COLUMN naziv_racun_18 TYPE character varying(50)");
		// DB::statement("ALTER TABLE roba ALTER COLUMN naziv_racun_32 TYPE character varying(50)");
		// DB::statement("ALTER TABLE roba ALTER COLUMN naziv_displej TYPE character varying(50)");

	    //update web_b2c_seo
	    DB::statement("SELECT addcol('public','web_b2c_seo','seo_title','character varying(255)')");
	    DB::statement("SELECT addcol('public','web_b2c_seo','flag_b2b_show','integer')");
		DB::statement("SELECT addcol('public','web_b2c_seo','web_content','text')");

		DB::statement("ALTER TABLE dobavljac_cenovnik ALTER COLUMN sifra_kod_dobavljaca TYPE character varying(350)");
		DB::statement("ALTER TABLE dobavljac_cenovnik_karakteristike ALTER COLUMN sifra_kod_dobavljaca TYPE character varying(350)");
		DB::statement("ALTER TABLE dobavljac_cenovnik_slike ALTER COLUMN sifra_kod_dobavljaca TYPE character varying(350)");

		return Redirect::to(AdminOptions::base_url().'admin');
	}

	function resolve_encoding(){
		$tables = array(
			"dobavljac_cenovnik" => array("naziv","opis","sifra_kod_dobavljaca","grupa", "podgrupa", "proizvodjac"),
			"dobavljac_cenovnik_karakteristike" => array("sifra_kod_dobavljaca","karakteristika_naziv","karakteristika_vrednost","karakteristika_grupa"),
			"dobavljac_cenovnik_slike" => array("sifra_kod_dobavljaca"),
			"roba" => array("naziv","naziv_web","web_opis","web_karakteristike","naziv_racun_18","naziv_racun_32","naziv_displej")
			);
		foreach($tables as $table => $table_col){
			foreach($table_col as $column){
				DB::statement("update ".$table." set ".$column." = convert_to(".$column.", 'WIN1250')");
				foreach(array("232"=>"š","360"=>"đ","350"=>"č","346"=>"ć","236"=>"ž") as $key => $val){
					DB::statement("update ".$table." set ".$column." = replace(".$column.", '".$key."', '".$val."') where ".$column." like '%".$key."%'");
					// DB::statement("update roba set naziv = replace(naziv, '\\', '')"); mora direktno u pg-adminu
				}
			}
		}
		return Redirect::to(AdminOptions::base_url().'admin');
	}
}