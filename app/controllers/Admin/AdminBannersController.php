<?php
use Service\TranslatorService;

class AdminBannersController extends Controller {

    public function banners_sliders($id,$type,$jezik_id=null){
        if(is_null($jezik_id)){
            $jezik_id = DB::table('jezik')->where(array('aktivan'=>1,'izabrani'=>1))->pluck('jezik_id');
        }
        $banners = DB::table('baneri')->where('tip_prikaza',1)->orderBy('redni_broj','asc')->get();
        $sliders = DB::table('baneri')->where('tip_prikaza',2)->orderBy('redni_broj','asc')->get();
        $popup = DB::table('baneri')->where('tip_prikaza',9)->orderBy('redni_broj','asc')->get();
        $lang_data = DB::table('baneri_jezik')->where(array('baneri_id'=>$id,'jezik_id'=>$jezik_id))->first();

        if(!($id != 0 && !is_null($lang_data))){
            $lang_data = (object) array('sadrzaj'=>'','naslov'=>'','nadnaslov'=>'','naslov_dugme'=>'','link'=>'','alt'=>'');
        }
        $od=DB::table('baneri')->where('baneri_id',$id)->pluck('datum_od');
        $do=DB::table('baneri')->where('baneri_id',$id)->pluck('datum_do'); 

        $data = array(
            "strana"=>'baneri-slajderi',
            "title"=> 'Baneri i slajderi',
            "id"=> $id,
            "jezik_id"=> $jezik_id,
            "type"=> $type,
            "banners"=> $banners,
            "sliders"=> $sliders,
            "popup"=> $popup,
            "datum_od"=> Input::get('datum_od') != 0 ? Input::get('datum_od'): $od,
            "datum_do"=> Input::get('datum_do') != 0 ? Input::get('datum_do'): $do,      
            "redni_broj" => DB::table('baneri')->where('tip_prikaza',1)->max('redni_broj')+1,
            "jezici" => DB::table('jezik')->where('aktivan',1)->get(),
            "item"=> $id != 0 ? DB::table('baneri')->where('baneri_id',$id)->first() : (object) array("baneri_id" => 0,"img" => "","link" => "#","link2" => "#","aktivan" => "","datum_do"=> $do,"datum_od"=>$od,"naziv" => "","tip_prikaza" => $type),
            "lang_data" => $lang_data
            );

        return View::make('admin/page',$data);
    }

    public function banner_edit(){
        $inputs = Input::get();
        $inputs['img'] = Input::file('img');

        $rules = array(
                'naziv' => 'required|regex:'.AdminSupport::regex().'|max:200',
                'link' => 'max:200',
                'naslov' => 'max:100',
                'nadnaslov' => 'max:100',
                'sadrzaj' => 'max:800',
                'alt' => 'max:255',
                );
        if($inputs['baneri_id'] == 0){
            $rules['img'] = 'required';
        }

        $validator = Validator::make($inputs, $rules,
            array(
                'required' => 'Niste popunili polje.',
                'regex' => 'Polje sadrži nedozvoljene karaktere.',
                'max' => 'Sadržaj polja je predugačak.',
                )
            );
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
            
            if(!is_null($inputs['datum_do']) && $inputs['datum_do'] != ''){
                $inputs['datum_do'] = date('Y-m-d',strtotime($inputs['datum_do']));
            }else{
                 $inputs['datum_do']=null;
            }
            if(!is_null($inputs['datum_od']) && $inputs['datum_od'] != ''){
                $inputs['datum_od'] = date('Y-m-d',strtotime($inputs['datum_od']));
            }else{
                 $inputs['datum_od']=null;
            }
            
            $max_image_size = 10000;
            $image = Input::file('img');

            $validator = Validator::make(
                array('img' => $image),
                array('img' => 'mimes:jpg,png,jpeg,gif,webp|max:'.strval($max_image_size)),
                array(
                    'required' => 'Niste izabrali fajl.',
                    'mimes' => 'Neodgovarajući format slike. Dozvoljeni formati su jpg, png, webp i jpeg.',
                    'max' => 'Maksimalna veličina slike je '.strval($max_image_size).'.',
                    )
                );
            if($validator->fails()){
                return Redirect::back()->withInput()->withErrors($validator->messages());
            }
            if($inputs['tip_prikaza'] == 1){
                $path = 'images/banners/';
            }elseif($inputs['tip_prikaza'] == 9){
                $path = 'images/banners/';
            }else{
              $path = 'images/slider/';  
            }


            $jezik_id = $inputs['jezik_id'];
            $redni_broj = isset($inputs['redni_broj']) ? $inputs['redni_broj'] : null;
            $naslov = isset($inputs['naslov']) ? $inputs['naslov'] : null;
            $nadnaslov = isset($inputs['nadnaslov']) ? $inputs['nadnaslov'] : null; 
            $sadrzaj = isset($inputs['sadrzaj']) ? $inputs['sadrzaj'] : null;
            $naslov_dugme = isset($inputs['naslov_dugme']) ? $inputs['naslov_dugme'] : null;
            $link = isset($inputs['link']) ? $inputs['link'] : null;
            $alt = isset($inputs['alt']) ? $inputs['alt'] : null;
            unset($inputs['jezik_id']);
            unset($inputs['naslov']);
            unset($inputs['nadnaslov']); 
            unset($inputs['sadrzaj']);
            unset($inputs['naslov_dugme']);
            unset($inputs['alt']);

            if($inputs['baneri_id'] == 0){
                unset($inputs['baneri_id']);
                unset($inputs['img']);

                DB::table('baneri')->insert($inputs);

                if($inputs['tip_prikaza'] == 1){
                AdminSupport::saveLog('BANERI_DODAJ', array(DB::table('baneri')->max('baneri_id')));
                }elseif($inputs['tip_prikaza'] == 9){
                AdminSupport::saveLog('POPUP_BANERI_DODAJ', array(DB::table('baneri')->where('tip_prikaza', 9)->pluck('baneri_id')));
                }elseif($inputs['tip_prikaza'] == 2){
                AdminSupport::saveLog('SLAJDERI_DODAJ', array(DB::table('baneri')->max('baneri_id')));  
                }

                $id = DB::select("SELECT currval('baneri_baneri_id_seq') as current_id")[0]->current_id;

                if($naslov != '' || $nadnaslov != '' || strip_tags($sadrzaj) != '' || $naslov_dugme != '' || $link != '' || $alt != ''){
                    DB::table('baneri_jezik')->insert(array('baneri_id'=>$id,'jezik_id'=>$jezik_id,'naslov'=>$naslov,'nadnaslov'=>$nadnaslov,'sadrzaj'=>$sadrzaj,'naslov_dugme'=>$naslov_dugme,'link'=>$link,'alt'=>$alt));
                    if($jezik_id==1){
                        foreach(DB::table('jezik')->where('aktivan',1)->where('jezik_id','!=',1)->orderBy('izabrani','desc')->get() as $jezik){
                            $translator = new TranslatorService(DB::table('jezik')->where('jezik_id',1)->pluck('kod'),$jezik->kod);
                            $jezik_data = array(
                                'baneri_id'=>$id,
                                'jezik_id'=>$jezik->jezik_id,
                                'naslov'=>$translator->translate($naslov),
                                'nadnaslov'=>$translator->translate($nadnaslov),
                                'sadrzaj'=>$translator->translate(html_entity_decode($sadrzaj)),
                                'alt'=>$translator->translate(html_entity_decode($alt)),
                                'naslov_dugme'=>$translator->translate($naslov_dugme)
                            );
                            DB::table('baneri_jezik')->insert($jezik_data);                 
                        }
                    }
                }
            }else{
                $id = $inputs['baneri_id'];
                unset($inputs['img']);
                unset($inputs['baneri_id']);
                unset($inputs['redni_broj']);
                DB::table('baneri')->where('baneri_id',$id)->update($inputs);

                if($naslov != '' || $nadnaslov != '' || strip_tags($sadrzaj) != '' || $naslov_dugme != '' || $link != ''){
                    if(is_null(DB::table('baneri_jezik')->where(array('baneri_id'=>$id,'jezik_id'=>$jezik_id))->first())){
                        DB::table('baneri_jezik')->insert(array('baneri_id'=>$id,'jezik_id'=>$jezik_id,'naslov'=>$naslov,'nadnaslov'=>$nadnaslov,'sadrzaj'=>$sadrzaj,'naslov_dugme'=>$naslov_dugme,'link'=>$link,'alt'=>$alt)); 
                    }else{
                        DB::table('baneri_jezik')->where(array('baneri_id'=>$id,'jezik_id'=>$jezik_id))->update(array('naslov'=>$naslov,'nadnaslov'=>$nadnaslov,'sadrzaj'=>$sadrzaj,'naslov_dugme'=>$naslov_dugme,'link'=>$link,'alt'=>$alt));
                    }
                }else{
                    DB::table('baneri_jezik')->where(array('baneri_id'=>$id,'jezik_id'=>$jezik_id))->delete();
                }  

                if($inputs['tip_prikaza'] == 1){
                    AdminSupport::saveLog('BANERI_IZMENI', array(DB::table('baneri')->max('baneri_id')));
                }elseif($inputs['tip_prikaza'] == 9){
                    AdminSupport::saveLog('POPUP_BANERI_IZMENI', array(DB::table('baneri')->max('baneri_id')));
                }elseif($inputs['tip_prikaza'] == 2){
                    AdminSupport::saveLog('SLAJDERI_IZMENI', array(DB::table('baneri')->max('baneri_id')));  
                }

            }

            if($image){
                $name = $id.'.'.$image->getClientOriginalExtension();
                $image->move($path, $name);
                DB::table('baneri')->where('baneri_id',$id)->update(array('img'=>$path.$name)); 
            }
            return Redirect::to(AdminOptions::base_url().'admin/baneri-slajdovi/'.$id.'/'.$inputs['tip_prikaza'].'/'.$jezik_id)->with('message','Uspešno ste sačuvali podatke.');
        }
    }
    public function bg_uload(){
        
        $slika_ime=$_FILES['bgImg']['name'];
        $slika_tmp_ime=$_FILES['bgImg']['tmp_name'];
        $naziv = Input::get('naziv');
        if( Input::get('link')){
            $link  = Input::get('link');
        }else{
             $link  = '#!';
        }
        if( Input::get('link2')){
            $link2  = Input::get('link2');
        }else{
             $link2  = '#!';
        }
        if ( !empty($slika_ime) ) {
            move_uploaded_file($slika_tmp_ime,"./images/upload/" . $slika_ime);

        $data = array(
            'naziv' => $naziv,
            'link' => $link,
            'link2' => $link2,
            'img' => "./images/upload/" . $slika_ime,
            'tip_prikaza' => 3
        );
        
        $log_ids_arr = DB::select("SELECT baneri_id from baneri where tip_prikaza = '3'");

        $count = DB::table('baneri')->where('tip_prikaza', 3)->count();

        if($count == 1){
            DB::table('baneri')->where('tip_prikaza', 3)->update($data);
        } else {
            DB::table('baneri')->where('tip_prikaza', 3)->insert($data);
        }          

        AdminSupport::saveLog('BANERI_SLIKA');
        return Redirect::back()->with('message','Uspešno ste dodali sliku.');
        }elseif(!empty($link) || empty($link)){
            $data = array(
                'naziv' => $naziv,
                'link' => $link,
                'link2' => $link2,
                'tip_prikaza' => 3
            );

            $count = DB::table('baneri')->where('tip_prikaza', 3)->count();

            if($count == 1){
                DB::table('baneri')->where('tip_prikaza', 3)->update($data);
            } else {
                DB::table('baneri')->where('tip_prikaza', 3)->insert($data);
            } 

            $log_ids_arr = DB::table('baneri')->max('baneri_id');

            AdminSupport::saveLog('BANERI_LINKOVI');

            return Redirect::back()->with('message','Uspešno ste dodali linkove.'); 
        }elseif(!empty($link2) || empty($link2)){
            $data = array(
                'naziv' => $naziv,
                'link' => $link,
                'link2' => $link2,
                'tip_prikaza' => 3
            );

            $count = DB::table('baneri')->where('tip_prikaza', 3)->count();

            if($count == 1){
                DB::table('baneri')->where('tip_prikaza', 3)->update($data);
            } else {
                DB::table('baneri')->where('tip_prikaza', 3)->insert($data);
            } 

            AdminSupport::saveLog('BANERI_LINKOVI', $log_ids_arr);

            return Redirect::back()->with('message','Uspešno ste dodali linkove.');
        }else{
            return Redirect::back()->with('message','Unesite podatke.');
        }
    }

    public function baneri_delete($id){
        $tip = DB::table('baneri')->where('baneri_id',$id)->pluck('tip_prikaza');
        if($tip == 1){
            AdminSupport::saveLog('BANERI_OBRISI', array(DB::table('baneri')->max('baneri_id')));
        }elseif($tip == 9){
            AdminSupport::saveLog('POPUP_BANERI_OBRISI', array(DB::table('baneri')->max('baneri_id')));
        }elseif($tip == 2){
            AdminSupport::saveLog('SLAJDERI_OBRISI', array(DB::table('baneri')->max('baneri_id')));  
        }
        DB::table('baneri')->where('baneri_id',$id)->delete();
        return Redirect::to(AdminOptions::base_url().'admin/baneri-slajdovi/0/1')->with('message','Uspešno ste obrisali sadržaj.');
    }

    public function bgImgDelete() {
        AdminSupport::saveLog('BANERI_SLIKA_OBRISI');
        DB::table('baneri')->where('tip_prikaza', 3)->delete();
        $log_ids_arr = DB::select("SELECT baneri_id from baneri where tip_prikaza = '3'");
        return Redirect::to('admin/baneri-slajdovi/0/1');
    }

}