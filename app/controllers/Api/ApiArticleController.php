<?php

class ApiArticleController extends Controller {

	public function articles(){
        $data = Input::get();
        $rules = array(
        	'page' => 'integer|min:1',
        	'limit' => 'integer|max:1000',
        	'id' => 'integer|exists:roba,roba_id',
        	'category_id' => 'integer|exists:grupa_pr,grupa_pr_id',
        	'brand_id' => 'integer|exists:proizvodjac,proizvodjac_id',
            'name' => 'regex:'.AdminSupport::regex().'|max:300',
            'external_codes' => 'array'
        );
        $validator = Validator::make($data,$rules);
        if($validator->fails()){
        	return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
        }

        $page = Input::get('page') ? intval(Input::get('page')) : 1;
        $limit = Input::get('limit') ? intval(Input::get('limit')) : 100;


		$articlesQuery = Article::with('category.parent','brand','type','vat','measure','stock.stockroom','mainImages.childs','comments');
		if(!is_null($id = Input::get('id'))){			
			$articlesQuery = $articlesQuery->where('roba_id',$id);
		}
		if(!is_null($categoryId = Input::get('category_id'))){
		    $categoryIds=array();
		    Groups::allGroups($categoryIds,$categoryId);			
			$articlesQuery = $articlesQuery->whereIn('grupa_pr_id',$categoryIds);
		}
		if(!is_null($brandId = Input::get('brand_id'))){			
			$articlesQuery = $articlesQuery->where('proizvodjac_id',$brandId);
		}
		if(!empty($name = Input::get('name'))){
			$articlesQuery = $articlesQuery->whereRaw("naziv ilike '%".$name."%'");
		}
		if(!empty($externalCodes = Input::get('external_codes'))){
			$subQueryArr = [];
			foreach($externalCodes as $externalCode){
				$subQueryArr[] = "'".$externalCode."'";
			}
			$subQuery = implode(',',$subQueryArr);

			$articlesQuery = $articlesQuery->whereRaw("id_is in (".$subQuery.")");
		}

		$articles = $articlesQuery->limit($limit)->offset((($page-1)*$limit))->get()->toArray();

		//populate
		$lang = DB::table('jezik')->where(array('aktivan'=>1, 'izabrani'=>1))->pluck('kod');
		$articles = array_map(function($article) use ($lang){
			$article['link'] = Options::base_url().Url_mod::slug_trans('artikal',$lang).'/'.Url_mod::slugify(Product::seo_title($article['id']));
			return $article;
		},$articles);


		return Response::json(['success'=>true,'products'=>$articles],200);
	}

	public function articlesSave(){
        $data = Input::get();
        $regex = AdminSupport::regex();

        if(count($data) > 20000){
        	return Response::json(['success'=>false,'messages'=>'Max input vars is 20000.'],200);
        }
        if(DB::table('roba')->count() > 50000){
        	return Response::json(['success'=>false,'messages'=>'Max number of articles is 50000.'],200);
        }

        foreach($data as $row){
	        $rules = array(
	        	'id' => 'integer|exists:roba,roba_id',
		    	'name' => ((isset($row['id']) || isset($row['external_code'])) ? '' : 'required|').'regex:'.$regex.'|max:300',
		    	'web_name' => ((isset($row['id']) || isset($row['external_code'])) ? '' : 'required|').'regex:'.$regex.'|max:300',
		    	'category_id' => 'integer|exists:grupa_pr,grupa_pr_id',
		    	'category_external_code' => 'max:255|exists:grupa_pr,sifra_is',
		    	'brand_name' => 'regex:'.$regex.'|max:100',
		    	'active' => 'in:0,1',
		    	'show' => 'in:0,1',
		    	'show_b2b' => 'in:0,1',
		    	'locked' => 'in:0,1',
		    	'type_name' => 'regex:'.$regex.'|max:200',
		    	'vat_value' => 'numeric|digits_between:1,7',
		    	'measure_name' => 'regex:'.$regex.'|max:30',
		    	'html_description' => 'max:5000',
		    	'basic_b2c_price' => 'numeric|digits_between:1,15',
		    	'basic_b2b_price' => 'numeric|digits_between:1,15',
		    	'web_price' => 'numeric|digits_between:1,15',
		    	'retail_price' => 'numeric|digits_between:1,15',
		    	'sale_price' => 'numeric|digits_between:1,15',
		    	'web_margin' => 'numeric|digits_between:1,7',
		    	'retail_margin' => 'numeric|digits_between:1,7',
		    	'tags_list' => 'regex:'.$regex.'|max:500',
		    	'external_code' => 'max:255',
		    	'images' => 'array',
		    	'stock' => 'array'
	        );
	        $validator = Validator::make($row,$rules);
	        if($validator->fails()){
	        	return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
	        	break;
	        }
	        if(isset($row['images'])){
	        	foreach($row['images'] as $image){
			        $imagesRules = array(
			        	'id' => 'integer|exists:web_slika,web_slika_id',
				    	'image_file' => 'mimes:jpg,png,jpeg,gif|max:10000',
				    	'active' => 'in:0,1',
				    	'default' => 'in:0,1',
			        );
			        $imageValidator = Validator::make($image,$imagesRules);
			        if($imageValidator->fails()){
			        	return Response::json(['success'=>false,'messages'=>$imageValidator->messages()],200);
			        	break;
			        }    		
	        	}
	        }
	        if(isset($row['stock'])){
	        	foreach($row['stock'] as $stock){
			        $stockRules = array(
				    	'stockroom_name' => 'required|max:100|in:RetailWarehouse,WholesaleWarehouse',
				    	'quantity' => 'required|numeric|digits_between:1,7',
				    	'reserved' => 'numeric|digits_between:1,7',
			        );
			        $stockValidator = Validator::make($stock,$stockRules);
			        if($stockValidator->fails()){
			        	return Response::json(['success'=>false,'messages'=>$stockValidator->messages()],200);
			        	break;
			        }    		
	        	}
	        }
        }



        $mappedCategories = Category::mappedByExternalCode();
        $mappedBrands = Brand::mappedByName();
        $mappedVats = Vat::mappedByName();
        $mappedTypes = Type::mappedByName();
        $mappedMeasureUnits = MeasureUnit::mappedByName();

        foreach($data as $row){
        	$article = null;
        	if(isset($row['id'])){
	        	$article = Article::where('roba_id',$row['id'])->first();
        	}
        	else if(isset($row['external_code'])){
	        	$article = Article::where('id_is',$row['external_code'])->first();
        	}
        	if(is_null($article)){
        		$article = new Article;
	        	$article->naziv = !empty($row['name']) ? $row['name'] : (isset($row['external_code']) ? $row['external_code'] : '');
	        	$article->naziv_web = !empty($row['naziv_web']) ? $row['naziv_web'] : (isset($row['external_code']) ? $row['external_code'] : '');
		    	$article->grupa_pr_id = isset($row['category_id']) ? $row['category_id'] : -1;
		    	$article->flag_aktivan = isset($row['active']) ? $row['active'] : 1;
		    	$article->flag_prikazi_u_cenovniku = isset($row['show']) ? $row['show'] : 0;
		    	$article->flag_cenovnik = isset($row['show_b2b']) ? $row['show_b2b'] : 0;
		    	$article->flag_zakljucan = isset($row['locked']) ? ($row['locked'] == 1) : false;
		    	$article->web_opis = isset($row['html_description']) ? $row['html_description'] : null;
		    	$article->racunska_cena_nc = isset($row['basic_b2c_price']) ? $row['basic_b2c_price'] : 0;
		    	$article->racunska_cena_end = isset($row['basic_b2b_price']) ? $row['basic_b2b_price'] : 0;
		    	$article->web_cena = isset($row['web_price']) ? $row['web_price'] : 0;
		    	$article->mpcena = isset($row['retail_price']) ? $row['retail_price'] : 0;
		    	$article->akcijska_cena = isset($row['sale_price']) ? $row['sale_price'] : 0;
		    	$article->web_marza = isset($row['web_margin']) ? $row['web_margin'] : 0;
		    	$article->mp_marza = isset($row['retail_margin']) ? $row['retail_margin'] : 0;
		    	$article->tags = isset($row['tags_list']) ? $row['tags_list'] : null;
        	}else{
        		if(isset($row['name'])){
		    		$article->naziv = $row['name'];
        		}
        		if(isset($row['web_name'])){
		    		$article->naziv_web = $row['web_name'];
        		}
        		if(isset($row['category_id'])){
		    		$article->grupa_pr_id = $row['category_id'];
        		}
        		if(isset($row['active'])){
		    		$article->flag_aktivan = $row['active'];
        		}
        		if(isset($row['show'])){
		    		$article->flag_prikazi_u_cenovniku = $row['show'];
        		}
        		if(isset($row['show_b2b'])){
		    		$article->flag_cenovnik = $row['show_b2b'];
        		}
        		if(isset($row['locked'])){
		    		$article->flag_zakljucan = $row['locked'];
        		}
        		if(isset($row['html_description'])){
		    		$article->web_opis = $row['html_description'];
        		}
        		if(isset($row['basic_b2c_price'])){
		    		$article->racunska_cena_nc = $row['basic_b2c_price'];
        		}
        		if(isset($row['basic_b2b_price'])){
		    		$article->racunska_cena_end = $row['basic_b2b_price'];
        		}
        		if(isset($row['web_price'])){
		    		$article->web_cena = $row['web_price'];
        		}
        		if(isset($row['retail_price'])){
		    		$article->mpcena = $row['retail_price'];
        		}
        		if(isset($row['sale_price'])){
		    		$article->akcijska_cena = $row['sale_price'];
        		}
        		if(isset($row['web_margin'])){
		    		$article->web_marza = $row['web_margin'];
        		}
        		if(isset($row['retail_margin'])){
		    		$article->mp_marza = $row['retail_margin'];
        		}
        		if(isset($row['tags_list'])){
		    		$article->tags = $row['tags_list'];
        		}     		
        	}

			if(isset($row['external_code'])){
	        	$article->id_is = $row['external_code'];
	        	$article->sifra_is = $row['external_code'];
        	}


	    	if(!isset($row['category_id']) && isset($row['category_external_code'])){
	    		$article->grupa_pr_id = Category::getIdByCode($row['category_external_code'],$mappedCategories);
	    	}
	    	if(isset($row['brand_name'])){
	    		$article->proizvodjac_id = Brand::getIdByName($row['brand_name'],$mappedBrands);
	    	}
	    	if(isset($row['vat_value'])){
	    		$article->tarifna_grupa_id = Vat::getIdByName($row['vat_value'],$mappedVats);
	    	}
	    	if(isset($row['type_name'])){
	    		$article->tip_cene = Type::getIdByName($row['type_name'],$mappedTypes);
	    	}
	    	if(isset($row['measure_name'])){
	    		$article->jedinica_mere_id = MeasureUnit::getIdByName($row['measure_name'],$mappedMeasureUnits);
	    	}

        	$article->save();

        	//IMAGES
	        if(isset($row['images'])){
	        	foreach($row['images'] as $image){
					if(isset($image['id'])){
						if(is_null($dbImage = Images::where(['web_slika_id' => $image['id'], 'roba_id' => $article->roba_id])->first())){
							continue;
						}
					}else{
				        $image['id'] = DB::select("SELECT nextval('web_slika_web_slika_id_seq')")[0]->nextval;
						$dbImage = new Images;
						$dbImage->web_slika_id = $image['id'];
						$dbImage->roba_id = $article->roba_id;
					}
			        $dbImage->akcija = isset($image['default']) ? $image['default'] : 0;
			        $dbImage->flag_prikazi = isset($image['active']) ? $image['active'] : 1;

			        if(isset($image['image_file'])){		        	
			        	$extension = $image['image_file']->getClientOriginalExtension();
				        $name = $image['id'].'.'.$extension;
				        $dbImage->putanja = 'images/products/big/'.$name;
				        AdminArticles::saveImageFile($image['image_file'],$name);
			        }

			        $dbImage->save(); 
	        	}
	        }

        	//STOCK
	        if(isset($row['stock'])){
	        	foreach($row['stock'] as $stock){
			    	if(isset($stock['stockroom_name'])){
			    		$stockroomId = 1;
			    		if($stock['stockroom_name'] == 'RetailWarehouse'){
			    			$stockroomId = 1;
			    		}else if($stock['stockroom_name'] == 'WholesaleWarehouse'){
			    			$stockroomId = 4;
			    		}

			    		$dbStock=Stock::where(['orgj_id'=>$stockroomId, 'roba_id'=>$article->roba_id, 'poslovna_godina_id'=>2014])->first();
			    		if(is_null($dbStock)){
			    			$dbStock = new Stock();
			    			$dbStock->orgj_id = $stockroomId;
			    			$dbStock->roba_id = $article->roba_id;
			    			$dbStock->poslovna_godina_id = 2014;
				    		$dbStock->kolicina = isset($stock['quantity']) ? $stock['quantity'] : 0;
				    		$dbStock->rezervisano = isset($stock['reserved']) ? $stock['reserved'] : 0;
				    		$dbStock->save();
			    		}else{
			    			$updateData = [
			    				'kolicina' => isset($stock['quantity']) ? $stock['quantity'] : 0,
			    				'rezervisano' => isset($stock['reserved']) ? $stock['reserved'] : 0
			    			];
			    			DB::table('lager')->where(['orgj_id'=>$stockroomId, 'roba_id'=>$article->roba_id, 'poslovna_godina_id'=>2014])->update($updateData);
			    		}
			    	}
	        	}
	        }

	    }

        return Response::json(['success'=>true],200);
	}

    public function articlesDelete(){
        $data = Input::get();

        if(count($data) > 20000){
            return Response::json(['success'=>false,'messages'=>'Max input vars is 20000.'],200);
        }

        if(!isset($data[0])){
            return Response::json(['success'=>false,'messages'=>'Array of inputs is required.'],200);
        }

        if(isset($data[0]['id'])){
            foreach($data as $row){
                $rules = array(
                    'id' => 'required|integer'
                );
                $validator = Validator::make($row,$rules);
                if($validator->fails()){
                    return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
                    break;
                }
            }
        }
        else if(isset($data[0]['external_code'])){
            foreach($data as $row){
                $rules = array(
                    'external_code' => 'required|max:255'
                );
                $validator = Validator::make($row,$rules);
                if($validator->fails()){
                    return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
                    break;
                }
            }
        }else{
            return Response::json(['success'=>false,'messages'=>'Element require id or external_code.'],200);
        }


        if(isset($data[0]['id'])){
            $ids = array_map(function($item){ return $item['id']; },$data);
            DB::table('roba')->whereIn('roba_id',$ids)->delete();
        }
        else if(isset($data[0]['external_code'])){
            $codes = array_map(function($item){ return $item['external_code']; },$data);
            DB::table('roba')->whereIn('id_is',$codes)->delete();
        }

        return Response::json(['success'=>true],200);
    }

}



// $curl = curl_init();

// curl_setopt_array($curl, array(
//   CURLOPT_URL => "http://local.wbp/api/v1/articles-save",
//   CURLOPT_RETURNTRANSFER => true,
//   CURLOPT_CUSTOMREQUEST => "POST",
//   CURLOPT_POSTFIELDS =>"{\n    \"test\": \"test\"\n}",
//   CURLOPT_HTTPHEADER => array(
//     "Content-Type: application/json"
//   ),
// ));

// $response = curl_exec($curl);