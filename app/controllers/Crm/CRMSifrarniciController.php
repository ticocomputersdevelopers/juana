<?php
use Service\TranslatorService;

class CRMSifrarniciController extends Controller {

	//status sifrarnik
    public function crm_status($crm_status_id=null){
        $data=array(
                "strana"=>'crm_status',
                "title"=>$crm_status_id != 0 ? AdminCrm::getStatusNaziv($crm_status_id) : 'Novi status',
                "naziv"=>$crm_status_id != 0 ? AdminCrm::getStatusNaziv($crm_status_id) : null,
                "crm_status" => DB::table('crm_status')->orderBy('crm_status_id', 'asc')->get(),
                "active" => DB::table('crm_status')->where('crm_status_id',$crm_status_id)->pluck('flag_aktivan'),
                "crm_status_id" => $crm_status_id
             
            );

            return View::make('crm/page', $data); 
    }
    
    public function status_edit(){
        $inputs = Input::get();
        $query_result=false;
        $validator = Validator::make($inputs, array('naziv' => 'required|max:100'));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'crm/crm_status/'.$inputs['crm_status_id'])->withInput()->withErrors($validator->messages());
        }else
        {
             if($inputs['crm_status_id'] != 0){
                $query_result=DB::table('crm_status')->where('crm_status_id',$inputs['crm_status_id'])->update(array('naziv'=>$inputs['naziv'],'flag_aktivan'=>$inputs['flag_aktivan']));
                return Redirect::to(AdminOptions::base_url().'crm/crm_status/')->with('message','Uspešno ste izmenili status.');
             }
             else
             {
                $query_result=DB::table('crm_status')->insert(array('naziv'=>$inputs['naziv'],'flag_aktivan'=>$inputs['flag_aktivan']));
                return Redirect::to(AdminOptions::base_url().'crm/crm_status/')->with('message','Uspešno ste dodali status.');
            }
        }
    }
    function status_delete($crm_status_id)
    { 
        DB::table('crm_status')->where('crm_status_id',$crm_status_id)->delete();
       return Redirect::to(AdminOptions::base_url().'crm/crm_status/')->with('message','Uspešno ste obrisali tip.');
    }

//tip sifrarnik
    public function crm_tip($crm_tip_id=null){ 
        $data=array(
                "strana"=>'crm_tip',
                "title"=>$crm_tip_id != 0 ? AdminCrm::getTipNaziv($crm_tip_id) : 'Novi tip',
                "naziv"=>$crm_tip_id != 0 ? AdminCrm::getTipNaziv($crm_tip_id) : null,
                "crm_tip" => DB::table('crm_tip')->orderBy('crm_tip_id', 'asc')->get(),
                "active" => DB::table('crm_tip')->where('crm_tip_id',$crm_tip_id)->pluck('flag_aktivan'),
                "crm_tip_id" => $crm_tip_id
             
            );

            return View::make('crm/page', $data); 
    }
    public function tip_edit(){
        $inputs = Input::get();
        $query_result=false;
        $validator = Validator::make($inputs, array('naziv' => 'required|max:100'));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'crm/crm_tip/'.$inputs['crm_tip_id'])->withInput()->withErrors($validator->messages());
        }else
        {
            if($inputs['crm_tip_id'] != 0){
                $query_result=DB::table('crm_tip')->where('crm_tip_id',$inputs['crm_tip_id'])->update(array('naziv'=>$inputs['naziv'],'flag_aktivan'=>$inputs['flag_aktivan']));
                return Redirect::to(AdminOptions::base_url().'crm/crm_tip/')->with('message','Uspešno ste izmenili tip.');
             }
             else
             {
                $query_result=DB::table('crm_tip')->insert(array('naziv'=>$inputs['naziv'],'flag_aktivan'=>$inputs['flag_aktivan']));
                return Redirect::to(AdminOptions::base_url().'crm/crm_tip/')->with('message','Uspešno ste dodali tip.');
             }
        
        }
    }
    function tip_delete($crm_tip_id)
    { 
        DB::table('crm_tip')->where('crm_tip_id',$crm_tip_id)->delete();
       return Redirect::to(AdminOptions::base_url().'crm/crm_tip/')->with('message','Uspešno ste obrisali tip.');
    }

// akcija tip sifrarnik
    public function crm_sifrarnik($crm_akcija_tip_id=null){
        $data=array(
                "strana"=>'crm_sifrarnik',
                "title"=>$crm_akcija_tip_id != 0 ? AdminCrm::getAkcija($crm_akcija_tip_id) : 'Nova akcija',
                "naziv"=>$crm_akcija_tip_id != 0 ? AdminCrm::getAkcija($crm_akcija_tip_id) : null,
                "crm_akcija_tip" =>DB::table('crm_akcija_tip')->orderBy('crm_akcija_tip_id', 'asc')->get(),
                "crm_akcija_tip_id" => $crm_akcija_tip_id,
                "active"=> $crm_akcija_tip_id != 0 ? AdminCrm::getAkcijaActive($crm_akcija_tip_id) : null,
                "rbr"=> $crm_akcija_tip_id != 0 ? AdminCrm::getAkcijaRBR($crm_akcija_tip_id) : null
            );

            return View::make('crm/page', $data); 
    }

    public function sifrarnik_edit(){
        $inputs = Input::get();
        //All::dd($inputs);
        $rbr= Input::get('rbr');
        $query_result=false;
        $validator = Validator::make($inputs, array('naziv' => 'required|max:50', 'rbr' => 'numeric|digits_between:1,10'));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'crm/crm_sifrarnik/'.$inputs['crm_akcija_tip_id'])->withInput()->withErrors($validator->messages());
        }else
        {
            if($inputs['crm_akcija_tip_id'] != 0){
                if($rbr==null)
                {   
                     DB::table('crm_akcija_tip')->where('crm_akcija_tip_id',$inputs['crm_akcija_tip_id'])->update(array('naziv'=>$inputs['naziv'],'flag_aktivan'=>$inputs['flag_aktivan'],'rbr'=>null));
                     return Redirect::to(AdminOptions::base_url().'crm/crm_sifrarnik/')->with('message','Uspešno ste izmenili akciju.');

                }
                DB::table('crm_akcija_tip')->where('crm_akcija_tip_id',$inputs['crm_akcija_tip_id'])->update(array('naziv'=>$inputs['naziv'],'flag_aktivan'=>$inputs['flag_aktivan'],'rbr'=>$rbr));
                return Redirect::to(AdminOptions::base_url().'crm/crm_sifrarnik/')->with('message','Uspešno ste izmenili akciju.');
            }
            else {
                if($rbr==null)
                    {
                $query_result=DB::table('crm_akcija_tip')->insert(array('naziv'=>$inputs['naziv'],'flag_aktivan'=>$inputs['flag_aktivan'],'rbr'=>null));
                return Redirect::to(AdminOptions::base_url().'crm/crm_sifrarnik/')->with('message','Uspešno ste dodali akciju.');
                    }
                else
                {
                $query_result=DB::table('crm_akcija_tip')->insert(array('naziv'=>$inputs['naziv'],'flag_aktivan'=>$inputs['flag_aktivan'],'rbr'=>$rbr));
                return Redirect::to(AdminOptions::base_url().'crm/crm_sifrarnik/')->with('message','Uspešno ste dodali akciju.');
                }
            }
        }   
    }
    public function akcija_tip_delete($crm_akcija_tip_id)
    { 
        DB::table('crm_akcija_tip')->where('crm_akcija_tip_id',$crm_akcija_tip_id)->delete();
       return Redirect::to(AdminOptions::base_url().'crm/crm_sifrarnik/')->with('message','Uspešno ste obrisali tip akcije.');
    }

     public function vrsta_kontakta($vrsta_kontakta_id=null){

        
        $data=array(
                "strana"=>'crm_kontakt_vrsta',
                "title"=> $vrsta_kontakta_id != 0 ? AdminCrm::getVrstaKontakta($vrsta_kontakta_id) : 'Nova vrsta',
                "naziv"=>$vrsta_kontakta_id != 0 ? AdminCrm::getVrstaKontakta($vrsta_kontakta_id) : null,
                "vrsta_kontakta" =>  DB::table('vrsta_kontakta')->orderBy('vrsta_kontakta_id', 'asc')->get(),
                "vrsta_kontakta_id"=> $vrsta_kontakta_id,
                "crm_kontakt_vrsta"=> $vrsta_kontakta_id
               
            );
        
            return View::make('crm/page', $data);
        
    }
    
    public function vrsta_kontakta_edit(){
        $inputs = Input::get();
        $query_result=false;
        //All::dd($inputs);
        $validator = Validator::make($inputs, array('naziv' => 'required|max:100'));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'crm/crm_kontakt_vrsta/'.$inputs['vrsta_kontakta_id'])->withInput()->withErrors($validator->messages());
        }else
        {
            if($inputs['vrsta_kontakta_id'] != null){
                DB::table('vrsta_kontakta')->where('vrsta_kontakta_id',$inputs['vrsta_kontakta_id'])->update(array('naziv'=>$inputs['naziv']));
                return Redirect::to(AdminOptions::base_url().'crm/crm_kontakt_vrsta/')->with('message','Uspešno ste izmenili vrstu.');
                $query_result=true;
             }
             elseif($inputs['vrsta_kontakta_id'] == null)
             {
                DB::table('vrsta_kontakta')->insert(array('naziv'=>$inputs['naziv']));
                return Redirect::to(AdminOptions::base_url().'crm/crm_kontakt_vrsta/')->with('message','Uspešno ste dodali vrstu.');
                $query_result=true;
                
             }
        
        }
    }
    public function vrsta_delete($vrsta_kontakta_id)
    { 
        DB::table('vrsta_kontakta')->where('vrsta_kontakta_id',$vrsta_kontakta_id)->delete();
       return Redirect::to(AdminOptions::base_url().'crm/crm_kontakt_vrsta/')->with('message','Uspešno ste obrisali tip.');
    }

    public function dodatni_kontakti()
    {
        $inputs = Input::get();
        //All::dd($inputs);
        
        $query_result=false;
        $validator_messages = array(
            'required'=>'Polje ne sme biti prazno!',
            'max'=>'Prekoračili ste maksimalnu vrednost polja!'
                               
                                );

        $validator_rules = array(
              
                'vrednost'=> 'required' 
            );
        
        $validator = Validator::make($inputs, $validator_rules, $validator_messages);

        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'crm/crm_home')->withInput()->withErrors($validator->messages());
        }
        else
        {  
            if($inputs['partner_kontakt_id']!=null)
            {
            $query_result=DB::table('partner_kontakt')->where('partner_kontakt_id',$inputs['partner_kontakt_id'])->update(array('vrsta_kontakta_id'=>$inputs['vrsta_kontakta_id'],'osoba'=>$inputs['osoba'],'vrednost'=>$inputs['vrednost'], 'napomena'=>$inputs['napomena']));
            }
            else
            {
            $query_result=DB::table('partner_kontakt')->insert(array('partner_id'=>$inputs['partner_id'],'vrsta_kontakta_id'=>$inputs['vrsta_kontakta_id'],'osoba'=>$inputs['osoba'],'vrednost'=>$inputs['vrednost'], 'napomena'=>$inputs['napomena']));
            }
            return Redirect::to(AdminOptions::base_url().'crm/crm_home/')->with('message','Uspešno ste sačuvali podatke.');
        }
    }


	
}