<?php

class AdminB2BController extends Controller {
    function index(){

        if(AdminB2BOptions::check_admin(array('B2B_NARUDZBINE')) && AdminB2BOptions::check_admin(array('B2B_NARUDZBINE_PREGLED'))){
        	return Redirect::to('admin/b2b/narudzbine/0/0/0/0/0');
        }
    	return View::make('adminb2b/pocetna',array('strana'=>'b2b_pocetna','title'=>'B2B Pocetna'));
    }
    function position(){
        $moved = Input::get('moved');
        $parent_id = Input::get('parent_id');
        $order_arr = Input::get('order');
        $log_ids_arr = array();

        foreach($order_arr as $key => $val){
            DB::table('web_b2b_seo')->where(array('parent_id'=>$parent_id, 'web_b2b_seo_id'=>$val))->update(array('rb_strane'=>$key));
            $log_ids_arr[] = $val;
        }

        AdminSupport::saveLog('B2B_STRANICE_POZICIJA', array($moved));
        
    }

    function login_to_portal(){
        Session::put('b2b_user_'.Options::server(),1);
        if(Admin_model::check_admin(array('DOKUMENTI','DOKUMENTI_PREGLED'))){ 
            Session::put('dokumenti_user_'.Options::server(),Session::get('b2c_admin'.AdminOptions::server()));
            Session::put('dokumenti_user_kind_'.Options::server(),'admin');
        }

        if(B2bOptions::info_sys('infograf')){
            Session::put('b2b_user_'.B2bOptions::server().'_discounts',array());
        }
        return Redirect::to(AdminB2BOptions::base_url().'b2b/pocetna');
    }
}