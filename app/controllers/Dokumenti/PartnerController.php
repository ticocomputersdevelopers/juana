<?php

class PartnerController extends Controller {

	public function partneri(){
        $params = array(
            'dokumenti' => '1',
            'parent_id' => !is_null($partner = DokumentiOptions::user('saradnik')) ? $partner->id : null
            );
        Input::merge($params);
        return App::make('AdminKupciPartneriController')->getPartneri();
	}

	public function partneriFilter(){
        $params = Input::get();
        $params['dokumenti'] = '1';
        Input::merge($params);
        return App::make('AdminKupciPartneriController')->partneriFilter();
	}

	public function partner($partner_id){
        $params = array(
            'dokumenti'=> '1',
            );
        Input::merge($params);
        return App::make('AdminKupciPartneriController')->getPartner($partner_id);
	}

	public function savePartner(){
        $params = Input::get();
        $params['dokumenti'] = '1';
        $params['parent_id'] = !is_null($partner = DokumentiOptions::user('saradnik')) ? $partner->id : null;
        Input::merge($params);
        return App::make('AdminKupciPartneriController')->savePartner($params['partner_id']);
	}

	public function deletePartner($partner_id,$confirm){
        $params = array(
            'dokumenti'=> '1',
            );
        Input::merge($params);
        return App::make('AdminKupciPartneriController')->deletePartner($partner_id,$confirm);
	}

}