<?php

use PHPExcel as Excel; 
use PHPExcel_IOFactory as IOFactory;

class Promobi {

    public static function createOrder($orderNumber,$cartItems,$note=''){
        $success = false;

        $partner_id = Session::get('b2b_user_'.B2bOptions::server());
        $partner = DB::table('partner')->where('partner_id',$partner_id)->whereNotNull('id_is')->first();

        if(!is_null($partner)){

			$store_data = self::createXml($orderNumber,$cartItems,$note);
			if($store_data->success){
				//ftp
	            $ftp_host = '94.130.104.140';
	            $ftp_user_name = 'manel';
	            $ftp_user_pass = '6SyomQ_SWoTb';
				$connect_it = ftp_connect($ftp_host);
				$login_result = ftp_login($connect_it,$ftp_user_name,$ftp_user_pass);
				ftp_pasv($connect_it, true);
				
				if (ftp_put( $connect_it, $store_data->ftp_store_path, $store_data->store_path, FTP_BINARY)){
				    $success = true;
				}
				ftp_close($connect_it);
			}

        }
        return $success;
    }

    public static function createXml($orderNumber,$cartItems,$note=''){
        $success = false;
        $store_path = '';
        $ftp_store_path = '';

        $partner_id = Session::get('b2b_user_'.B2bOptions::server());
        $partner = DB::table('partner')->where('partner_id',$partner_id)->whereNotNull('id_is')->first();
        if(!is_null($partner)){

			$xml = new DOMDocument("1.0","UTF-8");
			$narudzbina = $xml->createElement("narudzbina");
			$xml->appendChild($narudzbina);        	

			B2bCommon::xml_node($xml,"narudzbina_sifra",$orderNumber,$narudzbina);
			B2bCommon::xml_node($xml,"kupac_id",$partner->id_is,$narudzbina);
			B2bCommon::xml_node($xml,"datum",date('Y-m-d'),$narudzbina);
			B2bCommon::xml_node($xml,"napomena",$note,$narudzbina);

			$narudzbina_stavke = $xml->createElement("narudzbina_stavke");

			foreach($cartItems as $item){
        		$roba = DB::table('roba')->where('roba_id',$item->roba_id)->first();

        		if(!is_null($roba)){
					$stavka = $xml->createElement("narudzbina_stavka");

					B2bCommon::xml_node($xml,"ID",(!is_null($roba->id_is) && $roba->id_is != '' ? $roba->id_is : '-1'),$stavka);
					B2bCommon::xml_node($xml,"sifra",(!is_null($roba->sifra_is) && $roba->sifra_is != '' ? $roba->sifra_is : ''),$stavka);
					B2bCommon::xml_node($xml,"kolicina",$item->kolicina,$stavka);
					B2bCommon::xml_node($xml,"cena",$item->jm_cena,$stavka);
					B2bCommon::xml_node($xml,"pdv",DB::table('tarifna_grupa')->where('tarifna_grupa_id',$roba->tarifna_grupa_id)->pluck('porez'),$stavka);
					B2bCommon::xml_node($xml,"jedinica_mere",DB::table('jedinica_mere')->where('jedinica_mere_id',$roba->jedinica_mere_id)->pluck('naziv'),$stavka);
					B2bCommon::xml_node($xml,"broj_stavke",$item->broj_stavke,$stavka);
					B2bCommon::xml_node($xml,"opis",substr($roba->naziv,0,250),$stavka);

					$narudzbina_stavke->appendChild($stavka);
				}

			}

			$narudzbina->appendChild($narudzbina_stavke);
			$store_path = 'files/IS/xml/narudzbine/b2b/narudzbina'.$cartItems[0]->web_b2b_korpa_id.'.xml';

			$xml->formatOutput = true;
			$xml->save($store_path) or die("Error");
			$ftp_store_path = '/narudzbine/narudzbina'.$cartItems[0]->web_b2b_korpa_id.'.xml';
            
            $success = true;
        }

        return (object) array('success'=>$success,'store_path'=>$store_path,'ftp_store_path'=>$ftp_store_path);
    }

}