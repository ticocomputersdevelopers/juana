<?php

class B2b {

    public static function sumDuguje($items){
        $sum = 0;
        foreach($items->get() as $row){
            $sum+=$row->duguje;
        }
        return $sum;
    }

    public static function sumPotrazuje($items){
        $sum = 0;
        foreach($items->get() as $row){
            $sum+=$row->potrazuje;
        }
        return $sum;
    }

    public static function checkCategoryLevel($category_id){
        $categories = DB::table('grupa_pr')->where('parrent_grupa_pr_id',$category_id)->count();
        if($categories == 0){
            return true;
        }
        else {
            return false;
        }
    }

    public static function getManufacturers($grupa_pr_id, $filters){

        $filtersStr = "0";
        $br=0;
        $partner_id=Session::get('b2b_user_'.B2bOptions::server());
        foreach($filters as $key => $row){
            if( ($key != 'proizvodjac')){
                $filtersStr .= ",".implode(',',$row);
                $br++;
            }
        }
        if($filtersStr!='0'){
            $products="0";
            $webRobaKarakteristike = DB::select("SELECT wrk1.roba_id,  (SELECT count(roba_id) FROM web_roba_karakteristike WHERE grupa_pr_vrednost_id IN ( ".$filtersStr." ) AND roba_id=wrk1.roba_id)  as products  from web_roba_karakteristike wrk1
WHERE wrk1.roba_id > 0 AND wrk1.grupa_pr_vrednost_id IN (".$filtersStr.") GROUP BY wrk1.roba_id ",array());
            foreach($webRobaKarakteristike as $row){
                if($row->products == $br){
                    $products .= ",".$row->roba_id;
                }

            }
            $productsStr="AND r.roba_id IN (".$products.")";
        }
        else {
            $productsStr = "";
        }


        $manufacturers = DB::select("SELECT distinct(r.proizvodjac_id), p.naziv, count(r.roba_id) as products from roba r
                                                        LEFT JOIN (SELECT naziv, proizvodjac_id FROM proizvodjac) p ON r.proizvodjac_id = p.proizvodjac_id
                                                        WHERE r.proizvodjac_id NOT IN (SELECT proizvodjac_id FROM nivo_pristupa_proizvodjac WHERE partner_id = ".$partner_id." ) AND r.flag_aktivan = 1 AND r.flag_cenovnik = 1 AND r.grupa_pr_id = ? ".$productsStr." GROUP BY r.proizvodjac_id , p.naziv , p.proizvodjac_id order by products DESC ",array($grupa_pr_id));
        return $manufacturers;
    }




    public static function getCategoryFilters($grupa_pr_id, $filters){
        $filterValues=[];

        $proizStr = "";

        if(isset($filters['proizvodjac'])){
            $proizStr = " AND proizvodjac_id IN (".implode(",",$filters['proizvodjac']).") ";
        }

        $grupaPrNaziv = DB::table('grupa_pr_naziv')->where('grupa_pr_id',$grupa_pr_id)->where('active',1)->orderBy('rbr','asc')->get();
        foreach($grupaPrNaziv as $row) {

            $filterValues[] = [
                "grupa_id"=>$row->grupa_pr_naziv_id,
                "grupa_naziv"=>$row->naziv,
                "grupa_naziv_var"=>B2bUrl::filters_convert($row->naziv),
                "grupa_pr_vrednost"=>B2b::getCategoryFiltersValues($row->grupa_pr_naziv_id, B2bUrl::filters_convert($row->naziv), $filters, $proizStr, $grupa_pr_id)
            ];
        }
        return $filterValues;

    }


    private static function getCategoryFiltersValues($grupa_pr_naziv_id, $index, $filters, $proizStr, $grupa_pr_id){
        $filterValues=[];
        $filtersStr = "";
        $filtersStr2 = "0";
        $br=1;
        $productsOfManufacture = [];

        if(isset($filters['proizvodjac'])){
            $productsOfManufacture = DB::table('roba')->where('flag_aktivan',1)->where('flag_cenovnik',1)->whereIn('proizvodjac_id',$filters['proizvodjac'])->lists('roba_id');
        }
            foreach($filters as $key => $row){
                if( ($key != 'proizvodjac') AND ($key != $index)){
                    $filtersStr2 .= ",".implode(',',$row);
                    $br++;
                }
            }

		
		
        $grupaPrVrednost = DB::table('grupa_pr_vrednost')->where('grupa_pr_naziv_id',$grupa_pr_naziv_id)->where('active',1)->orderBy('rbr','asc')->get();
        foreach($grupaPrVrednost as $row){
           
			if($filtersStr2!='0'){

				$filtersStr = "grupa_pr_vrednost_id IN ( ".$filtersStr2.",".$row->grupa_pr_vrednost_id.")";

			}
			else{
				$filtersStr = "grupa_pr_vrednost_id IN ( ".$row->grupa_pr_vrednost_id.")";	

			}
                
            
            $filterValues[]= [
              "vrednost_id"=>$row->grupa_pr_vrednost_id,
              "vrednost_naziv"=>$row->naziv,
              "broj_proizvoda"=>B2b::countFilterProducts($filtersStr, $proizStr , $grupa_pr_id, $br, $productsOfManufacture)
            ];

        }

        return $filterValues;
    }

    private static function countFilterProducts($filtersStr, $proizStr, $grupa_pr_id, $br, $productsOfManufacture){
       $products = [0];
        if(count($productsOfManufacture)>0){
            $robaStr = " AND wrk1.roba_id IN (".implode(',',$productsOfManufacture).")";
        }
        else {
            $robaStr = "";
        }

        $webRobaKarakteristike = DB::select("SELECT wrk1.roba_id,  (SELECT count(roba_id) FROM web_roba_karakteristike WHERE ".$filtersStr." AND roba_id=wrk1.roba_id)  as products  from web_roba_karakteristike wrk1
WHERE wrk1.roba_id > 0 AND wrk1.".$filtersStr." ".$robaStr." GROUP BY wrk1.roba_id ",array());
        foreach($webRobaKarakteristike as $row){
            if($row->products == $br){
                $products[]=$row->roba_id;
            }

        }

        $roba = DB::select(" SELECT count(roba_id) as products FROM roba WHERE flag_aktivan = 1 AND flag_cenovnik = 1 AND grupa_pr_id = ".$grupa_pr_id." ".$proizStr." AND roba_id IN (".implode(',',$products).") ",array());
        foreach($roba as $row){
            return $row->products;
        }

    }

    public static function ip_adress(){
        $ip;
        if(getenv("HTTP_CLIENT_IP")){
            $ip = getenv("HTTP_CLIENT_IP");
        }
        else if(getenv("HTTP_X_FORWARDED_FOR")){
            $ip = getenv("HTTP_X_FORWARDED_FOR");
        }
        else if(getenv("REMOTE_ADDR")){
            $ip = getenv("REMOTE_ADDR");
        }
        else{
            $ip = "UNKNOWN";
        }
        return $ip;
    }

}