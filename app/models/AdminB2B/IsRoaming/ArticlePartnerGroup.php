<?php
namespace IsRoaming;

use DB;

class ArticlePartnerGroup {

	public static function table_body($article_partner_groups,$mappedArticles){
		$result_arr = array();

		foreach($article_partner_groups as $key => $article_partner_group) {
			if(isset($mappedArticles[strval($key)])){
				$roba_id = $mappedArticles[strval($key)];
				foreach($article_partner_group as $kategorija_partnera) {
					$id_kategorije = Support::getPartnerCategoryId($kategorija_partnera->naziv);
					$cena = $kategorija_partnera->cena;
					$avans_popust = $kategorija_partnera->popust;

					if(!is_null($roba_id) && !is_null($id_kategorije)){
						$result_arr[] = "(".strval($roba_id).",".strval($id_kategorije).",".strval($cena).",".strval($avans_popust).")";
					}
				}
			}
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba_partner_kategorija'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_partner_kategorija_temp(".implode(',',$columns).")";
		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="roba_id" && $col!="id_kategorije"){
		    	$updated_columns[] = "".$col." = roba_partner_kategorija_temp.".$col."";
			}
		}

		DB::statement("UPDATE roba_partner_kategorija rpk SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE roba_partner_kategorija_temp.roba_id = rpk.roba_id AND roba_partner_kategorija_temp.id_kategorije = rpk.id_kategorije");

		//insert
		DB::statement("INSERT INTO roba_partner_kategorija (SELECT * FROM ".$table_temp." WHERE (roba_partner_kategorija_temp.roba_id,roba_partner_kategorija_temp.id_kategorije) NOT IN (SELECT roba_id, id_kategorije FROM roba_partner_kategorija))");
	}

	public static function query_delete_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba_partner_kategorija'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_partner_kategorija_temp(".implode(',',$columns).")";

		DB::statement("DELETE FROM roba_partner_kategorija t WHERE (t.roba_id,t.id_kategorije) NOT IN (SELECT roba_id,id_kategorije FROM ".$table_temp.")");
	}

}