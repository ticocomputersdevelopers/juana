<?php
namespace IsXls;

use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class ReadFile {
	
	public static function articles($xlsx=false){

		$file_path = !$xlsx ? "files/roba.xls" : "files/roba.xlsx";		
		$success = false;
		$articles = array();
		if(File::exists($file_path)){

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($file_path);
	        $excelObj = $excelReader->load($file_path);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	   
	        for ($row = 2; $row <= $lastRow; $row++) {

				if(!is_null($worksheet->getCell('A'.$row)->getValue()) 
					&& !is_null($worksheet->getCell('B'.$row)->getValue()) 
					&& is_numeric($worksheet->getCell('L'.$row)->getValue())
					){ 
					$article = (object) array(
						'code' => trim((string) $worksheet->getCell('A'.$row)->getValue()),						
						'name' => trim((string) $worksheet->getCell('B'.$row)->getValue()),
						'jm' => !is_null($worksheet->getCell('D'.$row)->getValue()) ? trim((string) $worksheet->getCell('D'.$row)->getValue()) : '1',
						'tax' => !is_null($worksheet->getCell('C'.$row)->getValue()) ? trim((string) $worksheet->getCell('C'.$row)->getValue()) : '20',
						'main_group' => !is_null($worksheet->getCell('G'.$row)->getValue() ) ? trim((string) $worksheet->getCell('G'.$row)->getValue()) : '-1',
						'first_group' => !is_null($worksheet->getCell('F'.$row)->getValue() ) ? trim((string) $worksheet->getCell('F'.$row)->getValue()) : '-1',
						'second_group' => !is_null($worksheet->getCell('E'.$row)->getValue() ) ? trim((string) $worksheet->getCell('E'.$row)->getValue()) : '-1',
						'manufacturer' => !is_null($worksheet->getCell('H'.$row)->getValue()) ? trim((string) $worksheet->getCell('H'.$row)->getValue()) : '-1',
						'quantity' => !is_null($worksheet->getCell('O'.$row)->getValue()) && is_numeric(intval($worksheet->getCell('O'.$row)->getValue())) ? trim((string) $worksheet->getCell('O'.$row)->getValue()) : '0',
						'active' => (string) $worksheet->getCell('I'.$row)->getValue() == 'AKTIVAN' ? 1 : 0,
						'web_price' => trim((string) $worksheet->getCell('M'.$row)->getValue()),
						'mp_price' => trim((string) $worksheet->getCell('L'.$row)->getValue()),
						'dodatna_grupa1' => !is_null($worksheet->getCell('J'.$row)->getValue() ) ? trim((string) $worksheet->getCell('J'.$row)->getValue()) : '-1',
						'dodatna_grupa2' => !is_null($worksheet->getCell('K'.$row)->getValue() ) ? trim((string) $worksheet->getCell('K'.$row)->getValue()) : '-1',
						'velicina' => !is_null($worksheet->getCell('N'.$row)->getValue()) ? trim((string) $worksheet->getCell('N'.$row)->getValue()) : ''
						);
					

					$articles[] = $article;
				  //var_dump($article);die;
				}
			}
			if(count($articles) > 0){
				$success = true;
			}
		}
		return (object) array('articles'=>$articles, 'success'=>$success);
	}
	public static function groups($xlsx=false){
		$file_path = !$xlsx ? "files/roba.xls" : "files/roba.xlsx";		
		$success = false;
		
		$groups = array();
		$groups1 = array();

		$groups2 = array();
		$groups3 = array();

		if(File::exists($file_path)){
			// var_dump($file_path);die;
	        $excelReader = PHPExcel_IOFactory::createReaderForFile($file_path);
	        $excelObj = $excelReader->load($file_path);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	   
	        for ($row = 2; $row <= $lastRow; $row++) {

	        	$grupa1 = $worksheet->getCell('F'.$row)->getValue();
	        	//$grupa1 = str_replace(array('MUSKARCI','ZENE'), array('MUŠKARCI','ŽENE'), $grupa1)

	        	if( !in_array($grupa1, $groups1) ){
	        	$groups[]= (object) ['name'=>$grupa1, 'path' => $grupa1];
	        	$groups1[]= $grupa1;
	        	}
	        }
	        for ($row = 2; $row <= $lastRow; $row++) {

	        	$grupa1 = $worksheet->getCell('F'.$row)->getValue();
	        	$grupa2 = $worksheet->getCell('E'.$row)->getValue();

	        	if( !in_array($grupa1.'<=>'.$grupa2, $groups2) ){
	        	$groups[]= (object) ['name'=>$grupa2, 'path' => $grupa1.'<=>'.$grupa2];
	        	$groups2[]= $grupa1.'<=>'.$grupa2;
	        	}
	        }
	        for ($row = 2; $row <= $lastRow; $row++) {

	        	$grupa1 = $worksheet->getCell('F'.$row)->getValue();
	        	$grupa2 = $worksheet->getCell('E'.$row)->getValue();
	        	$grupa3 = $worksheet->getCell('G'.$row)->getValue();
	        	
	        	if( !in_array($grupa1.'<=>'.$grupa2.'<=>'.$grupa3, $groups3) ){
	        	$groups[]= (object) ['name'=>$grupa3, 'path' => $grupa1.'<=>'.$grupa2.'<=>'.$grupa3];
	        	$groups3[]= $grupa1.'<=>'.$grupa2.'<=>'.$grupa3;
	        	}
	        }
	    }
	    return $groups;
	}
	public static function dodatne_groupe1($xlsx=false){
		$file_path = !$xlsx ? "files/roba.xls" : "files/roba.xlsx";		
		$success = false;
		
		$groups = array();
		$groups1 = array();

		$groups2 = array();
		$groups3 = array();

		if(File::exists($file_path)){
			// var_dump($file_path);die;
	        $excelReader = PHPExcel_IOFactory::createReaderForFile($file_path);
	        $excelObj = $excelReader->load($file_path);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	   
	        for ($row = 2; $row <= $lastRow; $row++) {

	        	$grupa1 = $worksheet->getCell('J'.$row)->getValue();

	        	if( !in_array($grupa1, $groups1) ){
	        	$groups[]= (object) ['name'=>$grupa1, 'path' => $grupa1];
	        	$groups1[]= $grupa1;
	        	}
	        }
	        for ($row = 2; $row <= $lastRow; $row++) {

	        	$grupa1 = $worksheet->getCell('J'.$row)->getValue();
	        	$grupa2 = $worksheet->getCell('E'.$row)->getValue();

	        	if( !in_array($grupa1.'<=>'.$grupa2, $groups2) ){
	        	$groups[]= (object) ['name'=>$grupa2, 'path' => $grupa1.'<=>'.$grupa2];
	        	$groups2[]= $grupa1.'<=>'.$grupa2;
	        	}
	        }
	        for ($row = 2; $row <= $lastRow; $row++) {

	        	$grupa1 = $worksheet->getCell('J'.$row)->getValue();
	        	$grupa2 = $worksheet->getCell('E'.$row)->getValue();
	        	$grupa3 = $worksheet->getCell('G'.$row)->getValue();
	        	
	        	if( !in_array($grupa1.'<=>'.$grupa2.'<=>'.$grupa3, $groups3) ){
	        	$groups[]= (object) ['name'=>$grupa3, 'path' => $grupa1.'<=>'.$grupa2.'<=>'.$grupa3];
	        	$groups3[]= $grupa1.'<=>'.$grupa2.'<=>'.$grupa3;
	        	}
	        }
	    }
	    return $groups;
	}
	public static function dodatne_groupe2($xlsx=false){
		$file_path = !$xlsx ? "files/roba.xls" : "files/roba.xlsx";		
		$success = false;
		
		$groups = array();
		$groups1 = array();

		$groups2 = array();
		$groups3 = array();

		if(File::exists($file_path)){
			// var_dump($file_path);die;
	        $excelReader = PHPExcel_IOFactory::createReaderForFile($file_path);
	        $excelObj = $excelReader->load($file_path);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	   
	        for ($row = 2; $row <= $lastRow; $row++) {

	        	$grupa1 = $worksheet->getCell('K'.$row)->getValue();

	        	if( !in_array($grupa1, $groups1) ){
	        	$groups[]= (object) ['name'=>$grupa1, 'path' => $grupa1];
	        	$groups1[]= $grupa1;
	        	}
	        }
	        for ($row = 2; $row <= $lastRow; $row++) {

	        	$grupa1 = $worksheet->getCell('K'.$row)->getValue();
	        	$grupa2 = $worksheet->getCell('E'.$row)->getValue();

	        	if( !in_array($grupa1.'<=>'.$grupa2, $groups2) ){
	        	$groups[]= (object) ['name'=>$grupa2, 'path' => $grupa1.'<=>'.$grupa2];
	        	$groups2[]= $grupa1.'<=>'.$grupa2;
	        	}
	        }
	        for ($row = 2; $row <= $lastRow; $row++) {

	        	$grupa1 = $worksheet->getCell('K'.$row)->getValue();
	        	$grupa2 = $worksheet->getCell('E'.$row)->getValue();
	        	$grupa3 = $worksheet->getCell('G'.$row)->getValue();
	        	
	        	if( !in_array($grupa1.'<=>'.$grupa2.'<=>'.$grupa3, $groups3) ){
	        	$groups[]= (object) ['name'=>$grupa3, 'path' => $grupa1.'<=>'.$grupa2.'<=>'.$grupa3];
	        	$groups3[]= $grupa1.'<=>'.$grupa2.'<=>'.$grupa3;
	        	}
	        }
	    }
	    return $groups;
	}
}