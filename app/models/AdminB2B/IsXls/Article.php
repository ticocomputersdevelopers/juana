<?php
namespace IsXls;

use IsXls\Support;
use DB;

class Article {

	public static function table_body($articles){
		$result_arr = array();
		$code = array();

		$roba_id = DB::select("SELECT nextval('roba_roba_id_seq')")[0]->nextval;
		$sifra_k = DB::table('roba')->max('sifra_k')+1;
		$grupa_pr_id = -1;

		foreach($articles as $article) {
				
			if(!in_array($article->code, $code)){
	
				if(isset($article->code) AND !empty($article->code)){
				$roba_id++;
				$sifra_k++;
				$sifra_is = $article->code;
				$code[] =$article->code;
				$naziv = pg_escape_string(substr($article->name,0,300));
				$grupa_is=$article->first_group.'<=>'.$article->second_group.'<=>'.$article->main_group;
				// var_dump($grupa_is);die;

				$grupa_pr_id = Support::getGrupaId($grupa_is);
				$tarifna_grupa_id = Support::getTarifnaGrupaId($article->tax);
				$jedinica_mere_id = Support::getJedinicaMereId($article->jm);
				//$jedinica_mere_id = "1";
				$proizvodjac_id = Support::getProizvodjacId($article->manufacturer);
				//$racunska_cena_nc = intval($article->purchase_price);
				$mpcena = intval($article->mp_price); //(1+intval($article->porez)/100)
				$web_cena = $article->web_price < 100 ? $mpcena-($mpcena * $article->web_price/100) : $article->web_price;
				//$web_opis = pg_escape_string($article->description);
				//$web_karakteristike = pg_escape_string(Support::htmlCharacteristics($article->characteristics_groups));
				$active = $article->active;
				//$model = pg_escape_string($article->model);
				// $akcija = strpos($article->atributi, 'A') !== false ? 1 : 0;
				$akcija = 0;
				if(!empty($article->velicina)){
					$osobine = 1;
				}else{
					$osobine = 0;
				}
				
				if($article->quantity > 0 || !empty($article->velicina) || $article->velicina != 1 ){
					$flag_prikazi_u_cenovniku = 1;
				}else{
					$flag_prikazi_u_cenovniku = 0;
				}
				
				if($article->active == 1){
					$flag_prikazi_u_cenovniku = 1;
				}else{
					$flag_prikazi_u_cenovniku = 0;
				}
				
				$result_arr[] = "(".strval($roba_id).",NULL,'".$naziv."',NULL,NULL,NULL,".$grupa_pr_id.",".$tarifna_grupa_id.",".strval($jedinica_mere_id).",".strval($proizvodjac_id).",-1,".strval($sifra_k).",NULL,NULL,'".substr($naziv,0,20)."',0,-1,0,0,0,0,9,0,0,0,0,1,".$flag_prikazi_u_cenovniku.",0,NULL,1,0.00,0,0.00,0,NULL,".strval($mpcena).",false,0,(NULL)::integer,'".$naziv."',1,NULL,NULL,(NULL)::integer,(NULL)::integer,0,0,0,-1,-1,".strval($web_cena).",1,0,NULL,NULL,NULL,0,".strval($akcija).",0,NULL,NULL,NULL,NULL,1,0,NULL,0,0,1,1,-1,NULL,NULL,NULL,NULL,0,0.00,0.00,0.00,0,'".strval($sifra_is)."',(NULL)::date,(NULL)::date,".$osobine.",NULL,'".$sifra_is."','".$sifra_is."',0,0,0,(NULL)::date,(NULL)::date,0.00,0.00,1,NULL,(NULL)::integer)";
				}	
			}
		}

		return (object) array("body"=>implode(",",$result_arr),"codes"=>$code);
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {


		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";
		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		// update
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		$updated_columns_without_prices=array();
		foreach($columns as $col){
			if($col!="sifra_is"){
		    	$updated_columns[] = "".$col." = roba_temp.".$col."";
		    	if($col!="web_cena"){
		    		$updated_columns_without_prices[] = "".$col." = roba_temp.".$col."";
		    	}
			}
		}

		DB::statement("UPDATE roba t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE roba_temp.web_cena > 0 AND t.sifra_is=roba_temp.sifra_is::varchar");
		if(count($updated_columns_without_prices) > 0){
			DB::statement("UPDATE roba t SET ".implode(',',$updated_columns_without_prices)." FROM ".$table_temp." WHERE roba_temp.web_cena <= 0 AND t.sifra_is=roba_temp.sifra_is::varchar");
		}
		//insert
		DB::statement("INSERT INTO roba (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM roba t WHERE t.sifra_is=roba_temp.sifra_is::varchar))");



		DB::statement("SELECT setval('roba_roba_id_seq', (SELECT MAX(roba_id) FROM roba), FALSE)");
	}

	public static function query_update_unexists($table_temp_body) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("UPDATE roba t SET flag_prikazi_u_cenovniku = 0 WHERE t.sifra_is IS NOT NULL AND NOT EXISTS(SELECT * FROM ".$table_temp." WHERE t.sifra_is=roba_temp.sifra_is)");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}
	public static function table_body_partner($partners){
		$result_arr = array();	

		$partner_id = DB::select("SELECT nextval('partner_partner_id_seq')")[0]->nextval;
		$sifra = DB::table('partner')->max('sifra')+1;
		

		foreach($partners as $partner) {

			$partner_id++;
			$sifra++;

			$naziv=isset($partner->naziv) && $partner->naziv!='' ? pg_escape_string(substr(self::convert($partner->naziv),0,250)) : null;
	        $mesto=isset($partner->mesto) && $partner->mesto!='' ? pg_escape_string(substr(self::convert($partner->mesto),0,250)) : null;
	        $adresa=isset($partner->adresa) && $partner->adresa!='' ? pg_escape_string(substr(self::convert($partner->adresa),0,250)) : null;
	        $telefon=isset($partner->telefon) && $partner->telefon!='' ? pg_escape_string(substr($partner->telefon,0,250)) : null;
	        $pib=isset($partner->pib) && is_numeric(intval($partner->pib)) ? pg_escape_string(substr($partner->pib,0,100)) : null;
	        $mail=isset($partner->mail) && $partner->mail!='' ? pg_escape_string(substr($partner->mail,0,100)) : null;
	        $broj_maticni= isset($partner->maticni_broj) && is_numeric(intval($partner->maticni_broj)) ? pg_escape_string(substr($partner->maticni_broj,0,30)) : null;
	        $komercijalista=isset($partner->komercijalista) && $partner->komercijalista!='' ? pg_escape_string(substr($partner->komercijalista,0,100)) : null;


		        $result_arr[] = "(".strval($partner_id).",'".$sifra."','".trim($naziv)."','".trim($adresa)."','".trim($mesto)."',0,'".trim($telefon)."',NULL,'".strval($pib)."','".strval($broj_maticni)."',NULL,NULL,NULL,NULL,0,0,0,'".trim($naziv)."',0,0,1,1,0,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'".trim($mail)."',0,1,0,0,'".$komercijalista."',1,0,0,NULL,NULL,0)";
		//var_dump($result_arr);die;

		}

		return (object) array("body"=>implode(",",$result_arr));
	}
	public static function query_insert_update_partners($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='partner'"));
		$table_temp = "(VALUES ".$table_temp_body.") partner_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="partner_id" && $col!="id_is" && $col!="login" && $col!="password"){
		    	$updated_columns[] = "".$col." = partner_temp.".$col."";
			}
		}

		//insert
		DB::statement("INSERT INTO partner (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM partner t WHERE t.naziv=partner_temp.naziv))");

		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		DB::statement("SELECT setval('partner_partner_id_seq', (SELECT MAX(partner_id) FROM partner) + 1, FALSE)");
	}
	

	public static function convert($text){
	    $text = preg_replace("/[áàâãªä]/u","a",$text);
	    $text = preg_replace("/[ÁÀÂÃÄ]/u","A",$text);
	    $text = preg_replace("/[ÍÌÎÏ]/u","I",$text);
	    $text = preg_replace("/[íìîï]/u","i",$text);
	    $text = preg_replace("/[éèêë]/u","e",$text);
	    $text = preg_replace("/[ÉÈÊË]/u","E",$text);
	    $text = preg_replace("/[óòôõºö]/u","o",$text);
	    $text = preg_replace("/[ÓÒÔÕÖ]/u","O",$text);
	    $text = preg_replace("/[úùûü]/u","u",$text);
	    $text = preg_replace("/[ÚÙÛÜ]/u","U",$text);
	    $text = preg_replace("/[’‘‹›‚]/u","'",$text);
	    $text = preg_replace("/[“”«»„]/u",'"',$text);
	    $text = str_replace("–","-",$text);
	    $text = str_replace(" "," ",$text);
	    $text = str_replace("ç","c",$text);
	    $text = str_replace("Ç","C",$text);
	    $text = str_replace("ñ","n",$text);
	    $text = str_replace("Ñ","N",$text);
	 
	    //2) Translation CP1252. &ndash; => -
	    $trans = get_html_translation_table(HTML_ENTITIES); 
	    $trans[chr(130)] = '&sbquo;';    // Single Low-9 Quotation Mark 
	    $trans[chr(131)] = '&fnof;';    // Latin Small Letter F With Hook 
	    $trans[chr(132)] = '&bdquo;';    // Double Low-9 Quotation Mark 
	    $trans[chr(133)] = '&hellip;';    // Horizontal Ellipsis 
	    $trans[chr(134)] = '&dagger;';    // Dagger 
	    $trans[chr(135)] = '&Dagger;';    // Double Dagger 
	    $trans[chr(136)] = '&circ;';    // Modifier Letter Circumflex Accent 
	    $trans[chr(137)] = '&permil;';    // Per Mille Sign 
	    $trans[chr(138)] = '&Scaron;';    // Latin Capital Letter S With Caron 
	    $trans[chr(139)] = '&lsaquo;';    // Single Left-Pointing Angle Quotation Mark 
	    $trans[chr(140)] = '&OElig;';    // Latin Capital Ligature OE 
	    $trans[chr(145)] = '&lsquo;';    // Left Single Quotation Mark 
	    $trans[chr(146)] = '&rsquo;';    // Right Single Quotation Mark 
	    $trans[chr(147)] = '&ldquo;';    // Left Double Quotation Mark 
	    $trans[chr(148)] = '&rdquo;';    // Right Double Quotation Mark 
	    $trans[chr(149)] = '&bull;';    // Bullet 
	    $trans[chr(150)] = '&ndash;';    // En Dash 
	    $trans[chr(151)] = '&mdash;';    // Em Dash 
	    $trans[chr(152)] = '&tilde;';    // Small Tilde 
	    $trans[chr(153)] = '&trade;';    // Trade Mark Sign 
	    $trans[chr(154)] = '&scaron;';    // Latin Small Letter S With Caron 
	    $trans[chr(155)] = '&rsaquo;';    // Single Right-Pointing Angle Quotation Mark 
	    $trans[chr(156)] = '&oelig;';    // Latin Small Ligature OE 
	    $trans[chr(159)] = '&Yuml;';    // Latin Capital Letter Y With Diaeresis 
	    $trans['euro'] = '&euro;';    // euro currency symbol 
	    ksort($trans); 
	     
	    foreach ($trans as $k => $v) {
	        $text = str_replace($v, $k, $text);
	    }
	 
	    // 3) remove <p>, <br/> ...
	    $text = strip_tags($text); 
	     
	    // 4) &amp; => & &quot; => '
	    $text = html_entity_decode($text);
	     
	    // 5) remove Windows-1252 symbols like "TradeMark", "Euro"...
	    $text = preg_replace('/[^(\x20-\x7F)]*/','', $text); 
	     
	    $targets=array('\r\n','\n','\r','\t');
	    $results=array(" "," "," ","");
	    $text = str_replace($targets,$results,$text);
	     
		return ($text);
	}
}