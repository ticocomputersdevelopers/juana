<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);

use IsXml\FileData;
// use IsXml\DBData;
use IsXml\Article;
use IsXml\Stock;
use IsXml\Partner;
use IsXml\PartnerCard;
use IsXml\Support;



class AdminIsXml {

    public static function execute(){
        try {
            //partner
            $partners = FileData::partners();
            $resultPartner = Partner::table_body($partners);
            Partner::query_insert_update($resultPartner->body);

            //partner card
            $resultPartner = PartnerCard::table_body($partners);
            PartnerCard::query_insert_update($resultPartner->body);

            //articles
            $articles = FileData::articles();
            $groups = Support::filteredGroups($articles);
            Support::saveGroups($groups);

            $resultArticle = Article::table_body($articles);
            Article::query_insert_update($resultArticle->body,array('grupa_pr_id','web_opis','barkod','racunska_cena_nc','racunska_cena_end','mpcena','web_cena','proizvodjac_id','tarifna_grupa_id','flag_aktivan','flag_prikazi_u_cenovniku'));
            Article::query_update_unexists($resultArticle->body);

            $mapped_articles = Support::getMappedArticles();

            //b2b stock
            if(AdminB2BOptions::info_sys('xml')->b2b_magacin){
                $resultStock = Stock::table_body($articles,$mapped_articles,AdminB2BOptions::info_sys('xml')->b2b_magacin);
                Stock::query_insert_update($resultStock->body);
            }
            //b2c stock
            if(AdminB2BOptions::info_sys('xml')->b2c_magacin){
                $resultStock = Stock::table_body($articles,$mapped_articles,AdminB2BOptions::info_sys('xml')->b2c_magacin);
                Stock::query_insert_update($resultStock->body);
            }

            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);
        }catch (Exception $e){
            AdminB2BIS::saveISLog('false');
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }



}