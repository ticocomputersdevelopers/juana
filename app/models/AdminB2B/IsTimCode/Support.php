<?php
namespace IsTimCode;

use DB;
use finfo;
use Request;

class Support {

	public static function getJedinicaMereId($jedinica_mere){
		$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();

		if(is_null($jm)){
			DB::table('jedinica_mere')->insert(array(
				'jedinica_mere_id' => DB::table('jedinica_mere')->max('jedinica_mere_id')+1,
				'naziv' => $jedinica_mere
				));
			$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();
		}
		return $jm->jedinica_mere_id;
	}

	public static function getProizvodjacId($proizvodjac){
		$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();

		if(is_null($pro)){
			DB::table('proizvodjac')->insert(array(
				'naziv' => $proizvodjac,
				'sifra_connect' => 0
				));
			$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();
		}
		return $pro->proizvodjac_id;
	}
	public static function getStenjeArtiklaId($stanje){
		$roba_flag_cene = DB::table('roba_flag_cene')->where('naziv',$stanje)->first();

		if(is_null($roba_flag_cene)){
			DB::table('roba_flag_cene')->insert(array(
				'roba_flag_cene_id' => DB::table('roba_flag_cene')->max('roba_flag_cene_id') + 1,
				'naziv' => $stanje,
				'prikaz_cene' => 0
				));
			$roba_flag_cene = DB::table('roba_flag_cene')->where('naziv',$stanje)->first();
		}
		return $roba_flag_cene->roba_flag_cene_id;
	}
	public static function getMappedArticles(){
		$mapped = array();
		$articles = DB::table('roba')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($articles as $article){
			$mapped[trim(strval($article->id_is))] = $article->roba_id;
		}
		return $mapped;
	}
	public static function getMappedMAnufacturers(){
		$mapped = array();
		$manufacturers = DB::table('proizvodjac')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($manufacturers as $manufacturer){
			$mapped[trim(strval($manufacturer->id_is))] = $manufacturer->proizvodjac_id;
		}
		return $mapped;
	}
	public static function getMappedArticlesByCode(){
		$mapped = array();
		$articles = DB::table('roba')->whereNotNull('sifra_is')->where('sifra_is','!=','')->get();
		foreach($articles as $article){
			$mapped[trim(strval($article->sifra_is))] = $article->roba_id;
		}
		return $mapped;
	}


	public static function updateGroupsParent($groups,$internalGroupMapped){
		foreach($groups as $group){
			$group = $group->attributes;
			$group_id = isset($internalGroupMapped[$group->sifra]) ? intval($internalGroupMapped[$group->sifra]) : null;
			$parent_id = isset($group->nad) && $group->nad != 0 ? intval($group->nad) : 0;
			if(!is_null($group_id) && $parent_id != 0){
				$parrent_grupa_pr = DB::table('grupa_pr')->where('id_is',$parent_id)->first();
				if(!is_null($parrent_grupa_pr)){
					$parrent_grupa_pr_id = $parrent_grupa_pr->grupa_pr_id;
					// $parrent_grupa_pr_id = DB::table('grupa_pr')->where('grupa_pr_id',$parrent_grupa_pr_id)->pluck('parrent_grupa_pr_id');
					DB::table('grupa_pr')->where(array('id_is'=>$group_id))->update(array('parrent_grupa_pr_id'=>$parrent_grupa_pr_id));
				}else{
					DB::table('grupa_pr')->where('parrent_grupa_pr_id',$parent_id)->delete();
				}
			}
		}

	}

	public static function getMappedGroups(){
		$mapped = array();
		$groups = DB::table('grupa_pr')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($groups as $group){
			$mapped[$group->id_is] = $group->grupa_pr_id;
		}
		return $mapped;
	}

	public static function lager($token,$articles,$stockCodes){
		$filteredArticles = array_filter($articles,function($article){
			return $article->attributes->kolicina > 0 || $article->attributes->rezervacija > 0;
		});
		$stock = array();
		foreach($filteredArticles as $filteredArticle){
			$magacini = WingsAPI::lager($token,$filteredArticle->id);
			$stock[$filteredArticle->id] = array('kolicina'=>0,'rezervisano'=>0);
			foreach($magacini as $lager){
				if(in_array($lager->id,$stockCodes)){
					$stock[$filteredArticle->id]['kolicina'] += $lager->attributes->kolicina;
					$stock[$filteredArticle->id]['rezervisano'] += $lager->attributes->rezervacija;
				}
			}
		}
		return $stock;
	}

	public static function saveImages($token,$idIss,$mappedArticles){
		$web_slika_id= DB::select("SELECT MAX(web_slika_id) + 1 AS next_id FROM web_slika")[0]->next_id;
		if(is_null($web_slika_id)){
			$web_slika_id = 1;
		}
		foreach($idIss as $idIs){
		    if(isset($mappedArticles[$idIs])){
				try { 
				    $url = 'https://portal.wings.rs/scripts/imagemanager.php?las_imgSize=large&las_imgId='.$idIs;
				    // $destination = '/var/www/html/auto_shops/'.Request::server("SERVER_NAME").'/images/products/big/';
				    $destination = '/var/www/html/wbp/images/products/big/';

					$options = array(
					  'http'=>array(
					    'method'=>"GET",
					    'header'=>
					            "Host: portal.wings.rs\r\n".
								"User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0\r\n".
								"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n".
								"Accept-Language: en-US,en;q=0.5\r\n".
								"Accept-Encoding: gzip, deflate, br\r\n".
								"Connection: keep-alive\r\n".
								"Cookie: PHPSESSID=".$token.";\r\n".
								"Upgrade-Insecure-Requests: 1\r\n"
					  )
					);

					$context = stream_context_create($options);
					$content = @file_get_contents($url, false, $context);
					$finfo = new finfo(FILEINFO_MIME_TYPE);
					$buffer = $finfo->buffer($content);

					if(strpos($buffer, 'image') !== false){
						$name = $web_slika_id.'.'.str_replace('image/','',$buffer);
						file_put_contents($destination.$name,$content);	

						DB::statement("INSERT INTO web_slika (web_slika_id,roba_id,akcija,putanja) VALUES (".$web_slika_id.",".$mappedArticles[$idIs].",1,'images/products/big/".$name."')");
						$web_slika_id++;
					}
				}
				catch (Exception $e) {
				}
			}
		}

	}

	public static function updateArticles(){
		//stanja artikla
		$stanjePozoviteId = self::getStenjeArtiklaId('Pozovite za cenu');
		$stanjeUskoroId = self::getStenjeArtiklaId('Uskoro na stanju');
		
		DB::statement("UPDATE roba SET roba_flag_cene_id = 1 WHERE roba_flag_cene_id = ".strval($stanjeUskoroId)." AND roba_id IN (SELECT DISTINCT roba_id FROM lager WHERE orgj_id = 1 AND kolicina > 0)");
		DB::statement("UPDATE roba SET roba_flag_cene_id = ".strval($stanjeUskoroId)." WHERE roba_flag_cene_id = 1 AND roba_id NOT IN (SELECT DISTINCT roba_id FROM lager WHERE orgj_id = 1 AND kolicina > 0)");

		DB::statement("UPDATE roba SET roba_flag_cene_id = ".strval($stanjePozoviteId)." WHERE web_cena = 0 AND roba_flag_cene_id = 1");
		DB::statement("UPDATE roba SET roba_flag_cene_id = 1 WHERE web_cena > 0 AND roba_flag_cene_id = ".strval($stanjePozoviteId)."");
	}

	public static function entityDecode(array $id_iss=[]){
		if(count($id_iss) > 0){
			DB::statement("update roba set naziv = replace(naziv, '&#353;', 'š') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#273;', 'đ') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#269;', 'č') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#263;', 'ć') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#382;', 'ž') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#352;', 'Š') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#272;', 'Đ') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#268;', 'Č') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#262;', 'Ć') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#381;', 'Ž') where id_is in (".implode(",",$id_iss).")");

			DB::statement("update roba set naziv_web = replace(naziv_web, '&#353;', 'š') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#273;', 'đ') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#269;', 'č') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#263;', 'ć') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#382;', 'ž') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#352;', 'Š') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#272;', 'Đ') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#268;', 'Č') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#262;', 'Ć') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#381;', 'Ž') where id_is in (".implode(",",$id_iss).")");
		}else{
			// DB::statement("update roba set naziv = replace(naziv, '&#353;', 'š')");
			// DB::statement("update roba set naziv = replace(naziv, '&#273;', 'đ')");
			// DB::statement("update roba set naziv = replace(naziv, '&#269;', 'č')");
			// DB::statement("update roba set naziv = replace(naziv, '&#263;', 'ć')");
			// DB::statement("update roba set naziv = replace(naziv, '&#382;', 'ž')");
			// DB::statement("update roba set naziv = replace(naziv, '&#352;', 'Š')");
			// DB::statement("update roba set naziv = replace(naziv, '&#272;', 'Đ')");
			// DB::statement("update roba set naziv = replace(naziv, '&#268;', 'Č')");
			// DB::statement("update roba set naziv = replace(naziv, '&#262;', 'Ć')");
			// DB::statement("update roba set naziv = replace(naziv, '&#381;', 'Ž')");

			DB::statement("update roba set naziv_web = replace(naziv_web, '&#353;', 'š')");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#273;', 'đ')");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#269;', 'č')");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#263;', 'ć')");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#382;', 'ž')");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#352;', 'Š')");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#272;', 'Đ')");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#268;', 'Č')");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#262;', 'Ć')");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#381;', 'Ž')");

		}
		
		DB::statement("update proizvodjac set naziv = replace(naziv, '&#353;', 'š')");
		DB::statement("update proizvodjac set naziv = replace(naziv, '&#273;', 'đ')");
		DB::statement("update proizvodjac set naziv = replace(naziv, '&#269;', 'č')");
		DB::statement("update proizvodjac set naziv = replace(naziv, '&#263;', 'ć')");
		DB::statement("update proizvodjac set naziv = replace(naziv, '&#382;', 'ž')");
		DB::statement("update proizvodjac set naziv = replace(naziv, '&#352;', 'Š')");
		DB::statement("update proizvodjac set naziv = replace(naziv, '&#272;', 'Đ')");
		DB::statement("update proizvodjac set naziv = replace(naziv, '&#268;', 'Č')");
		DB::statement("update proizvodjac set naziv = replace(naziv, '&#262;', 'Ć')");
		DB::statement("update proizvodjac set naziv = replace(naziv, '&#381;', 'Ž')");		
    }

	public static function convert($string){
		$string = str_replace(array('è','ð','æ','Æ'),array('č','đ','ć','Ć'),$string);
		
        /* Only do the slow convert if there are 8-bit characters */
        if ( !preg_match("/[\200-\237]/", $string) && !preg_match("/[\241-\377]/", $string) )
               return $string;

        // decode three byte unicode characters
          $string = preg_replace_callback("/([\340-\357])([\200-\277])([\200-\277])/",
                    create_function ('$matches', 'return \'&#\'.((ord($matches[1])-224)*4096+(ord($matches[2])-128)*64+(ord($matches[3])-128)).\';\';'),
                    $string);

        // decode two byte unicode characters
          $string = preg_replace_callback("/([\300-\337])([\200-\277])/",
                    create_function ('$matches', 'return \'&#\'.((ord($matches[1])-192)*64+(ord($matches[2])-128)).\';\';'),
                    $string);

        // $string = str_replace(array("\n","&#352;","&#353;","&#268;","&#269;","&#272;","&#273;","&#381;","&#382;","&#262;","&#263;"),array("<br>","Š","š","Č","č","Đ","đ","Ž","ž","Ć","ć"),$string);
		$string = str_replace(array("\n"),array("<br>"),$string);

        return $string;
    }

}