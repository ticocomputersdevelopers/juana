<?php
namespace IsAcis;
use DB;

class Price {

	public static function table_body($articles,$mappedArticles){
		$result_arr = array();
		$codes = array();

		//$roba_id =  DB::table('roba')->max('roba_id')+1;
		//$sifra_k = DB::table('roba')->max('sifra_k')+1;
		// $grupa_pr_id = DB::select("SELECT grupa_pr_id FROM grupa_pr WHERE grupa_pr_id > 0")[0]->grupa_pr_id;
		$grupa_pr_id = 1;

		foreach($articles as $article) {

			// $roba_id++;
			// $sifra_k++;
			$sifra_is = $article->Sifra_Artikla;
			$id_is = $article->Id_Artikla;
			$roba_id = isset($mappedArticles[strval($sifra_is)]) ? $mappedArticles[strval($sifra_is)] : null;
			//var_dump($roba_id);die;
			$racunska_cena_nc = $article->Cena_Bez_PDV_a;
			$racunska_cena_nc = str_replace(",", "", $racunska_cena_nc);

			$result_arr[] = "(".strval($roba_id).",".strval($racunska_cena_nc).",".strval($racunska_cena_nc).",'".strval($sifra_is)."','".strval($id_is)."')";


		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_update($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}

		$all_columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$columns = $all_columns;
	
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(roba_id,racunska_cena_end,racunska_cena_nc,sifra_is,id_is)";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		
		// update

		$columns = [			
			'racunska_cena_nc',
			'racunska_cena_end'
		];
		foreach($columns as $col){
			if($col!="roba_id"  && $col!="sifra_is" && $col!="id_is"){
		    	$updated_columns[] = "".$col." = roba_temp.".$col."";
			}
		}

		DB::statement("UPDATE roba t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.sifra_is=roba_temp.sifra_is::varchar AND t.sifra_is IS NOT NULL AND t.sifra_is <> ''");	

		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

}