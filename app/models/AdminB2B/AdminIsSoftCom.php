<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);

use IsSoftCom\FileData;
use IsSoftCom\Support;
use IsSoftCom\Article;
use IsSoftCom\Stock;
use IsSoftCom\Partner;


class AdminIsSoftCom {

    public static function execute(){
        try {

            //articles
            $articles = FileData::articles();

            //groups
            Support::setISMainGroup();
            // $internalMappedGroups = Support::internalMappedGroups($articles);
            // $groups = Support::filteredGroups($articles,$internalMappedGroups);
            // Support::saveGroups($groups);

            //articles
            $resultArticle = Article::table_body($articles);
            Article::query_insert_update($resultArticle->body,array('grupa_pr_id','naziv','naziv_web','web_opis','barkod','racunska_cena_nc','racunska_cena_end','mpcena','web_cena','proizvodjac_id','tarifna_grupa_id','flag_aktivan','flag_cenovnik','flag_prikazi_u_cenovniku'));
            Article::query_update_unexists($resultArticle->body);
            $mappedArticles = Support::getMappedArticles();

            //stock
            $resultStock = Stock::table_body($articles,$mappedArticles);
            Stock::query_insert_update($resultStock->body);


            // //partner
            // $partners = FileData::partners();
            // $resultPartner = Partner::table_body($partners,array('id_kategorije'));
            // Partner::query_insert_update($resultPartner->body);

            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);
        }catch (Exception $e){
            AdminB2BIS::saveISLog('false');
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }



}