<?php

class AdminB2BVesti {

	 public static function getVesti($active=null){
        if($active==null){
            $vesti = DB::table('web_vest_b2b')->orderBy('rbr', 'asc')->orderBy('datum', 'dsc')->paginate(20);
        }elseif($active==1 || $active==0){
            $vesti = DB::table('web_vest_b2b')->where('aktuelno',$active)->orderBy('rbr', 'asc')->orderBy('datum', 'dsc')->paginate(20);
        }
        return $vesti;
    }
    public static function findB2BTitle($web_vest_b2b_id) {
		$jezik = DB::table('web_vest_b2b_jezik')->where('web_vest_b2b_id', $web_vest_b2b_id)->orderBy('jezik_id', 'asc')->first();
		if(!is_null($jezik)){
			return $jezik->naslov;
		}
		return '';
	}
	public static function find($web_vest_b2b_id, $column) {
		$info = DB::table('web_vest_b2b')->where('web_vest_b2b_id', $web_vest_b2b_id)->pluck($column);
		return $info;
	}

	public static function news_link_b2b($vest_id){
		Session::put('b2b_user_'.Options::server(),1);
		return AdminB2BOptions::base_url().'b2b/blog/'.$vest_id;
	}

}