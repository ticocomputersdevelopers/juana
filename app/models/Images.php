<?php

class Images extends Eloquent
{
    protected $table = 'web_slika';

    protected $primaryKey = 'web_slika_id';
    
    public $timestamps = false;

	protected $hidden = array(
        'web_slika_id',
        'roba_id',
        'slika',
        'akcija',
        'dobavljac_id',
        'sifra_kod_dobavljaca',
        'web_slika_id_dobavljac',
        'flag_prikazi' ,
        'putanja',
        'alt',
        'flag_sticker',
        'id_is'
    	);

	public function getIdAttribute()
	{
	    return isset($this->attributes['web_slika_id']) ? $this->attributes['web_slika_id'] : null;
	}
	public function getImagePathAttribute()
	{
	    return isset($this->attributes['putanja']) ? $this->attributes['putanja'] : null;
	}
    public function getDefaultAttribute()
    {
        return isset($this->attributes['akcija']) ? $this->attributes['akcija'] : null;
    }
    public function getActiveAttribute()
    {
        return isset($this->attributes['flag_prikazi']) ? $this->attributes['flag_prikazi'] : null;
    }

	protected $appends = array(
    	'id',
        'active',
        'default',
        'image_path'
    	);

    public function parent(){
        return $this->belongsTo('Images','parent_id');
    }

    public function childs(){
        return $this->hasMany('Images', 'parent_id', 'web_slika_id');
    }

    public function articles(){
        return $this->hasMany('Article', 'web_slika_id');
    }

}