<?php

class ArticleLang extends Eloquent
{
    protected $table = 'roba_jezik';
    
    public $timestamps = false;

	protected $hidden = array(
		'roba_id',
        'jezik_id',
        'tagovi'
    	);

	public function getArticleIdAttribute()
	{
	    return isset($this->attributes['roba_id']) ? $this->attributes['roba_id'] : null;
	}
    public function getLangIdAttribute()
    {
        return isset($this->attributes['jezik_id']) ? $this->attributes['jezik_id'] : null;
    }
    public function getTagsAttribute()
    {
        return isset($this->attributes['tagovi']) ? $this->attributes['tagovi'] : null;
    }

	protected $appends = array(
    	'article_id',
        'lang_id',
        'tags'
    	);

    public function article(){
        return $this->belongsTo('Article','roba_id');
    }

    public function lang(){
        return $this->belongsTo('Languages','jezik_id');
    }
}