<?php

class OrderB2B extends Eloquent
{
    protected $table = 'web_b2b_narudzbina';

    protected $primaryKey = 'web_b2b_narudzbina_id';
    
    public $timestamps = false;

	protected $hidden = array(
        'web_b2b_narudzbina_id',
        'orgj_id',
        'poslovna_godina_id',
        'vrsta_dokumenta_id',
        'broj_dokumenta',
        'datum_dokumenta',
        'valuta_id',
        'kurs',
        'web_nacin_isporuke_id',
        'iznos',
        'prihvaceno',
        'stornirano',
        'realizovano',
        'napomena',
        'ip_adresa',
        'posta_slanje_id',
        'posta_slanje_poslato',
        'posta_slanje_broj_posiljke',
        'promena',
        'partner_korisnik_id',
        'sifra_connect',
        'web_nacin_placanja_id',
        'sifra_is',
        'narudzbina_status_id',
        'avans',
        'web_b2b_korpa_id',
        'imenik_id'
    	);

	public function getIdAttribute()
	{
	    return isset($this->attributes['web_b2b_narudzbina_id']) ? $this->attributes['web_b2b_narudzbina_id'] : null;
	}
    public function getDocumentNumberAttribute()
    {
        return isset($this->attributes['broj_dokumenta']) ? $this->attributes['broj_dokumenta'] : null;
    }
    public function getDateAttribute()
    {
        return isset($this->attributes['datum_dokumenta']) ? $this->attributes['datum_dokumenta'] : null;
    }
    public function getStatusAttribute()
    {
        if($this->attributes['prihvaceno'] == 0 && $this->attributes['realizovano'] == 0 && $this->attributes['stornirano'] == 0){
            return 'NEW';
        }else if($this->attributes['prihvaceno'] == 1 && $this->attributes['realizovano'] == 0 && $this->attributes['stornirano'] == 0){
            return 'ACCEPTED';
        }else if($this->attributes['realizovano'] == 1 && $this->attributes['stornirano'] == 0){
            return 'REALIZED';
        }else if($this->attributes['stornirano'] == 1){
            return 'CANCELED';
        }else{
            return null;
        }
    }
    public function getNoteAttribute()
    {
        return isset($this->attributes['napomena']) ? $this->attributes['napomena'] : null;
    }
      

	protected $appends = array(
    	'id',
        'document_number',
        'date',
        'status',
        'note',
    	);

    public function partner(){
        return $this->belongsTo('PartnerApi','partner_id');
    }

    public function items(){
        return $this->hasMany('OrderB2BItem', 'web_b2b_narudzbina_id', 'web_b2b_narudzbina_id');
    }

}