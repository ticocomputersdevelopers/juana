<?php 
use \Statickidz\GoogleTranslate;

class RMALanguage{
	 //prevodi reci za dati jezik
	public static function trans($string,$lang=null){
		// return 'test';
        if($lang==null){
            $lang = Language::lang();
        }

        $jezik_id = DB::table('jezik')->where(array('kod'=>$lang))->pluck('jezik_id');
        $izabrani_jezik_id =  DB::table('jezik')->where(array('aktivan'=>1, 'izabrani'=>1))->pluck('jezik_id');

        if($izabrani_jezik_id != 2){
        	return $string;
        }else{
            $query = DB::select("SELECT * FROM prevodilac WHERE izabrani_jezik_id = 1 AND jezik_id = 4 AND izabrani_jezik_reci IS NOT NULL AND izabrani_jezik_reci ILIKE '".pg_escape_string($string)."'");

        	if(count($query) > 0){
        		return $query[0]->reci;
        	}else{
        		$str_arr = explode(' ',$string);
        		$trans_arr = array();
        		$not_translate = false;
        		foreach($str_arr as $str){
        			$query_str = DB::select("SELECT * FROM prevodilac WHERE izabrani_jezik_id = 1 AND jezik_id = 4 AND izabrani_jezik_reci IS NOT NULL AND izabrani_jezik_reci ILIKE '".pg_escape_string($str)."'");
		        	if(count($query_str) > 0){
		        		$trans_arr[] = $query_str[0]->reci;
		        	}else{
		        		$not_translate = true;
		        		$trans_arr[] = $str;
		        	}
        		}
        		return implode(' ',$trans_arr);
        	}
        }
	}
	public static function transRMA($string){
         return AdminLanguage::transAdmin($string);
    }
}