<?php
use Service\Drip;

class Order {

    public static function cart_to_order($order_data){
        date_default_timezone_set( 'Europe/Belgrade');
        $brojDokumenta = DB::table('vrsta_dokumenta')->where('vrsta_dokumenta_id',501)->pluck('sifra_vd').self::order_sifra();

        $data=array(
        'web_kupac_id'=>$order_data['web_kupac_id'],
        'orgj_id'=>DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id'),
        'poslovna_godina_id'=>Options::poslovna_godina(),
        'vrsta_dokumenta_id'=>501,
        'broj_dokumenta'=>$brojDokumenta,
        'datum_dokumenta'=>date("Y-m-d H:i:s"),
        'valuta_id'=>Session::has('valuta') ? Session::get('valuta') : 1,
        'kurs'=>Options::kurs(),
        'web_nacin_placanja_id'=>$order_data['web_nacin_placanja_id'],
        'web_nacin_isporuke_id'=>$order_data['web_nacin_isporuke_id'],
        'iznos'=>0,
        'prihvaceno'=>0,
        'stornirano'=>$order_data['stornirano'],
        'realizovano'=>0,
        'napomena'=>$order_data['napomena'],
        'ip_adresa'=>All::ip_adress(),
        'posta_slanje_id'=>-1,
        'posta_slanje_poslato'=>0,
        'promena'=>1,
        'flag_promena_connect'=>0,
        'sifra_connect'=>0,
        'popust'=>isset($order_data['popust']) ? $order_data['popust'] : 0.00,
        'poslednja_izmena'=>date('Y-m-d H:i:s')
        );
        DB::table('web_b2c_narudzbina')->insert($data);
        DB::table('web_b2c_korpa')->where('web_b2c_korpa_id',Cart::korpa_id())->update(array('web_kupac_id'=>$order_data['web_kupac_id'],'naruceno'=>1));
        self::cart_to_order_stavka();

        //drip
        if(Config::get('app.livemode') && Options::gnrl_options(3060) == 1){
            $dbOrder = DB::table('web_b2c_narudzbina')->where('broj_dokumenta',$brojDokumenta)->first();
            $drip = new Drip();
            $drip->addOrUpdateOrder($dbOrder,"placed");   
        }        

    }

    public static function order_sifra(){
        $br_nar = DB::select("SELECT (last_value + 1) as web_b2c_narudzbina_id FROM web_b2c_narudzbina_web_b2c_narudzbina_id_seq")[0]->web_b2c_narudzbina_id;
        return str_pad($br_nar, 5, '0', STR_PAD_LEFT);
    }

    public static function cart_to_order_stavka(){
        self::reservation();
        DB::statement("INSERT INTO web_b2c_narudzbina_stavka (web_b2c_narudzbina_id,broj_stavke,roba_id,kolicina,jm_cena,tarifna_grupa_id,racunska_cena_nc,osobina_vrednost_ids,project_id) SELECT currval('web_b2c_narudzbina_web_b2c_narudzbina_id_seq'),broj_stavke,roba_id,kolicina,jm_cena,tarifna_grupa_id,racunska_cena_nc,osobina_vrednost_ids,project_id FROM web_b2c_korpa_stavka WHERE web_b2c_korpa_id = ".Cart::korpa_id()."");
    }

    public static function reservation(){
        if(Options::vodjenje_lagera()==1 && Options::web_options(131)==1){
            $poslovna_godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');
            $orgj_id = DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id');
            foreach(DB::table('web_b2c_korpa_stavka')->select('roba_id','kolicina')->where('web_b2c_korpa_id',Cart::korpa_id())->get() as $stavka){
                $rezervisano = DB::table('lager')->where(array('roba_id'=>$stavka->roba_id,'poslovna_godina_id'=>$poslovna_godina_id,'orgj_id'=>$orgj_id))->pluck('rezervisano');
                if(!is_null($rezervisano)){
                    DB::table('lager')->where(array('roba_id'=>$stavka->roba_id,'poslovna_godina_id'=>$poslovna_godina_id,'orgj_id'=>$orgj_id))->update(array('rezervisano'=>($rezervisano + $stavka->kolicina)));
                }
            }
        }
    }

    public static function nacin_isporuke($selected_id=null){
        
        foreach(DB::table('web_nacin_isporuke')->where('selected',1)->get() as $row){
            if(!is_null($selected_id) && $selected_id != '' && $selected_id == $row->web_nacin_isporuke_id){
                echo '<option value="'.$row->web_nacin_isporuke_id.'" selected>'.Language::trans($row->naziv).'</option> ';
            }else{
                echo '<option value="'.$row->web_nacin_isporuke_id.'">'.Language::trans($row->naziv).'</option> ';
            }            
        }
        
    }
    
    public static function nacin_placanja($selected_id=null){

         foreach(DB::table('web_nacin_placanja')->where('selected',1)->orderBy('b2c_default','desc')->get() as $row){
            if(!is_null($selected_id) && $selected_id != '' && $selected_id == $row->web_nacin_placanja_id){
                echo '<option value="'.$row->web_nacin_placanja_id.'" selected>'.Language::trans($row->naziv).'</option> ';
            }else{
                echo '<option value="'.$row->web_nacin_placanja_id.'">'.Language::trans($row->naziv).'</option> ';
            }
        }
    }

    public static function broj_dokumenta($web_b2c_narudzbina_id){
        return DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('broj_dokumenta');
    }
    public static function datum_porudzbine($web_b2c_narudzbina_id){
        return DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('datum_dokumenta');
    }
     public static function n_i($web_b2c_narudzbina_id){
         $nacin_i=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_nacin_isporuke_id');
         
         return Language::trans(DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$nacin_i)->pluck('naziv'));
    }
    public static function n_p($web_b2c_narudzbina_id){
        $nacin_i=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_nacin_placanja_id');
        return Language::trans(DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$nacin_i)->pluck('naziv'));
    }
    public static function napomena_nar($web_b2c_narudzbina_id){
        return DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('napomena');
    }
    // public static function troskovi_isporuke_narudzbina($web_b2c_narudzbina_id){
    //     return DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_nacin_isporuke_id');
    // }
    // public static function mesto($mesto_id){
    //     return DB::table('mesto')->where('mesto_id',$mesto_id)->pluck('mesto');
    // }
    
    // public static function mesto_narudzbina($mesto){
    //     return DB::table('web_kupac')->where('mesto',$mesto)->pluck('mesto');
    // }
    public static function narudzbina_ukupno($web_b2c_narudzbina_id){
        $ukupno=0;
        foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get() as $row){
            $ukupno+=$row->kolicina*$row->jm_cena;
        }
        return $ukupno;
    }
    public static function popust($web_b2c_narudzbina_id){
        return DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('popust');
    }
    // public static function cena_isporuke(){
    //     return DB::table('cena_isporuke')->pluck('cena');
    // }

    // public static function cena_do(){
    //     return DB::table('cena_isporuke')->pluck('cena_do');
    // }

    // public static function troskovi_isporuke($web_b2c_narudzbina_id){ 
    //     $ukupno=0;
    //     foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get() as $row){
    //         $ukupno+=DB::table('roba')->where('roba_id',$row->roba_id)->pluck('tezinski_faktor')*$row->kolicina;
    //     }
    //     $cena = 0;
    //     $obj=DB::table('web_troskovi_isporuke')->orderBy('web_troskovi_isporuke_id','desc')->first();
    //     if(isset($obj)){
    //         $cena = $obj->cena;
    //     }
    //     foreach (DB::table('web_troskovi_isporuke')->orderBy('web_troskovi_isporuke_id','desc')->get() as $row) {
    //         if(round($ukupno) <= $row->tezina_gr){
    //             $cena = $row->cena;
    //         }
    //     }
    //     return $cena;
    // }

    public static function narudzbina_status_active($web_b2c_narudzbina_id){
        foreach (DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get() as $row){
            if($row->prihvaceno == 0 and $row->realizovano == 0 and $row->stornirano == 0){
                return Language::trans("Nova");
            }
            else  if($row->prihvaceno != 0 and $row->realizovano == 0 and $row->stornirano == 0){
                return Language::trans("Prihvaćena");
            }
            else  if($row->realizovano != 0 and $row->stornirano == 0){
                return Language::trans("Realizovana");
            }
            else  if($row->stornirano != 0){
                return Language::trans("Stornirana");
            }
        }
    }


} 