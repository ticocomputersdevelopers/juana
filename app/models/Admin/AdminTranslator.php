<?php
use Service\TranslatorService;

class AdminTranslator {

	public static function addToTranslator($langToId,$stringFrom,$stringTo=null,$langFromId=1){
		$langFrom = DB::table('jezik')->where(array('jezik_id'=>$langFromId,'aktivan'=>1))->first();
		$langTo = DB::table('jezik')->where(array('jezik_id'=>$langToId,'aktivan'=>1))->first();
		if((is_null($stringTo) || $stringTo == '') && !is_null($langFrom) && !is_null($langTo)){
			$translator = new TranslatorService($langFrom->kod,$langTo->kod);
			$stringTo = $translator->translate($stringFrom);
		}
		if(!is_null($langFrom) && !is_null($langTo) && $langFrom->jezik_id != $langTo->jezik_id  && !is_null($stringFrom) && !is_null($stringTo) && $stringFrom != '' && $stringTo != ''){
			$exists = DB::select("SELECT * FROM prevodilac WHERE izabrani_jezik_id = ".$langFromId." AND jezik_id = ".$langToId." AND izabrani_jezik_reci ILIKE '".$stringFrom."'");
			if(count($exists) > 0){
				DB::statement("UPDATE prevodilac SET reci = '".$stringTo."' WHERE izabrani_jezik_id = ".$langFromId." AND jezik_id = ".$langToId." AND izabrani_jezik_reci ILIKE '".$stringFrom."'");
			}else{
				DB::table('prevodilac')->insert(array('izabrani_jezik_id'=>$langFromId,'jezik_id'=>$langToId,'izabrani_jezik_reci'=>$stringFrom,'reci'=>$stringTo));
			}
			return true;
		}
		return false;
	}

	public static function addToTranslatorAll($stringFrom,$langFromId=1){
		$langFrom = DB::table('jezik')->where(array('jezik_id'=>$langFromId,'aktivan'=>1))->first();
		if(!is_null($langFrom) && !is_null($stringFrom) && $stringFrom != ''){
			foreach(DB::table('jezik')->where(array('aktivan'=>1))->where('jezik_id','<>',$langFromId)->get() as $jezik){
				$exists = DB::select("SELECT * FROM prevodilac WHERE izabrani_jezik_id = ".$langFromId." AND jezik_id = ".$jezik->jezik_id." AND izabrani_jezik_reci ILIKE '".$stringFrom."'");
				$translator = new TranslatorService($langFrom->kod,$jezik->kod);
				$stringTo = $translator->translate($stringFrom);
				if(count($exists) > 0){
					DB::statement("UPDATE prevodilac SET reci = '".$stringTo."' WHERE izabrani_jezik_id = ".$langFromId." AND jezik_id = ".$jezik->jezik_id." AND izabrani_jezik_reci ILIKE '".$stringFrom."'");
				}else{
					DB::table('prevodilac')->insert(array('izabrani_jezik_id'=>$langFromId,'jezik_id'=>$jezik->jezik_id,'izabrani_jezik_reci'=>$stringFrom,'reci'=>$stringTo));
				}
			}
			return true;
		}
		return false;
	}

	public static function updateTranslator($langToId,$stringFrom,$stringTo){
        if($langToId==1){
            AdminTranslator::addToTranslatorAll($stringTo);
        }else{
            AdminTranslator::addToTranslator($langToId,$stringFrom,$stringTo);
        }
	}
}