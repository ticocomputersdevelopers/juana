
<!DOCTYPE html> 
<html lang="sr"> 
<head> 
    <title>Prijava | {{B2bOptions::company_name() }}</title> 
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <link rel="icon" type="image/png" href="{{ B2bOptions::base_url()}}favicon.ico"> 
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
 
   <!-- BOOTSTRAP CDN -->
<!--     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
 
   <!-- BOOTSTRAP LOCAL -->  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
    <link href="{{ TicketingOptions::base_url()}}css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script src="{{ TicketingOptions::base_url()}}js/bootstrap.min.js"></script>
 
    <link href="{{ TicketingOptions::base_url()}}css/ticketing/main.css" rel="stylesheet" type="text/css" /> 
</head>

<body class="body-login">
    <div class="b2bLoginContainer">
        <div class="b2b-login row b2b-flex">
            <div class="col-md-5 col-sm-5 col-xs-12 center"> 
                <a class="logo-b2b-login" href="{{ B2bOptions::base_url()}}" title="{{ B2bOptions::company_name()}}">
                    <img src="../images/laptop-tiketing-logo.png" alt="">
                </a>
            </div> 
          <div class="col-md-7 col-sm-7 col-xs-12 left-border"> 
            <div class="b2bForm"> 
                <form action="{{ TicketingOptions::base_url() }}ticketing/login/store" method="post">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 no-padd text-center"> 
                            <p class="center no-margin">PRIJAVA</p>
                        </div>
                    </div><br>
                    <div class="field-group">
                        <label for="username">E-mail</label>
                        <input id="username" name="username" type="text" value="{{Input::old('username')}}" class="login-form__input form-control"> 
                        @if($errors->first('username'))
                            <span class="error">{{ $errors->first('username') }}</span>
                        @endif
                    </div>
                     
                    <div class="field-group">
                        <label for="password">Lozinka</label>
                        <input id="password" name="password" type="password" value="{{Input::old('password')}}" class="login-form__input form-control"> 
                         @if($errors->first('password'))
                            <span class="error">{{ $errors->first('password') }}</span>
                         @endif
                    </div> 
                    <div class="field-group">
                        <button class="submit btn">PRIJAVI SE</button>
                    </div> 
                </form> 
              <!--   <span class="text-right">B2B ENGINE BY: 
                     <a href="https://www.selltico.com/" target="_blank"> <img src="{{ Options::base_url()}}images/logo-selltico.png">     </a>
                </span>  -->
            </div>
        </div>
    </div>
    </div>
</body>
</html>