<header id="admin-header" class="row">
	<!-- WIDTH TOGGLE BUTTON -->
	<div class="header-width-toggle"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>

	<div class="logo-wrapper">
		<a class="logo" href="{{ B2bOptions::base_url()}}" title="{{ B2bOptions::company_name()}}">
			<img src="../images/laptop-tiketing-logo.png" alt="">
		</a>
	</div>
	<!-- <span class="main-menu-toggler"></span> -->
	<nav class="main-menu">

		<ul class="clearfix">
			<li class="menu-item">
				<div class="">
					<div class="logged-user">Ulogovan: 
						@if($user = TicketingOptions::user())
							@if($user->flag_vrsta_kupca == 0)
							{{ $user->ime.' '.$user->prezime }}
							@else
							{{ $user->naziv.' '.$user->pib }}
							@endif
						@endif
					</div>
				</div>
			</li>

			<li class="menu-item">
				<a href="{{TicketingOptions::base_url()}}ticketing" class="menu-item__link  @if(in_array($strana,array('prijem_na_servis'))) active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-file-text" aria-hidden="true"></i>
						<span class="menu-tooltip">Novi tiket</span>
					</div>
					<div class="menu-item__text">Novi tiket</div>
				</a>
			</li>
			<li class="menu-item">
				<a href="{{TicketingOptions::base_url()}}ticketing/reklamacije" class="menu-item__link  @if(in_array($strana,array('reklamacije'))) active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-copy" aria-hidden="true"></i>
						<span class="menu-tooltip">Tiketi</span>
					</div>
					<div class="menu-item__text">Tiketi</div>
				</a>
			</li>
			<li class="menu-item">
				<a href="{{TicketingOptions::base_url()}}ticketing/promena-lozinke" class="menu-item__link  @if(in_array($strana,array('promena_lozinke'))) active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-key" aria-hidden="true"></i>
						<span class="menu-tooltip">Promena Lozinke</span>
					</div>
					<div class="menu-item__text">Promena Lozinke</div>
				</a>
			</li>

		</ul>
	</nav>

	<div class="logout-wrapper">
		<div class="menu-item">
			<a href="{{ TicketingOptions::base_url()}}ticketing/logout" class="menu-item__link">
				<div class="menu-item__icon">
					<i class="fa fa-sign-out" aria-hidden="true"></i>
					<span class="menu-tooltip">Odjavi se</span>
				</div>
				<div class="menu-item__text">Odjavi se</div>
			</a>
		</div>
		<p class="footnote">TiCo &copy; {{ date('Y') }} - All rights reserved</p>
	</div>
</header>

