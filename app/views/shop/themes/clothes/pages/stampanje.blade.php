<!DOCTYPE html>
<html><head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<title>PDF</title>
<style>
* { 
	box-sizing: border-box;
 	padding: 0;
	margin: 0; 
	line-height: 1;	
} 
body {
	font-size: 14px;  
	font-family: DejaVu Sans;
}
ul { list-style-type: none; }

table {border-collapse: collapse; margin-bottom: 15px; width: 100%;}

td, th {border: 1px solid #cacaca; text-align: left; padding: 3px;}
 
.row::after {content: ""; clear: both; display: table;}
[class*="col-"] { float: left; padding: 5px; display: inline-block;}

.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}
.inline-block { display: inline-block; }
.text-right{ text-align: right; }
.text-center{ text-align: center; } 
.relative { position: relative; }

img{
	max-width: 100%;
}
.header{
	border-bottom: 2px solid #cc0000;
	background-color: #f2f2f2;
	padding: 10px 5px;
}
.header .logo {
	max-height: 100px;  
	margin: 20px;
} 
.main-content {
	padding: 15px;   
}
.main-content .product-img{
	max-height: 220px;
} 
.main-content .foo_1{
	font-size: 20px;
	margin: 15px 0;
	padding: 5px 0;
	border-bottom: 1px solid #ddd;
} 
.main-content .foo_2{
	font-size: 25px;
	margin: 15px 0;
	color: #cc0000;
} 
.main-content .foo_2 span{
	font-size: 90%;
} 
.main-content .foo_3 .ul_1,.main-content .foo_3 .genkar{ 
	border: 1px solid #ddd; 
}  
.main-content .foo_3 .ul_1 .li_1{ 
	width: 30%; 
}
.main-content .foo_3 .ul_1 .li_2{ 
	width: 60%; 
	border-bottom: 1px solid #ddd;
} 
.main-content .foo_3 ul li{
    font-size: 12px; 
    padding: 2px 10px;      
    width: 45%;
    display: inline-block;   
}      
.main-content .foo_3 .genkar li{
  	border-bottom: 1px solid #ddd;
}    
.footer{
	background: #464853; 
	color: #fff;
	padding: 20px 15px;
	position: absolute;
	bottom: 0;
	font-size: 12px;
}
</style>
</head><body>
	<div class="container relative"> 
		<div class="row header">
			<div class="col-6">
				<img class="logo" src="{{ Options::domain()}}{{Options::company_logo()}}" alt="logo">
			</div>
			<div class="col-5">
				<div>
					<p>Firma: {{AdminOptions::company_name()}}</p>
					<p>Adresa: {{AdminOptions::company_adress()}}</p>
					<p>Telefon: {{AdminOptions::company_phone()}}</p>
					<p>Fax: {{AdminOptions::company_fax()}}</p>
					<p>PIB: {{AdminOptions::company_pib()}}</p>
					<p>E-mail: {{AdminOptions::company_email()}}</p>
				</div>
			</div>
		</div>

	  	<div class="row main-content">
	  		<div class="col-6">  
		  		<p class="foo_1">{{ Product::seo_title($roba_id) }}</p> 

				@if(AdminOptions::sifra_view_web()==1)
				<p>{{Language::trans('Šifra') }}: {{Product::sifra($roba_id)}}</p>
				@elseif(AdminOptions::sifra_view_web()==4)                       
				<p>{{Language::trans('Šifra') }}: {{Product::sifra_d($roba_id)}}</p>
				@elseif(AdminOptions::sifra_view_web()==3)                       
				<p>{{Language::trans('Šifra') }}: {{Product::sku($roba_id)}}</p>
				@elseif(AdminOptions::sifra_view_web()==2)                       
				<p>{{Language::trans('Šifra') }}: {{Product::sifra_is($roba_id)}}</p>
				@endif

				<p>{{Language::trans('Kategorija prozivoda') }}: {{ Product::get_grupa_title($roba_id) }}</p>
				<p>{{Language::trans('Proizvođač') }}: {{ product::get_proizvodjac($roba_id) }}</p>
				 
				@if(All::provera_akcija($roba_id) != 1)
				<p class="foo_2"><span>{{Language::trans('Cena') }}:</span> {{ Cart::cena(Product::old_price($roba_id)) }}</p>
				@else
				<p class="foo_2"><span>{{Language::trans('Cena') }}:</span> {{ Cart::cena(Product::get_price($roba_id)) }}</p>
				@endif    
			</div>

			<div class="col-5 text-center">  
				<img class="product-img" src="{{ Options::domain() }}{{ Product::web_slika($roba_id) }}" alt="{{ Product::seo_title($roba_id) }}" />
			</div>
	  	 
		  	<div class="row foo_3">
	  			 {{ Product::get_karakteristike_short_print_spec($roba_id) }} 
		  	</div>
	 	</div>

	  	<div class="row footer">
	  		<p>Cene su informativnog karaktera. Prodavac ne odgovara za tačnost cena iskazanih na sajtu, zadržava pravo izmena cena. Ponudu za ostale artikle, informacije o stanju lagera i aktuelnim cenama možete dobiti na upit. Plaćanje se isključivo vrši virmanski - bezgotovinski.</p>
	  	</div> 
	</div>  
</body></html>
