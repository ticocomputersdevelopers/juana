@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<div class="row contact-page"> 
	<div class="col-md-5 col-sm-12 col-xs-12 sm-no-padd">

		<br>

		<h2><span class="section-title">{{ Language::trans('Kontakt') }}</span></h2>

		@if(Options::company_name() != '') 
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Firma') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_name() }}</div>
		</div> 
		@endif

		@if(Options::company_adress() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Adresa') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_adress() }}</div>
		</div>
		@endif
		
		@if(Options::company_city() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Grad') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_city() }}</div>
		</div>
		@endif
		
		@if(Options::company_phone() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Telefon') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_phone() }}</div>
		</div>
		@endif
		
		@if(Options::company_fax() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Fax') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_fax() }}</div>
		</div>
		@endif
		
		@if(Options::company_pib() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('PIB') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_pib() }}</div>
		</div>
		@endif
		
		@if(Options::company_maticni() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Matični broj') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_maticni() }}</div>
		</div>
		@endif
		
		@if( Options::company_email() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('E-mail') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7">
				<a class="mailto" href="mailto:{{ Options::company_email() }}">{{ Options::company_email() }}</a>
			</div> 
		</div>
		@endif

	</div> 


	<div class="col-md-7 col-sm-12 col-xs-12 sm-no-padd"> 

		<br>
		
		<h2><span class="section-title">{{ Language::trans('Da li imate pitanja') }}?</span></h2>

		<form method="POST" action="{{ Options::base_url() }}contact-message-send">
			<div>
				<label id="label_name">{{ Language::trans('Vaše ime') }} *</label>
				<input class="contact-name" name="contact-name" id="JScontact_name" type="text" value="{{ Input::old('contact-name') }}">
				<div class="error red-dot-error">{{ $errors->first('contact-name') ? $errors->first('contact-name') : "" }}</div>
			</div> 

			<div>
				<label id="label_email">{{ Language::trans('Vaša e-mail adresa') }} *</label>
				<input class="contact-email" name="contact-email" id="JScontact_email" type="text" value="{{ Input::old('contact-email') }}" >
				<div class="error red-dot-error">{{ $errors->first('contact-email') ? $errors->first('contact-email') : "" }}</div>
			</div>		

			<div>	
				<label id="label_message">{{ Language::trans('Vaša poruka') }} </label>
				<textarea class="contact-message" name="contact-message" rows="5" id="message">{{ Input::old('contact-message') }}</textarea>
				<div class="error red-dot-error">{{ $errors->first('contact-message') ? $errors->first('contact-message') : "" }}</div>
			</div> 

			<div class="capcha text-center"> 
				{{ Captcha::img(5, 160, 50) }}<br>
				<span>{{ Language::trans('Unesite kod sa slike') }}</span>
				<input type="text" name="captcha-string" tabindex="10" autocomplete="off">
				<div class="error red-dot-error">{{ $errors->first('captcha') ? $errors->first('captcha') : "" }}</div>
			</div>

			<div class="text-right"> 
				<button type="submit" class="button">{{ Language::trans('Pošalji') }}</button>
			</div>
		</form>
	</div>
</div>

@if(Options::company_map() != '' && Options::company_map() != ';')
<div class="map-frame relative">
	
	<div class="map-info">
		<h5>{{ Options::company_name() }}</h5>
		<h6>{{ Options::company_adress() }}, {{ Options::company_city() }}</h6> 
	</div>

	<iframe src="https://maps.google.com/maps?q={{ All::lat_long()[0] }},{{ All::lat_long()[1] }}&output=embed" width="100%" height="100%" frameborder="0" aria-hidden="false" tabindex="0"></iframe> 

</div> 
@endif


<script>
	function initMap(){ return; }
</script>

@if(Session::get('message'))
<script>
	$(document).ready(function(){ 
		alertSuccess("{{ Session::get('message') }}");
	});
</script>
@endif
@endsection     