@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<p class="welcome-login">{{ Language::trans('DOBRODOŠLI, MOLIM VAS PRIJAVITE SE!') }}</p>

<div id="loginModal" role="dialog">
    <div class="form-display">
        <div>
            <div class=""> 
                <label for="JSemail_login">{{ Language::trans('E-mail') }}</label>
                <input id="JSemail_login" type="text" value="" autocomplete="off">
        
                <label for="JSpassword_login">{{ Language::trans('Lozinka') }}</label>
                <input autocomplete="off" id="JSpassword_login" type="password" value=""> 
            </div>

            <div class="text-center forgot_pass">
                <!-- <a class="inline-block button login-form-button" href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}">{{Language::trans('Registruj se')}}</a> -->
                
                <button type="button" onclick="user_forgot_password()" class="forgot-psw">{{ Language::trans('Zaboravljena lozinka') }}</button>

            </div>

            <div class="text-center">
                <button type="button" onclick="user_login()" class="button login-form-button">{{ Language::trans('Prijavi se') }}</button>
                
                <div class="field-group error-login JShidden-msg" id="JSForgotSuccess"><br>
                    {{ Language::trans('Novu lozinku za logovanje dobili ste na navedenoj e-mail adresi') }}.
                </div> 
            </div>
        </div>   
    </div>
</div>

<div class="hidden row login-page-padding">
	<div class="col-xs-12">
	</div>

	<div class="col-md-12 no-padding">
		<div class="form-display">
			<form action="{{ Options::base_url()}}login" method="post" class="login-form" autocomplete="off">
				
				<div class="field-group row">
					<div class="col-md-12"> 
	 					<label for="email_login">{{ Language::trans('E-mail') }}</label> 
					</div>

					<div class="col-md-12">
						<?php if(Input::old('email_login')){ $old_mail = Input::old('email_login'); }else{ if(Input::old('email_fg')){ $old_mail = Input::old('email_fg'); }else{$old_mail ='';} } ?>
						<input class="login-form__input" name="email_login" type="text" value="{{ $old_mail }}" autocomplete="off">
					</div>
				</div>
				
				<div class="field-group row">
					<div class="col-md-12"> 
						<label for="lozinka_login">{{ Language::trans('Lozinka') }}</label>
					</div>
					<div class="col-md-12"> 
					    <input class="login-form__input" autocomplete="off" name="lozinka_login" type="password" value="{{ Input::old('lozinka_login') ? Input::old('lozinka_login') : '' }}">
					</div>
				</div>
				
				<div class="row"> 
					<div class="col-md-12 text-center"> 
						<form class="forgot_pass" action="{{ Options::base_url()}}zaboravljena-lozinka" method="post" autocomplete="off">
							<div class="error">{{ $errors->first('email_fg') ? $errors->first('email_fg') : "" }}</div>
							<?php if(Input::old('email_fg')){ $old_mail_fg = Input::old('email_login'); }else{ if(Input::old('email_login')){ $old_mail_fg = Input::old('email_login'); }else{$old_mail_fg ='';} } ?>
							<input name="email_fg" type="hidden" value="{{ $old_mail_fg }}" >
							<button class="btn-forgot-pass" type="submit">{{ Language::trans('Zaboravili ste lozinku?') }}</button>
						</form>
					</div>
				</div>

			</form>
			
			<div class="row text-center"> 
			 	<div class="col-md-12"> 
					<button type="submit" class="login-form-button admin-login">{{ Language::trans('Prijavi se') }}</button>
				</div>
			</div>

			<div class="field-group error-login">
				<div class="row"> 
					<div class="col-md-9 wrong-email"> 
				<?php if($errors->first('email_login')){ echo $errors->first('email_login'); }elseif($errors->first('lozinka_login')){ echo $errors->first('lozinka_login'); } ?>

				@if(Session::get('confirm'))
					Niste potvrdili registraciju.<br>Posle registracije dobili ste potvrdu na vašu e-mail adresu!
				@endif

				@if(Session::get('message'))
					Novu lozinku za logovanje dobili ste na navedenu e-mail adresu.
				@endif
					</div>
				</div>
			</div>
		</div>

	</div> <!-- end of .login-form-wrapper -->
	<!-- <div class="large-1 large-push-6 form-spacer">&nbsp;</div> -->
</div>
@endsection