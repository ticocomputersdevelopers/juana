@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<!-- CONTACT.blade -->

<div class="row"> 
	<div class="col-xs-12">
		<br><br>
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11358.265695081936!2d20.168699269775402!3d44.626338999999994!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475a14682fc5667d%3A0x8ae5870310e6ecbb!2sFarbara%20%22Juana%22%20Obrenovac!5e0!3m2!1sen!2srs!4v1637831378898!5m2!1sen!2srs" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
	</div>
	<div class="col-md-5 col-sm-12 col-xs-12 sm-no-padd">

		<br>

		<h2><span class="section-title">{{ Language::trans('Kontakt informacije') }}</span></h2>

		<div class="shop-info">
			@if(Options::company_name() != '') 
			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Firma') }}:</div>
				<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_name() }}</div>
			</div> 
			@endif

			@if(Options::company_adress() != '')
			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Adresa') }}:</div>
				<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_adress() }}</div>
			</div>
			@endif
			
			@if(Options::company_city() != '')
			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Grad') }}:</div>
				<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_city() }}</div>
			</div>
			@endif
			
			@if(Options::company_phone() != '')
			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Telefon') }}:</div>
				<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_phone() }}</div>
			</div>
			@endif
			
			@if(Options::company_fax() != '')
			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Fax') }}:</div>
				<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_fax() }}</div>
			</div>
			@endif
			
			@if(Options::company_pib() != '')
			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('PIB') }}:</div>
				<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_pib() }}</div>
			</div>
			@endif
			
			@if(Options::company_maticni() != '')
			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Matični broj') }}:</div>
				<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_maticni() }}</div>
			</div>
			@endif
			
			@if( Options::company_email() != '')
			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('E-mail') }}:</div>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<a class="mailto" href="mailto:{{ Options::company_email() }}">{{ Options::company_email() }}</a>
				</div> 
			</div>
			@endif
		</div>

		<br>

		<div class="shop-info">
			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Maloprodajni objekat') }}:</div>
				<div class="col-md-7 col-sm-7 col-xs-7"> Dunav</div>
			</div> 

			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Adresa') }}:</div>
				<div class="col-md-7 col-sm-7 col-xs-7"> Vuka Karadžića 113</div>
			</div> 

			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Grad') }}:</div>
				<div class="col-md-7 col-sm-7 col-xs-7"> 1500 Obrenovac</div>
			</div> 

			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Telefon') }}:</div>
				<div class="col-md-7 col-sm-7 col-xs-7"> 011/8765076</div>
			</div> 
		</div>

		<br>

		<div class="shop-info">
			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Maloprodajni objekat') }}:</div>
				<div class="col-md-7 col-sm-7 col-xs-7"> Boja 7</div>
			</div> 

			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Adresa') }}:</div>
				<div class="col-md-7 col-sm-7 col-xs-7"> Braće Joksića 226</div>
			</div> 

			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Grad') }}:</div>
				<div class="col-md-7 col-sm-7 col-xs-7"> 11500 Obrenovac-Zvečka</div>
			</div> 

			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Telefon') }}:</div>
				<div class="col-md-7 col-sm-7 col-xs-7"> 011/8765076</div>
			</div> 
		</div>

		<br>

		<div class="shop-info">
			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Maloprodajni objekat') }}:</div>
				<div class="col-md-7 col-sm-7 col-xs-7"> Juana Home</div>
			</div> 

			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Adresa') }}:</div>
				<div class="col-md-7 col-sm-7 col-xs-7"> Miloša Obrenovića 41</div>
			</div> 

			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Grad') }}:</div>
				<div class="col-md-7 col-sm-7 col-xs-7"> 11500 Obrenovac</div>
			</div> 

			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Telefon') }}:</div>
				<div class="col-md-7 col-sm-7 col-xs-7"> 064/1171517</div>
			</div> 
		</div>

	</div> 


	<div class="col-md-7 col-sm-12 col-xs-12 sm-no-padd"> 

		<br>
		
		<h2><span class="section-title">{{ Language::trans('Pošaljite poruku') }}</span></h2>

		<form method="POST" action="{{ Options::base_url() }}contact-message-send">
			<div>
				<label id="label_name">{{ Language::trans('Vaše ime') }} *</label>
				<input class="contact-name" name="contact-name" id="JScontact_name" type="text" value="{{ Input::old('contact-name') }}">
				<div class="error red-dot-error">{{ $errors->first('contact-name') ? $errors->first('contact-name') : "" }}</div>
			</div> 

			<div>
				<label id="label_email">{{ Language::trans('Vaša e-mail adresa') }} *</label>
				<input class="contact-email" name="contact-email" id="JScontact_email" type="text" value="{{ Input::old('contact-email') }}" >
				<div class="error red-dot-error">{{ $errors->first('contact-email') ? $errors->first('contact-email') : "" }}</div>
			</div>		

			<div>	
				<label id="label_message">{{ Language::trans('Vaša poruka') }} </label>
				<textarea class="contact-message" name="contact-message" rows="5" id="message">{{ Input::old('contact-message') }}</textarea>
				<div class="error red-dot-error">{{ $errors->first('contact-message') ? $errors->first('contact-message') : "" }}</div>
			</div> 

			<div class="capcha text-center"> 
				{{ Captcha::img(5, 160, 50) }}<br>
				<span>{{ Language::trans('Unesite kod sa slike') }}</span>
				<input type="text" name="captcha-string" autocomplete="off">
				<div class="error red-dot-error">{{ $errors->first('captcha') ? $errors->first('captcha') : "" }}</div>
			</div>

			<div class="text-right"> 
				<button type="submit" class="button">{{ Language::trans('Pošalji') }}</button>
			</div>
		</form>
	</div>
</div>


@if(Options::company_map() != '' && Options::company_map() != ';')
<div class="map-frame relative">
	
	<div class="map-info">
		<h5>{{ Options::company_name() }}</h5>
		<h6>{{ Options::company_adress() }}, {{ Options::company_city() }}</h6> 
	</div>

	<iframe src="https://maps.google.com/maps?q={{ All::lat_long()[0] }},{{ All::lat_long()[1] }}&output=embed" width="100%" height="100%" frameborder="0" aria-hidden="false" tabindex="0"></iframe> 

</div> 
@endif
 

@if(Session::get('message'))
<script>
	$(document).ready(function(){     
 
        bootboxDialog({ message: "<p>{{ Session::get('message') }}</p>" }); 

	});
</script>
@endif

<!-- CONTACT.blade END -->

@endsection     