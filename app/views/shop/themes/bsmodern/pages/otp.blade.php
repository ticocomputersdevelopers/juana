@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

	<div class="col-md-12 col-sm-12 col-xs-12 text-center">
		<br><br>
		<h3>{{ Language::trans('Nakon potvrde vrši se preusmeravanje na sajt Otp banke, gde se na zaštićenoj stranici realizuje proces plaćanja') }}.</h3></br>

		<h4>{{ Language::trans('Podaci o korisniku') }}:</h4>
		<ul>
			@if($web_kupac->flag_vrsta_kupca == 1)
				<li>{{ Language::trans('Firma') }}: {{ $web_kupac->naziv }}</li>
			@else
				<li>{{ Language::trans('Ime') }}: {{ $web_kupac->ime.' '.$web_kupac->prezime }}</li>
			@endif
			<li>{{ Language::trans('Adresa') }}: {{ $web_kupac->adresa }}, {{ $web_kupac->mesto }}</li>
		</ul>
		</br>
		<h4>{{ Language::trans('Podaci o trgovcu') }}:</h4>
		<ul>
			<li>{{ $preduzece->naziv }}</li>
			<li>{{ Language::trans('Kontakt osoba') }}: {{ $preduzece->kontakt_osoba }}</li>
			@if($preduzece->matbr_registra != '')
			<li>{{ Language::trans('MBR') }}: {{ $preduzece->matbr_registra }}</li>
			@endif
			@if($preduzece->pib != '')
			<li>VAT Reg No: {{ $preduzece->pib }}</li>
			@endif
			<li>{{ $preduzece->adresa }}, {{ $preduzece->mesto }}</li>
			<li>{{ Language::trans('Telefon') }}: {{ $preduzece->telefon }}</li>
			<li><a href="mailto:{{ $preduzece->email }}">{{ $preduzece->email }}</a></li>
		</ul>
		</br>

		<input type="checkbox" id="JSConditions" onclick="clickCheckbox()"> <a href="{{ Options::base_url() }}uslovi-kupovine" target="_blank">
		{{ Language::trans('Uslovi korišćenja') }}</a>

		<!-- <form method="post" action="https://form.wspay.biz/authorization.aspx"> -->
		<form method="POST" action="https://virtualpostest.sia.eu/vpos/payments/main?PAGE=LAND">
 				
 				<input type="hidden" name="PAGE" value="LAND">
				<input type="hidden" name="AMOUNT" value="{{ $TotalAmount }}"> 
				<input type="hidden" name="CURRENCY" value="941"> 
				<input type="hidden" name="LANG" value="SR"> 
				<input type="hidden" name="SHOPID"  value="{{$ShopID}}"> 
				<input type="hidden" name="ORDERID" value="{{$ShoppingCartID}}"> 
				<input type="hidden" name="URLDONE" value="{{$ReturnURL}}"> 
				<input type="hidden" name="URLBACK" value="{{$ReturnErrorURL}}"> 
				<input type="hidden" name="URLMS" value="{{$CancelURL}}"> 
				<input type="hidden" name="ACCOUNTINGMODE" value="D"> 
				<input type="hidden" name="AUTHORMODE" value="I"> 
				<!-- <input type="hidden" name="OPTIONS" value="G">  -->
				<input type="hidden" name="EMAIL" value="{{ $web_kupac->email }}"> 
				<input type="hidden" name="SHOPEMAIL" value="{{$preduzece->email}}"> 
				
				<input type="hidden" name="MAC" value="{{$mac}}">
				
				<input type="submit" value="Završi plaćanje karticom" id="JSWsPaySubmit" disabled>
			
		</form>
		<script type="text/javascript">
			function clickCheckbox(){
				if(document.getElementById("JSConditions").checked){
					document.getElementById("JSWsPaySubmit").removeAttribute('disabled');
				}else{
					document.getElementById("JSWsPaySubmit").setAttribute('disabled','disabled');
				}
			}
		</script>
	</div>
@endsection