<div class="row">    
    <div class="col-xs-12">
        <h1 class="article-heading">{{ Product::seo_title($roba_id) }}</h1>
    </div>

    <div class="quick-view-wrapper col-md-5 col-sm-6 col-xs-12 JSproduct-preview-image no-padding">
        <div class="JSModalImageElement quick-view-wrapper">
            <div>
                <div class="bg-img JSModalImage" style="background-image: url( '/{{ $image }}' )"></div>
            </div>
            @if(All::provera_akcija($roba_id))
                <div class="label sale-label">  {{ Language::trans('Akcija') }} </div>
            @endif

            <div class="row">
                @foreach(Product::get_list_images($roba_id) as $listImage)
                    <div class="modal-gallery-img col-xs-3  text-center">
                        <a href="#!">
                            <img src="{{ Options::domain() }}{{ $listImage->putanja }}" class="JSModalListImage customImg img-responsive" />
                        </a>
                    </div>
                @endforeach   
            </div>
        </div>
    </div>

    <div class="col-md-7 col-sm-6 col-xs-12">
        <div class=" product-preview-info">
            <div class="product-preview-tab-button flex">
                {{Language::trans('Detalji')}}<i class="fas fa-plus"></i><i class="fas fa-minus"></i>
            </div>

            <div class="product-preview-tab-content">
                <div class="review hidden">{{ Product::getRating($roba_id) }}<span class="JSocenaCount">1</span><span class="JSocenaLabel">Ocena</span> <span>|</span> <a href="#product_preview_tabs">{{Language::trans('Dodaj svoj komentar')}}</a></div>
                
                <!-- ARTICLE PASSWORD -->
                @if(AdminOptions::sifra_view_web()==1)
                <div>{{Language::trans('Šifra') }}: {{Product::sifra($roba_id)}}</div>
                @elseif(AdminOptions::sifra_view_web()==4)                       
                <div>{{Language::trans('Šifra') }}: {{Product::sifra_d($roba_id)}}</div>
                @elseif(AdminOptions::sifra_view_web()==3)                       
                <div>{{Language::trans('Šifra') }}: {{Product::sku($roba_id)}}</div>
                @elseif(AdminOptions::sifra_view_web()==2)                       
                <div>{{Language::trans('Šifra') }}: {{Product::sifra_is($roba_id)}}</div>
                @endif 

                @if(Product::getStatusArticle($roba_id) == 1)
                    @if(Cart::check_avaliable($roba_id) > 0)
                        <span class="article-available-label">{{ Language::trans('Artikal je na stanju') }}</span>
                    @else
                        <span class="article-available-label" style="color: #dc3333">{{ Language::trans('Artikal nije na stanju') }}</span>
                    @endif
                @endif

                <!-- PRICE -->
                <div class="product-preview-price">
                    @if(Product::getStatusArticlePrice($roba_id) == 1)

        <!--                         @if(Product::pakovanje($roba_id)) 
                        <div> 
                            <span class="price-label">{{ Language::trans('Pakovanje') }}:</span>
                            <span class="price-num">{{ Product::ambalaza($roba_id) }}</span> 
                        </div>
                        @endif  -->                                

                        @if(All::provera_akcija($roba_id))    

                        <div class="inline-block"> 
                            <span class="price-label hidden">{{ Language::trans('Akcijska cijena')}}:</span> 
                            <span class="JSaction_price price-num main-price" data-action_price="{{Product::get_price($roba_id)}}">
                                {{ Cart::cena(Product::get_price($roba_id,true,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }}
                            </span>
                        </div>

                        @if(Product::get_mpcena($roba_id) != 0) 
                        <div class="inline-block">
                            <span class="price-label hidden">{{ Language::trans('Maloprodajna cijena') }}:</span>
                            <span class="price-num mp-price product-old-price">{{ Cart::cena(Product::get_mpcena($roba_id)) }}</span> 
                        </div>
                        @endif  


                        @if(Product::getPopust_akc($roba_id)>0)
                        <div class="hidden"> 
                            <span class="price-label hidden">{{ Language::trans('Popust') }}: </span>
                            <span class="price-num discount">{{ Cart::cena(Product::getPopust_akc($roba_id)) }}</span>
                        </div>
                        @endif

                        @else

                        @if(Product::get_mpcena($roba_id) != 0) 
                        <div class="inline-block hidden"> 
                            <span class="price-label hidden">{{ Language::trans('Maloprodajna cijena') }}:</span>
                            <span class="price-num mp-price">{{ Cart::cena(Product::get_mpcena($roba_id)) }}</span> 
                        </div>
                        @endif

                        <div class="inline-block"> 
                            <span class="price-label hidden">{{ Language::trans('Cijena sa uračunatim popustom za gotovinsko plaćanje') }}:</span>
                            <span class="JSweb_price price-num main-price" data-cena="{{Product::get_price($roba_id)}}">
                               {{ Cart::cena(Product::get_price($roba_id,true,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }}
                           </span>
                       </div>

                       @if(Product::getPopust($roba_id)>0)
                           @if(AdminOptions::web_options(132)==1)
                           <div class="hidden">  
                            <span class="price-label hidden">{{ Language::trans('Popust') }}:</span>
                            <span class="price-num discount">{{ Cart::cena(Product::getPopust($roba_id)) }}</span>
                            </div>
                            @endif
                        @endif

                    @endif
                @endif 
                </div>

                <div class="add-to-cart-area clearfix">    
                    @if(Product::getStatusArticle($roba_id) == 1)
                         @if(Cart::check_avaliable($roba_id) > 0)
                         <form method="POST" action="{{ Options::base_url() }}product-cart-add" id="JSAddCartForm" class="flex"> 
                          
                            <!-- PITCHPRINT -->
                            @if(!empty(Product::design_id($roba_id) AND !empty(Options::pitchprint() AND Options::pitchprint_aktiv() == 1)))
                                @include('shop/themes/'.Support::theme_path().'partials/pitchprint')
                            @endif
                            <!-- PITCHPRINT END -->

                        @if(Product::check_osobine($roba_id))  
                            @foreach(Product::osobine_nazivi($roba_id) as $osobina_naziv_id)

                            <div class="attributes text-bold">

                                <div>{{ Product::find_osobina_naziv($osobina_naziv_id,'naziv') }}</div>

                                @foreach(Product::osobine_vrednosti($roba_id,$osobina_naziv_id) as $osobina_vrednost_id)
                                <label class="relative no-margin" style="background-color: {{ Product::find_osobina_vrednost($osobina_vrednost_id, 'boja_css') }}">

                                    <input type="radio" name="osobine{{ $osobina_naziv_id }}" value="{{ $osobina_vrednost_id }}" {{ Product::check_osobina_vrednost($roba_id,$osobina_naziv_id,$osobina_vrednost_id,Input::old('osobine'.$osobina_naziv_id)) }}>

                                    <span class="inline-block">{{ Product::find_osobina_vrednost($osobina_vrednost_id, 'vrednost') }}</span>
                                    
                                </label>
                                @endforeach

                            </div>

                            @endforeach 
                        @endif

                        @if(AdminOptions::web_options(313)==1) 
                        <div class="num-rates"> 
                            <div> 
                                <div class="inline-block lorem-1">{{ Language::trans('Broj rata') }}</div>
                            </div>
                            <select class="JSinterest" name="kamata">
                                {{ Product::broj_rata(Input::old('kamata')) }}
                            </select>
                        </div>
                        @endif

                        <input type="hidden" name="roba_id" value="{{ $roba_id }}">
                        <span class="hidden">&nbsp;{{Language::trans('Količina')}}&nbsp;</span>
                    @if(Options::web_options(320) == 1 AND (Product::jedinica_mere($roba_id)->jedinica_mere_id) == 3 AND Product::pakovanje($roba_id) ) 
                        <input type="number" name="kolicina" class="cart-amount weight" min="0" step="0.1" value="{{ Input::old('kolicina') ? Input::old('kolicina') : '1' }}"><span>&nbsp;{{Language::trans(' kg')}}&nbsp;</span>
                    @else
                        <button onClick="var result = document.getElementById('productAmount'); var productAmount = result.value; if( !isNaN( productAmount ) &amp;&amp; productAmount &gt; 0 ) result.value--;return false;" class="product-count" type="button"><i class="fa fa-minus"></i></button>
                       
                        <input id="productAmount" type="text" name="kolicina" class="cart-amount" value="{{ Input::old('kolicina') ? Input::old('kolicina') : '1' }}">

                        <button onClick="var result = document.getElementById('productAmount'); var productAmount = result.value; if( !isNaN( productAmount )) result.value++;return false;" class="product-count" type="button"><i class="fa fa-plus"></i></button>
                    @endif

                        <button type="submit" id="JSAddCartSubmit" class="add-to-cart button relative"><span class="sprite sprite-cart"></span>{{Language::trans('Dodaj u Korpu')}}</button>
                        <input type="hidden" name="projectId" value=""> 

                        <div class="red-dot-error">{{ $errors->first('kolicina') ? $errors->first('kolicina') : '' }}</div>  

                       <!--  <div class="printer inline-block" title="{{ Language::trans('Štampaj') }}">  
                            <a href="{{Options::base_url()}}stampanje/{{ $roba_id }}" target="_blank" rel="nofollow"><i class="fas fa-print"></i></a>
                        </div> -->

                        <!-- @if(Cart::kupac_id() > 0)
                        <button class="like-it JSadd-to-wish" type="button" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}"><i class="far fa-heart"></i></button>
                        @else
                        <button class="like-it JSnot_logged" type="button" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}"><i class="far fa-heart"></i></button> 
                        @endif -->


                    </form>

                    @else

                    <button class="not-available button">{{Language::trans('Nije dostupno')}}</button>       
                    @endif

                    @else
                    <button class="button" data-roba-id="{{$roba_id}}">
                        {{ Product::find_flag_cene(Product::getStatusArticle($roba_id),'naziv') }}
                    </button>
                    @endif
                </div>
            </div>
        </div>

        <div class="product-preview-desc">
            <div class="product-preview-tab-button flex">
                {{Language::trans('Opis')}}<i class="fas fa-plus"></i><i class="fas fa-minus"></i>
            </div>
            
            <div class="product-preview-tab-content">
                {{ Product::get_opis($roba_id) }} 
            </div>
        </div>
        
        <div class="product-preview-specs">
            <div class="product-preview-tab-button flex">
                {{Language::trans('Specifikacije')}}<i class="fas fa-plus"></i><i class="fas fa-minus"></i>
            </div>
            
            <div class="product-preview-tab-content">
                {{ Product::get_karakteristike($roba_id) }}
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <a class="modal-more-details pull-right" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}">{{ Language::trans('Idite na stranicu proizvoda') }} <i class="fas fa-angle-right"></i></a>
    </div>
</div>


<script>
    $(document).on('click','.JSModalListImage',function(){
        var image_source = $(this).attr('src');
        $(this).closest('.JSModalImageElement').find('.JSModalImage').css('background-image','url('+image_source+')');
    });

    $('.product-preview-info').slideDown();
    $('.product-preview-info').find('.product-preview-tab-button').addClass('tab-button-open');
    $('.product-preview-info').find('.product-preview-tab-content').slideDown();

    $('.product-preview-tab-button').each(function() {
        var obj = $(this),
            content = $(this).parent().find('.product-preview-tab-content');


        obj.on('click', function() {
            if(!$(this).is('.tab-button-open')) {
                $('.product-preview-tab-button').stop().removeClass('tab-button-open')
                $('.product-preview-tab-content').stop().slideUp();
                obj.stop().toggleClass('tab-button-open');
                content.stop().slideToggle();
            } else {
                obj.stop().toggleClass('tab-button-open');
                content.stop().slideToggle();
            }
        });
    });
</script>