<!-- HEADER.blade -->
<header>   
    <div id="JSfixed_header" {{ Options::web_options(321, 'str_data') != '' ? 'style=background-color:' . Options::web_options(321, 'str_data') : '' }}>  
        <div class="container{{(Options::web_options(321)==1) ? '-fluid' : '' }}"> 
            <div class="row flex xs-header-ordered"> 

                <!-- RESPONSIVE BUTTON -->
                <div class="hidden-md hidden-lg col-sm-1 col-xs-1 text-center">
                    <div class="resp-nav-btn"><span class="fas fa-bars"></span></div>
                </div>

                <div class="col-md-3 col-sm-3 col-xs-4">
                    
                    <h1 class="seo">{{ Options::company_name() }}</h1>
                    
                    <a class="logo v-align inline-block" href="/" title="{{Options::company_name()}}" rel="nofollow">
                        <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" class="img-responsive"/>
                    </a>
                </div>

                <div class="col-md-6 col-sm-5 col-xs-12 hidden-sm hidden-xs">  
                    <ul class="main-menu text-white ">
                        @foreach(All::header_menu_pages() as $row)
                        <li>
                            @if(All::broj_cerki($row->web_b2c_seo_id) > 0)  
                            <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a>
                            <ul class="drop-2">
                                @foreach(All::header_menu_pages($row->web_b2c_seo_id) as $row2)
                                <li> 
                                    <a href="{{ Options::base_url().Url_mod::page_slug($row2->naziv_stranice)->slug }}">{{Url_mod::page_slug($row2->naziv_stranice)->naziv}}</a>
                                    <ul class="drop-3">
                                        @foreach(All::header_menu_pages($row2->web_b2c_seo_id) as $row3)
                                        <li> 
                                            <a href="{{ Options::base_url().Url_mod::page_slug($row3->naziv_stranice)->slug }}">{{Url_mod::page_slug($row3->naziv_stranice)->naziv}}</a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </li>
                                @endforeach
                            </ul>
                            @else   
                            <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a> 
                            @endif                    
                        </li>                     
                        @endforeach 

                        @if(Options::web_options(121)==1)
                        <?php $konfiguratori = All::getKonfiguratos(); ?>
                        @if(count($konfiguratori) > 0)
                        @if(count($konfiguratori) > 1)
                        <li>
                            
                            <a href="#!" rel="nofollow">{{Language::trans('Konfiguratori')}}</a>

                            <ul class="drop-2">
                                @foreach($konfiguratori as $row)
                                <li>
                                    <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $row->konfigurator_id }}">{{ Language::trans($row->naziv) }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                        @else
                        <li>
                            <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $konfiguratori[0]->konfigurator_id }}">
                                {{Language::trans('Konfigurator')}}
                            </a>
                        </li>
                        @endif
                        @endif
                        @endif
                    </ul>   
                </div>
 
                @include('shop/themes/'.Support::theme_path().'partials/cart_top')


            </div> 
        </div>  
    </div> 
</header>

<div class="menu-background">   
    <div class="container"> 
        <div id="responsive-nav" class="row">
            @if(Options::category_view()==1)
                @include('shop/themes/'.Support::theme_path().'partials/categories/category')
            @endif               

            <div class="col-md-5 pull-right text-right col-sm-12 col-xs-12 no-padding hidden-sm hidden-xs">
                <div class="row header-search"> 
                    @if(Options::gnrl_options(3055) == 0)
                        <div class="col-md-3 col-sm-3 col-xs-3 no-padding JSselectTxt hidden">  
                           
                            {{ Groups::firstGropusSelect('2') }} 
                
                        </div>
                    @else  
                        <input type="hidden" class="JSSearchGroup2" value="">
                    @endif 

                     
                    <div class="{{ Options::gnrl_options(3055) == 0 ? 'col-xs-12' : 'col-xs-12' }} no-padding JSsearchContent2">  
                        <div class="relative"> 
                            <form autocomplete="off" class="inline-block">
                                <input type="text" id="JSsearch2" placeholder="{{ Language::trans('Pretraga') }}" />
                            </form>      
                            <button onclick="search2()" class="JSsearch-button2"> <i class="fas fa-search"></i> </button>
                        </div>
                    </div> 

                </div> 
            </div> 

            <div class="col-xs-12 hidden-lg hidden-md no-padding">
                <ul class="main-menu text-white ">
                    @foreach(All::header_menu_pages() as $row)
                    <li>
                        @if(All::broj_cerki($row->web_b2c_seo_id) > 0)  
                        <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a>
                        <ul class="drop-2">
                            @foreach(All::header_menu_pages($row->web_b2c_seo_id) as $row2)
                            <li> 
                                <a href="{{ Options::base_url().Url_mod::page_slug($row2->naziv_stranice)->slug }}">{{Url_mod::page_slug($row2->naziv_stranice)->naziv}}</a>
                                <ul class="drop-3">
                                    @foreach(All::header_menu_pages($row2->web_b2c_seo_id) as $row3)
                                    <li> 
                                        <a href="{{ Options::base_url().Url_mod::page_slug($row3->naziv_stranice)->slug }}">{{Url_mod::page_slug($row3->naziv_stranice)->naziv}}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            @endforeach
                        </ul>
                        @else   
                        <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a> 
                        @endif                    
                    </li>                     
                    @endforeach 

                    @if(Options::web_options(121)==1)
                    <?php $konfiguratori = All::getKonfiguratos(); ?>
                    @if(count($konfiguratori) > 0)
                    @if(count($konfiguratori) > 1)
                    <li>
                        
                        <a href="#!" rel="nofollow">{{Language::trans('Konfiguratori')}}</a>

                        <ul class="drop-2">
                            @foreach($konfiguratori as $row)
                            <li>
                                <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $row->konfigurator_id }}">{{ Language::trans($row->naziv) }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </li>
                    @else
                    <li>
                        <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $konfiguratori[0]->konfigurator_id }}">
                            {{Language::trans('Konfigurator')}}
                        </a>
                    </li>
                    @endif
                    @endif
                    @endif
                </ul>   
            </div>
        </div>
    </div>    
</div> 

<div class="col-md-12 col-sm-12 col-xs-12 no-padding hidden-lg hidden-md small-search">
    <div class="row header-search flex"> 
        @if(Options::gnrl_options(3055) == 0)
            <div class="col-md-3 col-sm-3 col-xs-3 no-padding JSselectTxt hidden">  
               
                {{ Groups::firstGropusSelect('2') }} 
    
            </div>
        @else  
            <input type="hidden" class="JSSearchGroup2" value="">
        @endif 

         
        <div class="{{ Options::gnrl_options(3055) == 0 ? 'col-xs-12' : 'col-xs-12' }} no-padding JSsearchContent2 text-center">  
            <div class="relative"> 
                <form autocomplete="off" class="inline-block">
                    <input type="text" id="JSsearch2" placeholder="{{ Language::trans('Pretraga') }}" />
                </form>      
                <button onclick="search2()" class="JSsearch-button2"> <i class="fas fa-search"></i> </button>
            </div>
        </div> 

    </div> 
</div> 

<!-- HEADER.blade END -->

<!--========= QUICK VIEW MODAL ======== -->
<div class="modal fade" id="JSQuickView" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
            <button type="button" id="JSQuickViewCloseButton" class="close close-me-btn">
              <span>&times;</span>
            </button>
          </div>
          <div class="modal-body" id="JSQuickViewContent">
            <img alt="loader-image" class="gif-loader" src="{{Options::base_url()}}images/quick_view_loader.gif">
          </div>
      </div>    
    </div>
</div>