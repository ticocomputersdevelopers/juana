<div class="col-md-3 col-sm-8 col-xs-7 no-padding text-right flex-sm">
    <!-- sm login -->
	<div class="dropdown inline-block hidden-lg hidden-md">

        <button class="dropdown-toggle login-btn" type="button" data-toggle="dropdown">
           <span class="fas fa-user"></span>
        </button>

        @if(Session::has('b2c_kupac'))

        <ul class="dropdown-menu logged-dropdown-user">
            <a href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_user_name())}}" rel="nofollow">{{ WebKupac::get_user_name() }}</a>

            <a href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_company_name())}}" rel="nofollow">{{ WebKupac::get_company_name() }}</a>

            <div> 
                <!-- <span class="JSbroj_wish fas fa-heart"> {{ Cart::broj_wish() }} </span>   -->

                <a id="logout-button" class="inline-block" href="{{Options::base_url()}}logout" rel="nofollow">{{ Language::trans('Odjavi se') }}</a>
            </div>
        </ul>
        @else 
            <ul class="dropdown-menu login-dropdown">
                <!-- ====== LOGIN MODAL TRIGGER ========== -->
                <li>
                    <a href="{{Options::base_url()}}{{ Url_mod::slug_trans('prijava') }}" rel="nofollow">
                    <i class="fas fa-user"></i> {{ Language::trans('Prijavi se') }}</a>
                </li>
                <li>
                    <a href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}" rel="nofollow"> 
                    <i class="fas fa-user-plus"></i> {{ Language::trans('Registracija') }}</a>
                </li>
            </ul>
        @endif
    </div>

    <!-- sm search -->
    <div class="hidden-lg hidden-md">
        <button class="login-btn small-search-btn"><i class="fas fa-search"></i></button>
    </div>

	<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('korpa') }}" rel="nofollow">
	
		<div class="header-cart-container relative text-right inline-block">  
			<div class="top-cart-info relative">
				<div class="header-cart">
					<span class="fas fa-shopping-basket"></span>
				</div>

                <span class="hidden-lg hidden-md">
                    <span class="JSmini-cart-sum inline-block">{{ Cart::broj_cart() }}</span>
                </span>

				<span class="hidden-sm hidden-xs">
					<b>(<span class="JSmini-cart-sum inline-block">{{ Cart::broj_cart() }}</span>) {{ Language::trans('kom.') }}</b> <span class="JSmini-cart-flag"><span class="mini-cart-amount inline-block">{{ Cart::cena(Cart::cart_ukupno()) }}</span> <span class="mini-cart-pdv">{{ Language::trans('sa PDV-om') }}</span></span>
				</span>
			</div>

			<div class="JSheader-cart-content hidden-sm hidden-xs">
				@include('shop/themes/'.Support::theme_path().'partials/mini_cart_list') 
			</div>
		</div>

	</a>
</div> 