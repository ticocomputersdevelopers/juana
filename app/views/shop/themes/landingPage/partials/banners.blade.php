

<div class="banners">

	<?php foreach(DB::table('baneri')->where('tip_prikaza', 1)->orderBy('redni_broj','asc')->get() as $row){ ?>
	
		<div class="banners-bg" style='background-image: url( {{ Options::domain() . $row->img }} );' >

			<div class="container baners-desc-div">
				<div class="banner-desc">
					<h2 class="main-desc">WE KNOW WHAT'S BEST FOR YOUR MTB</h2>

					<p class="short-desc">Two professional riders, one store and years of experience. </p>
					
					<div>
						<a href="<?php echo $row->link; ?>" class="baner-link">
							Pročitaj više
						</a>
					</div>
				</div>
			</div>
		</div>
		
	<?php } ?>  
		
 
</div> 

   