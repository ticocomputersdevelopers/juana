
@if(Config::get('app.livemode') AND false)

<script>
	@if($strana == 'artikal')
        @if(Session::has('success_add_to_cart') AND Session::get('success_add_to_cart')['success'])
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
              "event": "eec.add",
              "ecommerce": {
                "currencyCode": "{{ Articles::get_valuta() }}",
                "add": {
                  "products": [{
                    "name": "{{ addslashes(str_replace("'","",$title)) }}",
                    "content_type": "product_group",
                    "item_group_id": parseInt("{{ ($category = Product::get_main_category($roba_id)) ? $category->grupa_pr_id : '' }}"),
                    "id": parseInt("{{ $roba_id }}"),
                    "price": "{{ Product::get_price($roba_id) }}",
                    "brand": "{{ addslashes(str_replace("'","",Product::get_proizvodjac_name($roba_id))) }}",
                    "category": "{{ ($category = Product::get_main_category($roba_id)) ? $category->grupa : 'None' }}",
                    "variant": "Standard",
                    "quantity": {{ Session::get('success_add_to_cart')['quantity'] }}
                   }]
                }
              },
              'eventCallback': function() {
          
              }
            });

        @elseif(empty($errors->first('kolicina')))
			window.dataLayer = window.dataLayer || [];
			window.dataLayer.push({
				"event": "eec.detail",
				"ecommerce": {
					"detail": {
						"actionField": {"list": "Stranica proizvoda"},
						"products": [
							{
								"name": "{{ addslashes(str_replace("'","",$title)) }}",
                    			"content_type": "product_group",
                    			"item_group_id": parseInt("{{ ($category = Product::get_main_category($roba_id)) ? $category->grupa_pr_id : '' }}"),
								"id": parseInt("{{ $roba_id }}"),
								"price": "{{ Product::get_price($roba_id) }}",
								"brand": "{{ addslashes(str_replace("'","",Product::get_proizvodjac_name($roba_id))) }}",
								"category": "{{ ($category = Product::get_main_category($roba_id)) ? $category->grupa : 'None' }}",
								"variant": "Standard"
							}
						]
					}
				}
			});

        @endif


    @elseif($strana == 'korpa' and count($articles_details) > 0)
    	var productObjects = $.parseJSON('<?php echo json_encode($articles_details); ?>');
    	// console.log(productObjects)
		var products = [];
		for (var i = 0; i < productObjects.length; i++) {
			window.dataLayer.push({
	  			"name": productObjects[i].name,
                "content_type": "product_group",
    			"item_group_id": productObjects[i].category_id,
	  			"id": productObjects[i].id,
	  			"price": productObjects[i].price,
	  			"brand": productObjects[i].brand,
	  			"category": productObjects[i].category,
	  			"variant": "Standard",
	  	        "quantity": productObjects[i].quantity
			});
		}

		  @if($errors->any())
		    var actionField = {"step": 2, "option": "{{ DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',Input::old('web_nacin_placanja_id'))->pluck('naziv') }}" };
		  @else
		  	var actionField = {"step": 1 };
		  @endif

		  window.dataLayer = window.dataLayer || [];
		  window.dataLayer.push({
		    "event": "eec.checkout",
		    "ecommerce": {
		      "checkout": {
		        "actionField": actionField,
		        "products": products
		     }
		   }
		  });

    @elseif($strana == 'order' and count($articles_details) > 0)

    	var productObjects = $.parseJSON('<?php echo json_encode($articles_details); ?>');
// console.log(productObjects)
		var products = [];
		for (var i = 0; i < productObjects.length; i++) {
			products.push({
	  			"name": productObjects[i].name,
                "content_type": "product_group",
    			"item_group_id": productObjects[i].category_id,
	  			"id": productObjects[i].id,
	  			"price": productObjects[i].price,
	  			"brand": productObjects[i].brand,
	  			"category": productObjects[i].category,
	  			"variant": "Standard",
	  	        "quantity": productObjects[i].quantity
			});
		}

		  window.dataLayer = window.dataLayer || [];
		  window.dataLayer.push({
		    "event": "eec.checkout",
		    "ecommerce": {
		      "checkout": {
		        "actionField": {"step": 2, "option": "{{ Order::n_p($web_b2c_narudzbina_id) }}"},
		        "products": products
		     }
		   }
		  });

		  window.dataLayer.push({
		    "event": "eec.purchase",
		    "ecommerce": {
              "currencyCode": "{{ strtoupper(Articles::get_valuta()) }}",
		      "purchase": {
			      "actionField": {
			        "transactionId": "{{ Order::broj_dokumenta($web_b2c_narudzbina_id) }}",
			        "transactionAffiliation": "Acme Clothing",
			        "transactionTotal": "{{ Order::narudzbina_ukupno($web_b2c_narudzbina_id) + ((Options::checkTroskoskovi_isporuke() == 1 AND Options::checkTezina() == 0 AND Order::narudzbina_ukupno($web_b2c_narudzbina_id) < Cart::cena_do() AND Cart::cena_do() > 0) ? Cart::cena_dostave() : 0) }}",
			        "transactionShipping": "{{ ((Options::checkTroskoskovi_isporuke() == 1 AND Options::checkTezina() == 0 AND Order::narudzbina_ukupno($web_b2c_narudzbina_id) < Cart::cena_do() AND Cart::cena_do() > 0) ? Cart::cena_dostave() : 0) }}"
			      },
		          "products": products
		     }
		   }
		  });
	@endif
</script>


<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','');</script>
<!-- End Google Tag Manager -->


@endif
