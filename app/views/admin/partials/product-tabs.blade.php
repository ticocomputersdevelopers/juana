<div class="m-tabs clearfix">
	<div class="m-tabs__tab{{ $strana == 'product' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/product/{{ $roba_id }}">{{ AdminLanguage::transAdmin('ARTIKAL') }} </a></div>
	@if(Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'slike' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/product_slike/{{ $roba_id }}">{{ AdminLanguage::transAdmin('SLIKE') }} </a></div>
	@endif
	@if(Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'opis' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/product_opis/{{ $roba_id }}">{{ AdminLanguage::transAdmin('OPIS') }} </a></div>
	@if(Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'karakteristike' || $strana == 'generisane' || $strana == 'dobavljac_karakteristike' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/product_karakteristike/{{ $roba_id }}">{{ AdminLanguage::transAdmin('KARAKTERISTIKE') }} </a></div>
	@endif
	<div class="m-tabs__tab{{ $strana == 'product_osobine' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/product-osobine/{{ $roba_id }}">{{ AdminLanguage::transAdmin('OSOBINE') }} </a></div>
	@if(Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'vezani_artikli' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/vezani_artikli/{{ $roba_id }}">{{ AdminLanguage::transAdmin('VEZANI ARTIKLI') }} </a></div>
	@endif
	@if(Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'srodni_artikli' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/srodni_artikli/{{ $roba_id }}">{{ AdminLanguage::transAdmin('SRODNI ARTIKLI') }} </a></div>
	@endif
	@if(Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
	<div class="m-tabs__tab{{ ($strana == 'product_akcija' OR $strana == 'product_akcija_b2b') ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/product_akcija/{{ $roba_id }}">{{ AdminLanguage::transAdmin('AKCIJA') }} </a></div>
	@endif
	@if(Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'dodatni_fajlovi' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/dodatni_fajlovi/{{ $roba_id }}">{{ AdminLanguage::transAdmin('FAJLOVI') }} </a></div>
	@endif
	@endif
	@if(Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'seo' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/product_seo/{{ $roba_id }}">{{ AdminLanguage::transAdmin('SEO I TAGOVI') }} </a></div>
	@endif
	@if(Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
	<div class="m-tabs__tab"><a href="{{AdminArticles::article_link($roba_id)}}" target="_blank">{{ AdminLanguage::transAdmin('VIDI ARTIKAL') }} </a></div>
	@endif
</div>