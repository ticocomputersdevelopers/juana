@if(Session::has('message'))
<script>
	alertify.success('{{ Session::get('message') }}');
</script>
@endif

<section class="comments-page" id="main-content">

	@include('admin/partials/tabs')

	<div class="row">
		<section class="medium-10 medium-centered columns">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Komentari') }}</h3> 
				<div class="table-scroll padding-h-8"> 
					<table>
						<thead>
							<tr>
								<th>{{ AdminLanguage::transAdmin('Ime') }}</th>
								<th>{{ AdminLanguage::transAdmin('Komentar') }}</th>
								<th>{{ AdminLanguage::transAdmin('Artikal') }}</th>
								<th>{{ AdminLanguage::transAdmin('Status') }}</th>
								<th>{{ AdminLanguage::transAdmin('Detaljnije') }}</th>
							</tr>
						</thead>
						<tbody>
							@foreach($comments as $row)
							<tr>
								<td>{{ $row->ime_osobe }}</td>
								
								<td class="no-white-space max-width-450">{{ $row->pitanje }}</td>
								
								<td>
									<a class="no-white-space max-width-450" href="{{AdminArticles::article_link($row->roba_id)}}" target="_blank">
										{{AdminArticles::find($row->roba_id, 'naziv')}}
									</a>
								</td>
								
								<td>
									@if($row->komentar_odobren == 1)
									<span class="radius secondary label">{{ AdminLanguage::transAdmin('Odobreno') }}</span>
									@else
									<span class="label success radius">{{ AdminLanguage::transAdmin('Novo') }}</span>	
									@endif
								</td>
								@if(!AdminOptions::checkB2B())
									<td><a href="{{AdminOptions::base_url()}}admin/komentari/{{$row->web_b2c_komentar_id}}">{{ AdminLanguage::transAdmin('Detaljnije') }}</a></td>
								@else
									<td><a href="{{AdminOptions::base_url()}}admin/komentari/{{$row->web_b2b_komentar_id}}">{{ AdminLanguage::transAdmin('Detaljnije') }}</a></td>
								@endif
							</tr>
							@endforeach
						</tbody>
					</table>
				</div> 
			</div>
		</section>

		<!-- ====================== -->
		<div class="text-center">
			<a href="#" class="video-manual" data-reveal-id="comments-manual">{{ AdminLanguage::transAdmin('Uputstvo') }} <i class="fa fa-film"></i></a>
			<div id="comments-manual" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
				<div class="video-manual-container"> 
					<p><span class="video-manual-title">{{ AdminLanguage::transAdmin('Komentari') }}</span></p>
					<iframe src="https://player.vimeo.com/video/271252889" width="840" height="426" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				</div>
				<a class="close-reveal-modal" aria-label="Close">&#215;</a>
			</div>
		</div>
		<!-- ====================== -->
	</div>
</section>