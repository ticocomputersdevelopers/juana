
<section id="main-content" class="orders-page row">
	<!-- ORDER LIST -->
	<section class="articles-listing medium-12 columns orders-listing">


		<!--=============================
		=            Filters            =
		==============================-->
		<div class="row"> 
			<div class="column medium-3"> 
				<h1 class="title-filters-h1">{{ AdminLanguage::transAdmin('Produžene garancije') }}</h1>

			 		<div id="orders-manual" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
						<div class="video-manual-container"> 
							<p><span class="video-manual-title">{{ AdminLanguage::transAdmin('Produžene garancije') }}</span></p>
							<iframe src="https://player.vimeo.com/video/271250335" width="840" height="426" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>
						<a class="close-reveal-modal" aria-label="Close">&#215;</a>
					</div>
			  	<!-- ====================== -->
			</div>

			<div class="column medium-3"> 
				<input type="text" class="search-field" id="JSSearch" value="{{$search}}" autocomplete="on" placeholder="{{ AdminLanguage::transAdmin('Pretraži Produžene garancije') }}...">
				<button type="submit" id="JSSearchBtn" class="m-input-and-button__btn btn btn-primary btn-radius abs-button">
					<i class="fa fa-search" aria-hidden="true"></i>
				</button>
			</div>

			<div class="columns medium-2">
				<input name="datum_od" type="text" value="{{ $datum_od }}" autocomplete="off" placeholder="Datum od" {{ Admin_model::check_admin(array('GARANCIJE_AZURIRANJE')) == false ? 'readonly' : '' }}>
			</div>
			<div class="columns medium-2">
				<input name="datum_do" type="text" value="{{ $datum_do }}" autocomplete="off" placeholder="Datum do" {{ Admin_model::check_admin(array('GARANCIJE_AZURIRANJE')) == false ? 'readonly' : '' }}>
			</div>
			<div class="columns medium-2">
				<button id="JSGarancijeIzvestaj" class="m-input-and-button__btn btn btn-primary btn-radius abs-button">{{ AdminLanguage::transAdmin('Izveštaj') }}</button>
			</div>
		 </div> 
  		
  		<div class="row"> 
  			<div class="column medium-12"> 
  		 	<div class="warr-cont">
				<table>
					<thead>
						<tr>
							<th>
							@if(Admin_model::check_admin(array('GARANCIJE_AZURIRANJE')))
								{{ AdminLanguage::transAdmin('Izmeni') }}
							@endif
							</th>
							<th class="JSSort" data-sort_column="row" data-sort_direction="{{ AdminGarancije::sortDirection('row',$sort_column,$sort_direction) }}" title="{{ AdminLanguage::transAdmin('Ukupno') }}">{{ AdminLanguage::transAdmin('Ukupno') }}: {{ $count }}</th>
							<th class="JSSort" data-sort_column="produzene_garancije_id" data-sort_direction="{{ AdminGarancije::sortDirection('produzene_garancije_id',$sort_column,$sort_direction) }}" title="{{ AdminLanguage::transAdmin('Redni broj') }}">{{ AdminLanguage::transAdmin('RB') }}</th>
							<th class="JSSort" data-sort_column="ime" data-sort_direction="{{ AdminGarancije::sortDirection('ime',$sort_column,$sort_direction) }}">{{ AdminLanguage::transAdmin('Ime') }}</th>
							<th class="JSSort" data-sort_column="prezime" data-sort_direction="{{ AdminGarancije::sortDirection('prezime',$sort_column,$sort_direction) }}">{{ AdminLanguage::transAdmin('Prezime') }}</th>
							<th class="JSSort" data-sort_column="adresa" data-sort_direction="{{ AdminGarancije::sortDirection('adresa',$sort_column,$sort_direction) }}">{{ AdminLanguage::transAdmin('Adresa') }}</th>
							<th class="JSSort" data-sort_column="grad" data-sort_direction="{{ AdminGarancije::sortDirection('grad',$sort_column,$sort_direction) }}">{{ AdminLanguage::transAdmin('Grad') }}</th>
<!-- 							<th class="JSSort" data-sort_column="email" data-sort_direction="{{ AdminGarancije::sortDirection('email',$sort_column,$sort_direction) }}">E-mail</th>
							<th class="JSSort" data-sort_column="telefon" data-sort_direction="{{ AdminGarancije::sortDirection('telefon',$sort_column,$sort_direction) }}">Tel</th> -->
							<!-- <th class="JSSort" data-sort_column="proizvodjac" data-sort_direction="{{ AdminGarancije::sortDirection('proizvodjac',$sort_column,$sort_direction) }}">Proizvođač</th> -->
							<th class="JSSort" data-sort_column="grupa" data-sort_direction="{{ AdminGarancije::sortDirection('grupa',$sort_column,$sort_direction) }}">{{ AdminLanguage::transAdmin('Grupa') }}</th>
							<th class="JSSort" data-sort_column="naziv_web" data-sort_direction="{{ AdminGarancije::sortDirection('naziv_web',$sort_column,$sort_direction) }}">{{ AdminLanguage::transAdmin('Proizvod') }}</th>
							<th class="JSSort" data-sort_column="serijski_broj" data-sort_direction="{{ AdminGarancije::sortDirection('serijski_broj',$sort_column,$sort_direction) }}">{{ AdminLanguage::transAdmin('Serijski broj') }}</th>
							<th class="JSSort" data-sort_column="maloprodaja" data-sort_direction="{{ AdminGarancije::sortDirection('maloprodaja',$sort_column,$sort_direction) }}" title="{{ AdminLanguage::transAdmin('Naziv maloprodaje') }}">{{ AdminLanguage::transAdmin('Naziv mp.') }}</th>
							<th class="JSSort" data-sort_column="prodavnica_mesto" data-sort_direction="{{ AdminGarancije::sortDirection('prodavnica_mesto',$sort_column,$sort_direction) }}" title="{{ AdminLanguage::transAdmin('Mesto maloprodaje') }}">{{ AdminLanguage::transAdmin('Mesto mp.') }}</th>
							<th class="JSSort" data-sort_column="fiskalni" data-sort_direction="{{ AdminGarancije::sortDirection('fiskalni',$sort_column,$sort_direction) }}" title="{{ AdminLanguage::transAdmin('Fiskalni račun') }}">{{ AdminLanguage::transAdmin('Fisk. račun') }}</th>
							<th class="JSSort" data-sort_column="final_parent_created" data-sort_direction="{{ AdminGarancije::sortDirection('final_parent_created',$sort_column,$sort_direction) }}">{{ AdminLanguage::transAdmin('Dat. kreiranja') }}</th>
							<th class="JSSort" data-sort_column="datum_kreiranja" data-sort_direction="{{ AdminGarancije::sortDirection('datum_kreiranja',$sort_column,$sort_direction) }}">{{ AdminLanguage::transAdmin('Poslednja izmena') }}</th>
						</tr>
					</thead>
					<tbody>  
						
						@foreach($garancije as $row)
						<tr>
							<td><a href="{{ AdminOptions::base_url() }}admin/garancija/{{ $row->produzene_garancije_id }}">
							@if(Admin_model::check_admin(array('GARANCIJE_AZURIRANJE')))
								{{ AdminLanguage::transAdmin('Izmeni') }}
							@endif
							</a></td>
							<td>{{$row->row}}</td> 
							<td>{{$row->produzene_garancije_id}}</td> 
							<td>{{$row->ime}}</td> 
							<td>{{$row->prezime}}</td> 
							<td>{{$row->adresa}}</td> 
							<td>{{$row->grad}}</td> 
				<!-- 			<td>{{$row->email}}</td> 
							<td>{{$row->telefon}}</td>  -->
							<!-- <td>{{$row->proizvodjac}}</td>  -->
							<td>{{$row->grupa}}</td> 
							<td>{{$row->naziv_web}}</td> 
							<td>{{$row->serijski_broj}}</td> 
							<td>{{$row->maloprodaja}}</td> 
							<td>{{$row->prodavnica_mesto}}</td> 
							<td>{{$row->fiskalni}}</td> 
							<td>{{ $row->final_parent_created }}</td> 
							<td>{{ ($row->final_parent_created == $row->datum_kreiranja) ? AdminLanguage::transAdmin('Nema izmena') : $row->datum_kreiranja }}</td> 
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
				{{ Paginator::make($garancije, $count, $limit)->links() }}
		</div>
	</div>
	</section>
	
</section> <!-- end of #main-content -->
