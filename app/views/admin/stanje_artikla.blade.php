<div id="main-content" >
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif

	@include('admin/partials/tabs')

	<div class="row">
		<div class="medium-3 columns">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Izaberi stanje') }}</h3>
				<select class="JSeditSupport">
					<option value="{{ AdminOptions::base_url() }}admin/stanje-artikla">{{ AdminLanguage::transAdmin('Dodaj novi') }}</option>
					@foreach(AdminSupport::getFlagCene(true) as $row)
					<option value="{{ AdminOptions::base_url() }}admin/stanje-artikla/{{ $row->roba_flag_cene_id }}" @if($row->roba_flag_cene_id == $roba_flag_cene_id) {{ 'selected' }} @endif>({{ $row->roba_flag_cene_id }}) {{ $row->naziv }} </option>
					@endforeach
				</select>
			</div>
		</div>

		<section class="medium-5 columns">
			<div class="flat-box">
				<h1 class="title-med">{{ $title }} {{ $roba_flag_cene_id == 1 ? AdminLanguage::transAdmin('Podrazumevani') : '' }}</h1>
				
				<form method="POST" action="{{ AdminOptions::base_url() }}admin/stanje-artikla-edit" enctype="multipart/form-data">
					<input type="hidden" name="roba_flag_cene_id" value="{{ $roba_flag_cene_id }}">
					
					<div class="row">
						<div class="columns medium-6 {{ $errors->first('naziv') ? ' error' : '' }}">
							<label for="naziv">{{ AdminLanguage::transAdmin('Naziv stanja') }}</label>
							<input type="text" name="naziv" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : $naziv) }}" autofocus="autofocus" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
						<div class="columns medium-6">
							<label>{{ AdminLanguage::transAdmin('Prikaz cene') }}</label>
							<select name="prikaz_cene" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								{{ AdminSupport::selectCheck(Input::old('prikaz_cene') ? Input::old('prikaz_cene') : $prikaz_cene) }}
							</select>
						</div>

						@if(AdminOptions::checkB2C())
						<div class="columns medium-6">
							<label>{{ AdminLanguage::transAdmin('B2C aktivno') }}</label>
							<select name="selected" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								{{ AdminSupport::selectCheck(Input::old('selected') ? Input::old('selected') : $selected) }}
							</select>
						</div>
						@endif

						<div class="columns medium-6">
							@if(AdminOptions::checkB2B())
							<label>{{ AdminLanguage::transAdmin('B2B aktivno') }}</label>
							<select name="b2b_selected" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								{{ AdminSupport::selectCheck(Input::old('b2b_selected') ? Input::old('b2b_selected') : $b2b_selected) }}
							</select>
							@endif
						</div>
					</div>

					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE'))) 
					<div class="btn-container center">
						<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
						@if($roba_flag_cene_id != null AND $roba_flag_cene_id != 1)	
						<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/stanje-artikla-delete/{{ $roba_flag_cene_id }}">{{ AdminLanguage::transAdmin('Obriši') }}</button>
						@endif
					</div> 
					@endif
				</form>
			</div>  
		</section>
	</div>  
</div>