<div class="row"> 
	<div class="napomena">
		<p>{{ AdminLanguage::transAdmin('Kod placanja - poziv na broj') }}: {{$predracun->broj_dokumenta}}</p>
		<p><strong>{{ AdminLanguage::transAdmin('Napomena o poreskom oslobadjanju') }}:</strong></p>
		<p></p>
	</div>
</div>

<div class="row"> 
	<table class="signature">
		<tr>
			<td class=""></td>
			<td class="text-right"><span class="robu_izdao">{{ AdminLanguage::transAdmin('Predračun izdao') }}</span> ___________________________</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td style='width: 60%;'>
				<p style="border-top: 1px solid #666;"> {{ AdminLanguage::transAdmin('Predračun je izrađen na računaru i punovažan je bez pečata i potipisa') }}</p>
			</td>
			<td class=""></td>
		</tr>
	</table>
</div>