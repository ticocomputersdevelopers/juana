<!-- @extends('b2b.templates.login')
@section('content')
<div class="b2boverlay-login"></div>
<div class="b2bLoginContainer">
    <div class="b2b-flex">
        <div class="center"> 
            <a class="logo-b2b-login" href="{{ B2bOptions::base_url()}}" title="{{ B2bOptions::company_name()}}">
                <img class="b2b-login-img" src="{{ B2bOptions::base_url()}}{{ B2bOptions::company_logo()}}" alt="">
            </a>
        </div>  
     
        <div class="b2bForm"> 
            <form action="{{route('b2b.login.store')}}" method="post"> 
                <div class="field-group">
                    <label for="username">E-mail</label>
                    <input id="username" name="username" type="text" value="{{Input::old('username')}}" class="login-form__input form-control"> 
                    @if($errors->first('username'))
                        <span class="error">{{ $errors->first('username') }}</span>
                    @endif
                </div> 
                <div class="field-group">
                    <label for="password">Lozinka</label>
                    <input id="password" name="password" type="password" value="{{Input::old('password')}}" class="login-form__input form-control"> 
                     @if($errors->first('password'))
                        <span class="error">{{ $errors->first('password') }}</span>
                     @endif
                </div> 
                <div class="field-group"> 
                    <div> 
                        <button class="submit btn" onclick="login()">PRIJAVI SE</button> 
                    </div>
                    <div class="text-right"> 
                        Nemate nalog? 
                        @if(!B2bOptions::info_sys('calculus'))
                        <a class="submit" href="{{route('b2b.registration')}}"> Registrujte se</a>
                        @endif
                    </div>
                    <div> 
                        @if(!B2bOptions::info_sys('wings') AND !B2bOptions::info_sys('calculus'))
                        <a class="forgotten-psw" href="{{ route('b2b.forgot_password') }}">Zaboravljena lozinka</a>
                        @endif 
                    </div>   
                </div>
            </form> 
            <span class="text-right">B2B ENGINE BY: 
                 <a href="https://www.selltico.com/" target="_blank"> <img src="{{ Options::domain() }}images/logo-selltico.png"></a>
            </span> 
        </div>
    </div>
</div> 
@endsection -->