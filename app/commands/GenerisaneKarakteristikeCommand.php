<?php
require __DIR__.'/../../vendor/autoload.php';

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

// use PHPExcel; 
// use PHPExcel_IOFactory;

class GenerisaneKarakteristikeCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'import:generisane';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}
	
	public static function MappedGroups(){
		$mapped = array();
		$groups = DB::table('grupa_pr')->whereNotNull('grupa')->where('grupa','!=','')->get();
		foreach($groups as $group){
			$mapped[$group->grupa] = $group->grupa_pr_id;
		}
		return $mapped;
	}
	public static function MappedGroupsNaziv(){
		$mapped = array();
		$groups = DB::select("SELECT grupa_pr_naziv_id,naziv,(SELECT grupa FROM grupa_pr WHERE grupa_pr_id = gpn.grupa_pr_id) as grupa FROM grupa_pr_naziv gpn");
		foreach($groups as $group){
			$mapped[$group->grupa.'<=>'.$group->naziv] = $group->grupa_pr_naziv_id;
		}
		return $mapped;
	}
	public static function MappedGroupsVrednosti(){
		$mapped = array();
		$groups = DB::select("SELECT gp.grupa, gpn.naziv as naziv, gpv.naziv as vrednost, gpv.grupa_pr_vrednost_id FROM grupa_pr_vrednost gpv LEFT JOIN grupa_pr_naziv gpn ON gpn.grupa_pr_naziv_id = gpv.grupa_pr_naziv_id LEFT JOIN grupa_pr gp ON gp.grupa_pr_id = gpn.grupa_pr_id ");
		foreach($groups as $group){
			$mapped[$group->grupa.'<=>'.$group->naziv.'<=>'.$group->vrednost ] = $group->grupa_pr_vrednost_id;
		}
		return $mapped;
	}

	public static function MappedRoba(){
		$mapped = array();
		$roba = DB::table('roba')->whereNotNull('sifra_d')->where('sifra_d','!=','')->get();
		foreach($roba as $row){
			$mapped[$row->sifra_d] = $row->roba_id;
		}
		return $mapped;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire(){	

		$karakteristike_nazivi=[
			'E'=>'Grlo',
			'F'=>'Dimabilnost',
			'G'=>'Garancija',
			'H'=>'Snaga',
			'I'=>'Temperatura svetlosti',
			'K'=>'Boja svetlosti',
			'S'=>'IP zaštita',
			'T'=>'Radni napon',
			'V'=>'Boja artikla'
		];
		$mappedGroups=self::MappedGroups();

		$grupa_pr_naziv_arr=array();
		foreach( DB::select("SELECT grupa_pr_id,grupa FROM grupa_pr WHERE grupa_pr_id NOT IN (SELECT DISTINCT parrent_grupa_pr_id FROM grupa_pr WHERE parrent_grupa_pr_id is not null ) ") as $grupa ){
			$rbr=1;
			foreach ($karakteristike_nazivi as $karakteristika) {				
				$grupa_pr_naziv_arr[] = "(".$grupa->grupa_pr_id.", '".$karakteristika."', ".$rbr.")"; 
				$rbr++;
			}
		}

		$grupa_pr_naziv_temp="(VALUES ".implode(',', $grupa_pr_naziv_arr).") 
	    	grupa_pr_naziv_temp(grupa_pr_id,naziv,rbr)";

	    DB::statement("INSERT INTO grupa_pr_naziv (grupa_pr_id,naziv,rbr) SELECT * FROM ".$grupa_pr_naziv_temp." WHERE (grupa_pr_id,naziv,rbr) NOT IN ( SELECT grupa_pr_id,naziv,rbr FROM grupa_pr_naziv) ");
	   
		$file_karak_vrednosti=array();	   

		$products_file = "fajl.xlsx";
		
		$excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	    $excelObj = $excelReader->load($products_file);

		$i = 0;
		while ($excelObj->setActiveSheetIndex($i)){

	    	$worksheet = $excelObj->getActiveSheet();
			$lastRow = $worksheet->getHighestRow();
			$sheetCount = $excelObj->getSheetCount();
			$lastSheet = $sheetCount;

	   		$i++;
	   		if($i == $lastSheet){
	   			break;
	   		}
	           

	        for ($row = 2; $row <= $lastRow; $row++) {

	        	$sifra = $worksheet->getCell('B'.$row)->getValue();
	        	$grupa = $worksheet->getCell('D'.$row)->getValue();

	        	foreach ($karakteristike_nazivi as $key => $karakteristika_naziv) {
	        		$karak_value=trim($worksheet->getCell($key.$row)->getValue());
		        	if(!empty($karak_value) AND $karak_value !='-'){
			        	if(!isset($file_karak_vrednosti[$grupa.'<=>'.$karakteristika_naziv])) {
			        		$file_karak_vrednosti[$grupa.'<=>'.$karakteristika_naziv]=array();
			        	}
			        	if(!in_array($karak_value, $file_karak_vrednosti[$grupa.'<=>'.$karakteristika_naziv])){
			        		$file_karak_vrednosti[$grupa.'<=>'.$karakteristika_naziv][]=$karak_value;
			        	}
			        }
	        	}
	        }
    	}
       
        $mappedGroupsNaziv=self::MappedGroupsNaziv();
		$grupa_pr_vrednost_arr=array();

		foreach( $file_karak_vrednosti as $key => $karak_vrednosti ){			
			if(isset($mappedGroupsNaziv[$key])){	
				foreach ($karak_vrednosti as $rbr => $vrednost) {								
					$grupa_pr_vrednost_arr[] = "(".$mappedGroupsNaziv[$key].", '".$vrednost."', ".($rbr+1).")";			
				}			
			}
		}

		$grupa_pr_vrednost_temp="(VALUES ".implode(',', $grupa_pr_vrednost_arr).") 
	    	grupa_pr_vrednost_temp(grupa_pr_naziv_id,naziv,rbr)";

	    DB::statement("INSERT INTO grupa_pr_vrednost (grupa_pr_naziv_id,naziv,rbr) SELECT * FROM ".$grupa_pr_vrednost_temp." WHERE (grupa_pr_naziv_id,naziv,rbr) NOT IN ( SELECT grupa_pr_naziv_id,naziv,rbr FROM grupa_pr_vrednost) ");

		$products_file = "fajl.xlsx";
		
		$excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	    $excelObj = $excelReader->load($products_file);

	    $web_roba_karak_temp_arr=array();
	    $mappedRoba=self::MappedRoba();
		$mappedGroupsVrednosti=self::MappedGroupsVrednosti();

		$i = 0;
		while ($excelObj->setActiveSheetIndex($i)){

	    	$worksheet = $excelObj->getActiveSheet();
			$lastRow = $worksheet->getHighestRow();
			$sheetCount = $excelObj->getSheetCount();
			$lastSheet = $sheetCount;

	   		$i++;
	   		if($i == $lastSheet){
	   			break;
	   		}	           

	        for ($row = 2; $row <= $lastRow; $row++) {

	        	$sifra = $worksheet->getCell('B'.$row)->getValue();
	        	$grupa = $worksheet->getCell('D'.$row)->getValue();
				$rbr=1;
				
				foreach ($karakteristike_nazivi as $key => $karakteristika_naziv) {
					$karak_value=trim($worksheet->getCell($key.$row)->getValue());
					
					if(!empty($karak_value) AND $karak_value !='-'){
						if(isset($mappedRoba[$sifra]) && isset($mappedGroupsVrednosti[$grupa.'<=>'.$karakteristika_naziv.'<=>'.$karak_value]) ){
						// $web_roba_karak_temp_arr[] = "(roba_id, rbr, grupa_pr_naziv_id, vrednost, grupa_pr_vrednost_id)"; 						   
						   $grupa_pr_naziv_id=isset($mappedGroupsNaziv[$grupa.'<=>'.$karakteristika_naziv]) ? $mappedGroupsNaziv[$grupa.'<=>'.$karakteristika_naziv] : -1 ;
						   $web_roba_karak_temp_arr[] = "(".$mappedRoba[$sifra].", ".$rbr.", ".$grupa_pr_naziv_id.", '".$karak_value."', ".$mappedGroupsVrednosti[$grupa.'<=>'.$karakteristika_naziv.'<=>'.$karak_value].")"; 
						   $rbr++;
						}
					}
				}	
	        	
	        }		
    	}
    	$web_roba_karak_temp="(VALUES ".implode(',', $web_roba_karak_temp_arr).") 
	    	web_roba_karakteristike_temp(roba_id, rbr, grupa_pr_naziv_id, vrednost, grupa_pr_vrednost_id)";

	    DB::statement("INSERT INTO web_roba_karakteristike (roba_id, rbr, grupa_pr_naziv_id, vrednost, grupa_pr_vrednost_id) SELECT * FROM ".$web_roba_karak_temp." WHERE (roba_id, rbr, grupa_pr_naziv_id, vrednost, grupa_pr_vrednost_id) NOT IN ( SELECT roba_id, rbr, grupa_pr_naziv_id, vrednost, grupa_pr_vrednost_id FROM web_roba_karakteristike) ");
	    
		$this->info('Finish.');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		// 	array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
