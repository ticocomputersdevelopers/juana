<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


class DCMappingCommand extends Command {
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'dc:mapping';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'DC Mapping Command.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $dc_ids = array_map('current',DB::select("SELECT dobavljac_cenovnik_id FROM dobavljac_cenovnik WHERE partner_id IN (SELECT partner_id FROM dobavljac_cenovnik_kolone WHERE partner_id <> -1) AND roba_id = -1 AND povezan = 0 AND flag_zakljucan = 0"));
        AdminImport::dc_mapped($dc_ids,($this->argument('mapped_all') == true ? true : false));
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('mapped_all', InputArgument::OPTIONAL, 'Image path on root.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}