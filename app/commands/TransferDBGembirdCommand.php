<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use IsLogik\Support as ISLogikSupport;
use Import\Support as ImportSupport;

class TransferDBGembirdCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'transfer:db:gembird';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}
	public static function convert($string){
		// $string = iconv("UTF-8", "WINDOWS-1250//TRANSLIT//IGNORE",$string);
		$string = iconv(mb_detect_encoding($string, mb_detect_order(), true), "UTF-8//TRANSLIT//IGNORE",$string);

		return $string;
	}
	public static function convert1250($string){
		// $string = iconv("UTF-8", "WINDOWS-1250//TRANSLIT//IGNORE",$string);
		$string = iconv(mb_detect_encoding($string, mb_detect_order(), true), "WINDOWS-1250//TRANSLIT//IGNORE",$string);

		// $string = preg_replace('/[^a-zA-Z0-9\ \!\%\&\(\)\=\*\/\,\.\+\-\_\@\?\:\;\<\>\'\"\č\ć\ž\š\đ\Č\Ć\Ž\Š\Đ]/', '',iconv(mb_detect_encoding($string, mb_detect_order(), true), "ISO_8859_5//TRANSLIT//IGNORE",$string));
		// $string = utf8_decode(utf8_encode($string));

		return $string;
	}

	public static function decrypt($string){
		$key = 'opIaod3392iiu';
	    $sizes = array(16,24,32);
	    foreach($sizes as $s){
	        while(strlen($key) < $s) $key = $key."\0";
	        if(strlen($key) == $s) break;
	    }
		$string = base64_decode($string);
		return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128,$key,$string,MCRYPT_MODE_CBC));
	}

	public static function getMappedArticles(){
		$mapped = array();
		$articles = DB::table('roba')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($articles as $article){
			$mapped[$article->id_is] = $article->roba_id;
		}
		return $mapped;
	}
	public static function getMappedGroups(){
		$mapped = array();
		$groups = DB::table('grupa_pr')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($groups as $group){
			$mapped[$group->id_is] = $group->grupa_pr_id;
		}
		return $mapped;
	}
	public static function getMappedManufacturers(){
		$mapped = array();
		$manufacturers = DB::table('proizvodjac')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($manufacturers as $group){
			$mapped[$group->id_is] = $group->proizvodjac_id;
		}
		return $mapped;
	}
	public static function getMappedPartners(){
		$mapped = array();
		$partners = DB::table('partner')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($partners as $partner){
			$mapped[$partner->id_is] = $partner->partner_id;
		}
		return $mapped;
	}

	public static function fileExtension($image_path){
		$extension = 'jpg';
    	$slash_parts = explode('/',$image_path);
    	$old_name = $slash_parts[count($slash_parts)-1];
    	$old_name_arr = explode('.',$old_name);
    	$extension = $old_name_arr[count($old_name_arr)-1];
    	return $extension;
	}

	/**
	 * Execute the console command. 
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$project_dir = $this->argument('project_dir');

		$transfer_pgsql = DB::connection('transfer_pgsql');

		// grupe
		$groups = $transfer_pgsql->select("SELECT * FROM grupa_pr WHERE grupa_pr_id >0 ");

		$group_rows ="";
		foreach ($groups as $group) {
			$group_rows .= "(nextval('grupa_pr_grupa_pr_id_seq'),'".$group->grupa."','".$group->opis."',(".(is_null($group->parrent_grupa_pr_id) ? 'NULL' : $group->parrent_grupa_pr_id ).")::integer,nextval('grupa_pr_grupa_pr_id_seq'),".$group->web_b2b_prikazi.",".$group->web_b2c_prikazi.",".$group->prikaz.",'".(!empty($group->sifra_connect) ? trim($group->sifra_connect) : '')."',NULL,NULL,NULL,NULL,0,".((!is_null($group->redni_broj) && $group->redni_broj!='') ? $group->redni_broj : '0').",'".$group->putanja_slika."','".$group->keywords."',(NULL)::integer,NULL,'".$group->grupa_pr_id."',NULL),";
		}

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='grupa_pr'"));

		$table_temp = "(VALUES ".substr($group_rows,0,-1).") grupa_pr_temp(".implode(',',$columns).")";
		DB::statement("INSERT INTO grupa_pr (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM grupa_pr t WHERE t.grupa=grupa_pr_temp.grupa))");
		DB::statement("update grupa_pr gp set parrent_grupa_pr_id = (select grupa_pr_id from grupa_pr where (id_is)::integer = gp.parrent_grupa_pr_id limit 1) where parrent_grupa_pr_id > 0");
		$getMappedGroups = self::getMappedGroups();


		// proizvodjaci
		$proizvodjaci = $transfer_pgsql->select("SELECT * FROM proizvodjac WHERE proizvodjac_id > -1");
		$proizvodjac_rows="";
		foreach ($proizvodjaci as $proizvodjac) {
		$proizvodjac_rows .= "(nextval('proizvodjac_proizvodjac_id_seq'),'".$proizvodjac->naziv."','".trim($proizvodjac->sifra)."',0,0,'".(!empty($proizvodjac->sifra_connect) ? trim($proizvodjac->sifra_connect) : '')."',NULL,NULL,NULL,1,(NULL)::integer,(NULL)::integer,'".$proizvodjac->proizvodjac_id."',0),";
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='proizvodjac'"));
		$table_temp = "(VALUES ".substr($proizvodjac_rows,0,-1).") proizvodjac_temp(".implode(',',$columns).")";		

		DB::statement("INSERT INTO proizvodjac (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM proizvodjac t WHERE t.naziv=proizvodjac_temp.naziv))");
		$getMappedManufacturers = self::getMappedManufacturers();


		// partneri
		$partneri = $transfer_pgsql->select("SELECT *, (select mesto from mesto where mesto_id = p.mesto_id) AS mesto FROM partner p WHERE partner_id >0 and partner_id <> 3447");
		$partner_rows="";
		foreach ($partneri as $partner) {
		$partner_rows .= "(nextval('partner_partner_id_seq'),'".$partner->sifra."','".pg_escape_string(trim($partner->naziv))."','".pg_escape_string(trim($partner->adresa))."','".trim($partner->mesto)."',0,'".pg_escape_string(trim($partner->telefon))."',NULL,'".strval($partner->pib)."','".strval($partner->broj_maticni)."',NULL,NULL,NULL,NULL,0,0,0,'".pg_escape_string(trim($partner->naziv_puni))."',0,0,1,1,0,'".$partner->login."','".$partner->password."','".strval($partner->racun)."',NULL,0,NULL,NULL,NULL,'".trim($partner->mail)."','".(!empty($partner->sifra_connect) ? trim($partner->sifra_connect) : '')."',1,0,0,NULL,1,'".$partner->partner_id."',(NULL)::integer,NULL,NULL,(NULL)::integer),";
		}
		$partner_rows .= "(nextval('partner_partner_id_seq'),'A03514','RTL BECEJ','PETEFI SANDORA 38',NULL,0,'+381 61 2012122',NULL,'105709719',NULL,NULL,NULL,NULL,NULL,0,0,0,'RTL BECEJ',0,0,1,1,0,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'3505',1,0,0,NULL,1,'3447',(NULL)::integer,NULL,NULL,(NULL)::integer),";

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='partner'"));
		$table_temp = "(VALUES ".substr($partner_rows,0,-1).") partner_temp(".implode(',',$columns).")";		

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("INSERT INTO partner (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM partner t WHERE t.naziv=partner_temp.naziv))");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		$getMappedPartners = self::getMappedPartners();

		//partner_je
		$partner_jes = $transfer_pgsql->select("SELECT * FROM partner_je");
		$partner_je_rows=array();
		if(count($partner_jes) > 0){
			foreach($partner_jes as $partner_je){
			    if(isset($getMappedPartners[$partner_je->partner_id])){
			    	$partner_id = $getMappedPartners[$partner_je->partner_id];
			    	$partner_je_rows[] = "(".$partner_id.",".$partner_je->partner_vrsta_id.")";
				}
			}
		}
		if(count($partner_je_rows) > 0){
			DB::statement("INSERT INTO partner_je (partner_id,partner_vrsta_id) VALUES ".implode(',',$partner_je_rows)."");
		}

		//partner_rabat_grupa
		$partner_grupas = $transfer_pgsql->select("SELECT * FROM partner_rabat_grupa");
		$partner_grupa_rows=array();
		if(count($partner_grupas) > 0){
			foreach($partner_grupas as $partner_grupa){
			    if(isset($getMappedGroups[$partner_grupa->grupa_pr_id]) && isset($getMappedPartners[$partner_grupa->partner_id])){
			    	$partner_id = $getMappedPartners[$partner_grupa->partner_id];
			    	$grupa_pr_id = $getMappedGroups[$partner_grupa->grupa_pr_id];
			    	$partner_grupa_rows[] = "(".$partner_id.",".$grupa_pr_id.",".$partner_grupa->rabat.",".trim($partner_grupa->sifra_kupca_logik).",".trim($partner_grupa->sifra_kategorije_logik).",-1)";
				}
			}
		}
		if(count($partner_grupa_rows) > 0){
			DB::statement("INSERT INTO partner_rabat_grupa (partner_id,grupa_pr_id,rabat,sifra_kupca_logic,sifra_kategorije_logic,proizvodjac_id) VALUES ".implode(',',$partner_grupa_rows)."");
		}

		// web_kupac
		$kupci = $transfer_pgsql->select("SELECT *, (select mesto from mesto where mesto_id = wk.mesto_id) AS mesto FROM web_kupac wk WHERE web_kupac_id >0 ");

		$kupac_rows="";
		foreach ($kupci as $kupac) {
		$kupac_rows .= "(nextval('web_kupac_web_kupac_id_seq'),NULL,'".$kupac->lozinka."','".pg_escape_string($kupac->ime)."','".pg_escape_string($kupac->adresa)."','".pg_escape_string($kupac->telefon)."','".$kupac->email."',(".($kupac->flag_potvrda == '' ? '0' : $kupac->flag_potvrda ).")::integer,(".($kupac->partner_id == '' ? 'NULL' : $kupac->partner_id ).")::integer,'".pg_escape_string($kupac->prezime)."',0,(".($kupac->flag_vrsta_kupca == '' ? 'NULL' : $kupac->flag_vrsta_kupca ).")::integer,'".pg_escape_string($kupac->telefon_mobilni)."','".pg_escape_string($kupac->naziv)."',NULL,'".pg_escape_string($kupac->mesto)."','".pg_escape_string($kupac->pib)."',(".($kupac->status_registracije == '' ? 'NULL' : $kupac->status_registracije ).")::integer,'".trim($kupac->sifra_connect)."',0,NULL,0,NULL,0,0),";
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='web_kupac'"));
		$table_temp = "(VALUES ".substr($kupac_rows,0,-1).") web_kupac_temp(".implode(',',$columns).")";		

		DB::statement("INSERT INTO web_kupac (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM web_kupac t WHERE t.email=web_kupac_temp.email))");


		// roba
		$articles = $transfer_pgsql->select("SELECT * FROM roba where roba_id > -1");
		
		$article_rows="";
		foreach ($articles as $article) {
			$flag_prikazi_u_cenovniku = $article->flag_aktivan == 1 ? ($article->web_cena > 0 ? $article->flag_prikazi_u_cenovniku : 0) : 0;
			$naziv = pg_escape_string(self::convert1250($article->naziv));
			$article_rows .= "(nextval('roba_roba_id_seq'),'".(!empty($article->sifra_d) ? trim($article->sifra_d) : '')."','".$naziv."',NULL,NULL,NULL,".($article->grupa_pr_id > 0 ? $getMappedGroups[$article->grupa_pr_id] : '-1').",0,1,".($article->proizvodjac_id > -1 ? strval($getMappedManufacturers[$article->proizvodjac_id]) : -1).",-1,nextval('roba_roba_id_seq'),NULL,NULL,'".substr($naziv,0,20)."',0,-1,0,0,0,0,9,0,0,0,0,1,".strval($flag_prikazi_u_cenovniku).",0,NULL,".$article->flag_aktivan.",".strval($article->racunska_cena_nc).",".strval($article->racunska_cena_a).",".strval($article->racunska_cena_nc).",0,NULL,".strval(($article->mpcena ? $article->mpcena : 0.00)).",false,0,(NULL)::integer,'".substr($naziv,0,199)."',".$article->flag_cenovnik.",NULL,NULL,(NULL)::integer,(NULL)::integer,0,0,0,-1,".($article->dobavljac_id > 0 ? $getMappedPartners[$article->dobavljac_id] : '-1').",".(isset($article->web_cena) ? strval($article->web_cena) : '0').",1,0,'".pg_escape_string(str_replace("\n","<br>",self::convert1250($article->web_opis)))."',NULL,NULL,".pg_escape_string(self::convert1250($article->web_flag_karakteristike)).",".$article->akcija_flag_primeni.",(".(!is_null($article->akcija_popust) ? $article->akcija_popust : '0.00').")::numeric,'".pg_escape_string(self::convert1250($article->akcija_opis))."','".pg_escape_string(self::convert1250(substr($article->akcija_naslov,0,100)))."',NULL,NULL,1,0,'".$article->barkod."',0,0,1,1,-1,'".pg_escape_string(self::convert1250($article->model))."',NULL,NULL,NULL,0,".(!is_null($article->b2b_max_rabat) ? $article->b2b_max_rabat : 0).",".(!is_null($article->b2b_akcijski_rabat) ? $article->b2b_akcijski_rabat : 0).",".$article->akcijska_cena.",0,NULL,(NULL)::date,(NULL)::date,(NULL)::integer,NULL,NULL,'".strval($article->roba_id)."',0,(NULL)::integer),";
		}

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".substr($article_rows,0,-1).") roba_temp(".implode(',',$columns).")";

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("INSERT INTO roba (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM roba t WHERE t.naziv=roba_temp.naziv))");
		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	    $getMappedArticles = self::getMappedArticles();


	    //roba grupe
		$roba_grupe = $transfer_pgsql->select("SELECT * FROM roba_grupe");
		$roba_grupa_rows=array();
		if(count($roba_grupe) > 0){
			foreach($roba_grupe as $roba_grupa){
			    if(isset($getMappedGroups[$roba_grupa->grupa_pr_id]) && isset($getMappedArticles[$roba_grupa->roba_id])){
			    	$grupa_pr_id = $getMappedGroups[$roba_grupa->grupa_pr_id];
			    	$roba_id = $getMappedArticles[$roba_grupa->roba_id];
			    	$roba_grupa_rows[] = "(".$roba_id.",".$grupa_pr_id.")";
				}
			}
		}
		if(count($roba_grupa_rows) > 0){
			DB::statement("INSERT INTO roba_grupe (roba_id,grupa_pr_id) VALUES ".implode(',',$roba_grupa_rows)."");
		}	    

		//lager
		$lagers = $transfer_pgsql->select("SELECT * FROM lager");
		$lager_rows=array();
		if(count($lagers) > 0){
			foreach($lagers as $lager){
			    if(isset($getMappedArticles[$lager->roba_id])){
			    	$roba_id = $getMappedArticles[$lager->roba_id];
			    	$lager_rows[] = "(".$roba_id.",".$lager->orgj_id.",".$lager->poslovna_godina_id.",".$lager->kolicina.")";
				}
			}
		}
		if(count($lager_rows) > 0){
			DB::statement("INSERT INTO lager (roba_id,orgj_id,poslovna_godina_id,kolicina) VALUES ".implode(',',$lager_rows)."");
		}

		// //slike
		// $images = $transfer_pgsql->select("SELECT * FROM web_slika");
		// $web_slika_id= DB::select("SELECT MAX(web_slika_id) + 1 AS next_id FROM web_slika")[0]->next_id;
		// if(is_null($web_slika_id)){
		// 	$web_slika_id = 1;
		// }
		// foreach($images as $image){

		//     if(isset($getMappedArticles[$image->roba_id])){
		//     	$extension = self::fileExtension($image->putanja);
		//     	if(in_array($extension,array('jpg','png','jpeg','JPG','PNG','JPEG'))){
		// 			try { 
		// 			    $name = $web_slika_id.'.'.$extension;

		// 			    $source = '/var/www/html/'.$project_dir.'/'.$image->putanja;
		// 			    $destination = '/var/www/html/wbp/images/products/big/'.$name;

		// 			    File::copy($source,$destination);

		// 				DB::statement("INSERT INTO web_slika (web_slika_id,roba_id,akcija,putanja) VALUES (".$web_slika_id.",".$getMappedArticles[$image->roba_id].",".$image->akcija.",'images/products/big/".$name."')");
		// 				$web_slika_id++;
		// 			}
		// 			catch (Exception $e) {
		// 			}
		// 		}
		// 	}
		// }
		// DB::statement("SELECT setval('web_slika_web_slika_id_seq', (SELECT MAX(web_slika_id)+1 FROM web_slika), FALSE)");


		//characteristics
		$characteristics = $transfer_pgsql->select("SELECT * FROM grupa_pr_naziv");
		if($characteristics){
			foreach($characteristics as $characteristic){
				$grupa_pr_naziv_id = DB::select("SELECT MAX(grupa_pr_naziv_id) + 1 AS next_id FROM grupa_pr_naziv")[0]->next_id;
				if(is_null($grupa_pr_naziv_id)){
					$grupa_pr_naziv_id = 1;
				}
				if(isset($transfer_pgsql->select("SELECT grupa FROM grupa_pr WHERE grupa_pr_id = ".$characteristic->grupa_pr_id."")[0])){
					$grupa_naziv = $transfer_pgsql->select("SELECT grupa FROM grupa_pr WHERE grupa_pr_id = ".$characteristic->grupa_pr_id."")[0]->grupa;
					$grupa_pr = DB::select("SELECT grupa_pr_id FROM grupa_pr WHERE grupa = '".pg_escape_string($grupa_naziv)."'");

					if(isset($grupa_pr[0])){
						DB::statement("INSERT INTO grupa_pr_naziv (grupa_pr_naziv_id,grupa_pr_id,active,css,naziv,rbr) VALUES (".$grupa_pr_naziv_id.",".$grupa_pr[0]->grupa_pr_id.",".$characteristic->active.",'".$characteristic->css."','".pg_escape_string($characteristic->naziv)."',".$characteristic->rbr.")");

						$characteristic_values = $transfer_pgsql->select("SELECT * FROM grupa_pr_vrednost WHERE grupa_pr_naziv_id = ".$characteristic->grupa_pr_naziv_id."");
						if($characteristic_values){
							foreach($characteristic_values as $characteristic_value){
								DB::statement("INSERT INTO grupa_pr_vrednost (grupa_pr_naziv_id,active,css,naziv,rbr,boja) VALUES (".$grupa_pr_naziv_id.",".$characteristic_value->active.",'".$characteristic_value->css."','".pg_escape_string($characteristic_value->naziv)."',".(!is_null($characteristic_value->rbr) ? $characteristic_value->rbr : '0').",'".$characteristic_value->boja."')");
							}
						}
					}
				}
			}
			if(count($characteristics) > 0){
				DB::statement("SELECT setval('grupa_pr_naziv_grupa_pr_naziv_id_seq', (SELECT MAX(grupa_pr_naziv_id)+1 FROM grupa_pr_naziv), FALSE)");
				DB::statement("SELECT setval('grupa_pr_vrednost_grupa_pr_vrednost_id_seq', (SELECT MAX(grupa_pr_vrednost_id)+1 FROM grupa_pr_vrednost), FALSE)");
			}
		}


		$article_characteristics = $transfer_pgsql->select("SELECT * FROM web_roba_karakteristike");
		if($article_characteristics){
			foreach($article_characteristics as $article_characteristic){
				if(isset($getMappedArticles[$article_characteristic->roba_id])){
					$group_characteristic_values = $transfer_pgsql->select("SELECT * FROM grupa_pr_vrednost WHERE grupa_pr_vrednost_id = ".$article_characteristic->grupa_pr_vrednost_id."");
					if(isset($group_characteristic_values[0])){
						$characteristic_value = $group_characteristic_values[0]->naziv;
						$grupa_pr_vrednost = DB::select("SELECT * FROM grupa_pr_vrednost WHERE naziv = '".pg_escape_string($characteristic_value)."'");
						if(isset($grupa_pr_vrednost[0])){
							DB::statement("INSERT INTO web_roba_karakteristike (roba_id,rbr,grupa_pr_naziv_id,vrednost,vrednost_css,grupa_pr_vrednost_id) VALUES (".$getMappedArticles[$article_characteristic->roba_id].",".(!is_null($article_characteristic->rbr) ? $article_characteristic->rbr : '0').",".$grupa_pr_vrednost[0]->grupa_pr_naziv_id.",'".pg_escape_string($article_characteristic->vrednost)."','".$article_characteristic->vrednost_css."',".$grupa_pr_vrednost[0]->grupa_pr_vrednost_id.")");
						}
					}
				}
			}
		}


		// dobavljac cenovnik
		$dobavljac_cenovnik = $transfer_pgsql->select("SELECT * FROM dobavljac_cenovnik");
		$dc_rows="";
		foreach ($dobavljac_cenovnik as $dc) {
			if(isset($getMappedPartners[$dc->partner_id])){
				$naziv = pg_escape_string($dc->naziv);
				$dc_rows .= "(nextval('dobavljac_cenovnik_dobavljac_cenovnik_id_seq'),".$getMappedPartners[$dc->partner_id].",'".$dc->sifra_kod_dobavljaca."',".(isset($getMappedArticles[$dc->roba_id]) ? $getMappedArticles[$dc->roba_id] : -1).",".$dc->povezan.",'".$naziv."',".$dc->pdv.",".$dc->tarifna_grupa_id.",'".$dc->grupa."','".$dc->podgrupa."',".(isset($getMappedGroups[$dc->grupa_pr_id]) ? $getMappedGroups[$dc->grupa_pr_id] : -1).",'".$dc->proizvodjac."',".(isset($getMappedManufacturers[$dc->proizvodjac_id]) ? $getMappedManufacturers[$dc->proizvodjac_id] : -1).",".$dc->kolicina.",0,".$dc->cena_nc.",".$dc->zadnja_cena_nc.",".$dc->cena_a.",".$dc->a_marza.",".$dc->cena_end.",".$dc->end_marza.",".$dc->mpcena.",".$dc->mp_marza.",".$dc->web_cena.",".$dc->web_marza.",".$dc->flag_novi.",".$dc->flag_aktivan.",'".$dc->opis."',".$dc->flag_zakljucan.",'".$dc->model."',".$dc->flag_prikazi_u_cenovniku.",'".$dc->url_detaljno."','".$dc->detaljno_izvrsi_fajl."',".$dc->flag_opis_postoji.",".$dc->flag_slika_postoji.",".$dc->web_flag_karakteristike.",'".$dc->barkod."',".$dc->pmp_cena."	,".$dc->eponuda_cena.",".$dc->rucna_cena.",'".$dc->komentar."',0,NULL),";
			}
		}

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='dobavljac_cenovnik'"));
		$table_temp = "(VALUES ".substr($dc_rows,0,-1).") dobavljac_cenovnik_temp(".implode(',',$columns).")";

		DB::statement("INSERT INTO dobavljac_cenovnik (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM dobavljac_cenovnik t WHERE t.naziv=dobavljac_cenovnik_temp.naziv))");


		// dobavljac cenovnik slike
		$dobavljac_cenovnik_slike = $transfer_pgsql->select("SELECT * FROM dobavljac_cenovnik_slike");
		$dcs_rows="";
		foreach ($dobavljac_cenovnik_slike as $dcs) {
			if(isset($getMappedPartners[$dcs->partner_id])){
				$dcs_rows .= "(nextval('dobavljac_cenovnik_slike_dobavljac_cenovnik_slike_id_seq'),".$getMappedPartners[$dcs->partner_id].",'".$dcs->sifra_kod_dobavljaca."','".$dcs->putanja."',".$dcs->akcija."),";
			}
		}

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='dobavljac_cenovnik_slike'"));
		$table_temp = "(VALUES ".substr($dcs_rows,0,-1).") dobavljac_cenovnik_slike_temp(".implode(',',$columns).")";
		DB::statement("INSERT INTO dobavljac_cenovnik_slike (SELECT * FROM ".$table_temp.")");


		// dobavljac cenovnik karakteristike
		$dc_karak_count = $transfer_pgsql->select("SELECT count(dobavljac_cenovnik_karakteristike_id) as count FROM dobavljac_cenovnik_karakteristike")[0]->count;
		$limit = 200000;
		$div = intdiv($dc_karak_count,$limit);
		$mod = fmod($dc_karak_count,$limit);
		if($mod > 0){
			$div++;
		}

		foreach(range(0,($div-1)) as $page){
			$dobavljac_cenovnik_karak = $transfer_pgsql->select("SELECT * FROM dobavljac_cenovnik_karakteristike LIMIT ".$limit." OFFSET ".strval($limit*$page));
			$dck_rows="";
			foreach ($dobavljac_cenovnik_karak as $dck) {
				if(isset($getMappedPartners[$dck->partner_id])){
					$dck_rows .= "(nextval('dobavljac_cenovnik_karakteris_dobavljac_cenovnik_karakteris_seq'),".$getMappedPartners[$dck->partner_id].",'".$dck->sifra_kod_dobavljaca."',".(isset($getMappedArticles[$dck->roba_id]) ? $getMappedArticles[$dck->roba_id] : '-1').",'".$dck->karakteristika_naziv."','".$dck->karakteristika_vrednost."','".$dck->karakteristika_grupa."',(".(!is_null($dck->redni_br_grupe) ? "'".$dck->redni_br_grupe."'" : "NULL").")::integer),";
				}
			}

			$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='dobavljac_cenovnik_karakteristike'"));
			$table_temp = "(VALUES ".substr($dck_rows,0,-1).") dobavljac_cenovnik_karakteristike_temp(".implode(',',$columns).")";

			DB::statement("INSERT INTO dobavljac_cenovnik_karakteristike (SELECT * FROM ".$table_temp.")");
		}


$this->info('Transfered.');
die;

DB::statement("UPDATE grupa_pr set id_is = NULL");
DB::statement("UPDATE grupa_pr set id_is = trim(sifra_connect) WHERE sifra_connect is not null and sifra_connect <> ''");
DB::statement("UPDATE proizvodjac set id_is = NULL");
DB::statement("UPDATE proizvodjac set id_is = trim(sifra_connect) WHERE sifra_connect is not null and sifra_connect <> ''");
DB::statement("UPDATE partner set id_is = NULL");
DB::statement("UPDATE partner set id_is = trim(sifra_connect) WHERE sifra_connect is not null and sifra_connect <> ''");
DB::statement("UPDATE roba set id_is = NULL");
DB::statement("UPDATE roba set id_is = trim(sifra_d) where dobavljac_id = -1 and sifra_d is not null and sifra_d <> ''");
die;
		// podrzan import
		$importi = $transfer_pgsql->select("SELECT * FROM podrzan_import WHERE podrzan_import_id >0 ");
		$import_rows="";
		foreach ($importi as $import) {
		$import_rows .= "(('".$import->podrzan_import_id."')::integer,'".$import->naziv."','".$import->vrednost_kolone."',(".($import->dozvoljen == '' ? 'NULL' : $import->dozvoljen ).")::integer,(".($import->file_upload == '' ? 'NULL' : $import->file_upload ).")::integer,'".$import->file_type."'),";
		}
		$columns = array_map('current',$transfer_pgsql->select("SELECT column_name FROM information_schema.columns where table_name='podrzan_import'"));
		$table_temp = "(VALUES ".substr($import_rows,0,-1).") podrzan_import_temp(".implode(',',$columns).")";		

		DB::statement("INSERT INTO podrzan_import (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM podrzan_import t WHERE t.naziv=podrzan_import_temp.naziv))");

		// dob_cen_kolone
		$kolone = $transfer_pgsql->select("SELECT * FROM dobavljac_cenovnik_kolone WHERE dobavljac_cenovnik_kolone_id >0 ");
		$kolone_rows="";
		foreach ($kolone as $kolona) {
		$kolone_rows .= "(nextval('dobavljac_cenovnik_kolone_dobavljac_cenovnik_kolone_id_seq'),(".($kolona->partner_id == '' ? 'NULL' : $kolona->partner_id ).")::integer,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,(".($kolona->sifra_attribute == '' ? 'NULL' : $kolona->sifra_attribute ).")::integer,NULL,(".($kolona->valuta_id == '' ? 'NULL' : $kolona->valuta_id ).")::integer,(".($kolona->naziv_proizvodjac == '' ? 'NULL' : $kolona->naziv_proizvodjac ).")::integer,'".$kolona->naziv_skripte."','".$kolona->naziv_skripte_slika."','".$kolona->import_naziv_web."','".$kolona->service_username."','".$kolona->service_password."','".$kolona->auto_link."',(".($kolona->auto_import == '' ? 'NULL' : $kolona->auto_import ).")::integer,(".($kolona->auto_import_short == '' ? 'NULL' : $kolona->auto_import_short ).")::integer),";
		}
		$columns = array_map('current',$transfer_pgsql->select("SELECT column_name FROM information_schema.columns where table_name='dobavljac_cenovnik_kolone'"));
		$table_temp = "(VALUES ".substr($kolone_rows,0,-1).") dobavljac_cenovnik_kolone_temp(".implode(',',$columns).")";		

		DB::statement("INSERT INTO dobavljac_cenovnik_kolone (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM dobavljac_cenovnik_kolone t WHERE t.import_naziv_web=dobavljac_cenovnik_kolone_temp.import_naziv_web))");

		
		// web b2c seo
		$seos = $transfer_pgsql->select("SELECT * FROM web_b2c_seo WHERE web_b2c_seo_id >0 ");
		$seo_rows="";
		foreach ($seos as $seo) {
		$seo_rows .= "(nextval('web_b2c_seo_web_b2c_seo_id_seq'),'".$seo->naziv_stranice."','".$seo->title."','".$seo->description."',
		'".$seo->keywords."',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,(".($seo->flag_page == '' ? 'NULL' : $seo->flag_page ).")::integer,(".($seo->rb_strane == '' ? '0' : $seo->rb_strane ).")::integer,(".($seo->menu_top == '' ? 'NULL' : $seo->menu_top ).")::integer,(".($seo->header_menu == '' ? 'NULL' : $seo->header_menu ).")::integer,(".($seo->footer == '' ? 'NULL' : $seo->footer ).")::integer,(".($seo->disable == '' ? 'NULL' : $seo->disable ).")::integer,-1,(".($seo->header_menu == '' ? 'NULL' : $seo->header_menu ).")::integer,(".($seo->footer == '' ? 'NULL' : $seo->footer ).")::integer,(".($seo->b2b_header == '' ? 'NULL' : $seo->b2b_header ).")::integer,(".($seo->b2b_footer == '' ? 'NULL' : $seo->b2b_footer ).")::integer,(NULL)::integer,(NULL)::integer),";


		}
		$columns = array_map('current',$transfer_pgsql->select("SELECT column_name FROM information_schema.columns where table_name='web_b2c_seo'"));
		$table_temp = "(VALUES ".substr($seo_rows,0,-1).") web_b2c_seo_temp(".implode(',',$columns).")";		

		DB::statement("INSERT INTO web_b2c_seo (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM web_b2c_seo t WHERE t.naziv_stranice=web_b2c_seo_temp.naziv_stranice))");

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('project_dir', InputArgument::OPTIONAL, 'Project Dir.')
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			// array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
